"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
const dotenv_1 = require("dotenv");
(0, dotenv_1.config)();
const http_1 = __importDefault(require("http"));
const https_1 = __importDefault(require("https"));
const body_parser_1 = __importDefault(require("body-parser"));
const express_fileupload_1 = __importDefault(require("express-fileupload"));
const index_1 = require("./index");
const admin_routes_1 = __importDefault(require("../modules/admin/admin_routes"));
const user_routes_1 = __importDefault(require("../modules/user/user_routes"));
const event_routes_1 = __importDefault(require("../modules/event/event_routes"));
const upload_routes_1 = __importDefault(require("../modules/uploads/upload_routes"));
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const output_openapi_json_1 = __importDefault(require("../../output.openapi.json"));
const fs_1 = __importDefault(require("fs"));
const app = (0, express_1.default)();
const env = process.env.ENVIRONMENT;
console.log("----------env-------", env);
let port = process.env.LOCAL_PORT;
if (env == 'PROD') {
    port = process.env.PROD_PORT;
}
app.use((0, cors_1.default)({ origin: "*" }));
app.use((0, express_fileupload_1.default)());
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(express_1.default.json());
app.get('/', (req, res) => { res.send('Hello World!'); });
app.use("/Admin", admin_routes_1.default);
app.use("/User", user_routes_1.default);
app.use("/Event", event_routes_1.default);
app.use("/Upload", upload_routes_1.default);
// make swagger documentation
let swagger_options = { customSiteTitle: "Meeta Api Documentation" };
app.use('/docs', swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(output_openapi_json_1.default, swagger_options));
let server;
// if (env == 'PROD') {
//   server = http.createServer(app);
//   server.listen(port, () => {
//     console.log(`Server running at port ${port}...`);
//   })
//   // backup_db_cron.start();
//   // remove_img_cron.start();
// }
// else {
//   server = http.createServer(app);
//   server.listen(port, () => {
//     console.log(`Server running at port ${port}...`);
//   })
// }
if (process.env.SSL == "true") {
    server = https_1.default.createServer({
        key: fs_1.default.readFileSync(process.env.SSL_KEY),
        cert: fs_1.default.readFileSync(process.env.SSL_CERT),
    }, app);
}
else {
    server = http_1.default.createServer(app);
}
server.listen(port, console.log(`server is running on PORT ${port}`));
(0, index_1.connect_to_db)();
(0, index_1.bootstrap_data)();
