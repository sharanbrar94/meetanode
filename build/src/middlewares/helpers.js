"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.decrypt_password = exports.bcrypt_password = exports.gen_unique_code = exports.generate_otp = exports.set_options = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const DAO = __importStar(require("../DAO"));
const Models = __importStar(require("../models/index"));
const randomstring_1 = __importDefault(require("randomstring"));
const index_1 = require("../config/index");
const default_limit = index_1.app_constant.default_limit;
const salt_rounds = index_1.app_constant.salt_rounds;
const set_options = (pagination, limit) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let options = {
            lean: true,
            sort: { _id: -1 }
        };
        if (pagination == undefined && typeof limit != undefined) {
            options = {
                lean: true,
                limit: parseInt(limit),
                sort: { _id: -1 }
            };
        }
        else if (typeof pagination != undefined && limit == undefined) {
            options = {
                lean: true,
                skip: parseInt(pagination) * default_limit,
                limit: default_limit,
                sort: { _id: -1 }
            };
        }
        else if (typeof pagination != undefined && typeof limit != undefined) {
            options = {
                lean: true,
                skip: parseInt(pagination) * parseInt(limit),
                limit: parseInt(limit),
                sort: { _id: -1 }
            };
        }
        return options;
    }
    catch (err) {
        throw err;
    }
});
exports.set_options = set_options;
const generate_otp = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let options = {
            length: 4,
            charset: '123456789'
        };
        let code = randomstring_1.default.generate(options);
        return 1234;
    }
    catch (err) {
        throw err;
    }
});
exports.generate_otp = generate_otp;
const gen_unique_code = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let options = {
            length: 7,
            charset: 'alphanumeric'
        };
        let random_value = randomstring_1.default.generate(options);
        // fetch users count
        let total_users = yield DAO.count_data(Models.Users, {});
        let inc_value = Number(total_users) + 1;
        // unique code
        let unique_code = `${random_value}${inc_value}`;
        return unique_code;
    }
    catch (err) {
        throw err;
    }
});
exports.gen_unique_code = gen_unique_code;
const bcrypt_password = (password) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const hash = yield bcrypt_1.default.hashSync(password, salt_rounds);
        return hash;
    }
    catch (err) {
        throw err;
    }
});
exports.bcrypt_password = bcrypt_password;
const decrypt_password = (password, hash) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const decryt = yield bcrypt_1.default.compareSync(password, hash);
        return decryt;
    }
    catch (err) {
        throw err;
    }
});
exports.decrypt_password = decrypt_password;
