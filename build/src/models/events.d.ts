/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const Events: import("mongoose").Model<import("mongoose").Document<any> & {
    _id: import("mongoose").Types.ObjectId;
    description: string;
    __v: number;
    is_deleted: boolean;
    created_at: string;
    user_id: any;
    event_name: string;
    event_type: number;
    private_event: number;
    slug: string;
    start_date: Date;
    end_date: Date;
    tagline: string;
    overview: string;
    state: string;
    city: string;
    country: string;
    address: string;
    cover_photo: string;
    twitter: string;
    website: string;
} & {
    user_id?: unknown;
}> & {
    [name: string]: Function;
};
export default Events;
