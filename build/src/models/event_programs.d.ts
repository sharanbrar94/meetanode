/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const EventPrograms: import("mongoose").Model<import("mongoose").Document<any> & {
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    created_at: string;
    event_id: any;
    program_name: string;
} & {
    event_id?: unknown;
}> & {
    [name: string]: Function;
};
export default EventPrograms;
