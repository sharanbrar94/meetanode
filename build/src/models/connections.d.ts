/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const Connections: import("mongoose").Model<import("mongoose").Document<any> & {
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    created_at: string;
    sent_by: any;
    sent_to: any;
    last_message: string;
    updated_at: string;
} & {
    sent_by?: unknown;
    sent_to?: unknown;
}> & {
    [name: string]: Function;
};
export default Connections;
