"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts_mongoose_1 = require("ts-mongoose");
const social_type = [null, "GOOGLE", "TWITTER", "APPLE"];
const account_status = ["ACTIVATED", "DEACTIVATED"];
const UserSchema = (0, ts_mongoose_1.createSchema)({
    social_type: ts_mongoose_1.Type.string({ default: null, enum: social_type }),
    social_token: ts_mongoose_1.Type.string({ default: null }),
    image: ts_mongoose_1.Type.string({ default: null }),
    cover_image: ts_mongoose_1.Type.string({ default: null }),
    username: ts_mongoose_1.Type.string({ default: null }),
    name: ts_mongoose_1.Type.string({ default: null }),
    email: ts_mongoose_1.Type.string({ default: null }),
    is_blocked: ts_mongoose_1.Type.boolean({ default: false }),
    is_deleted: ts_mongoose_1.Type.boolean({ default: false }),
    created_at: ts_mongoose_1.Type.string({ default: +new Date() })
});
const Users = (0, ts_mongoose_1.typedModel)('users', UserSchema);
exports.default = Users;
