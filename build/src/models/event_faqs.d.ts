/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const EventFaqs: import("mongoose").Model<import("mongoose").Document<any> & {
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    is_deleted: boolean;
    created_at: string;
    question: string;
    answer: string;
    event_id: any;
} & {
    event_id?: unknown;
}> & {
    [name: string]: Function;
};
export default EventFaqs;
