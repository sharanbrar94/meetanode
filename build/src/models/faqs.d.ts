/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const Faq: import("mongoose").Model<import("mongoose").Document<any> & {
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    created_at: string;
    question: string;
    answer: string;
} & {}> & {
    [name: string]: Function;
};
export default Faq;
