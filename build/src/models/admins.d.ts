/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const Admin: import("mongoose").Model<import("mongoose").Document<any> & {
    roles: string[];
    name: string;
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    image: string;
    email: string;
    password: string;
    super_admin: boolean;
    is_blocked: boolean;
    is_deleted: boolean;
    created_at: string;
} & {}> & {
    [name: string]: Function;
};
export default Admin;
