/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const ScheduleSpeaker: import("mongoose").Model<import("mongoose").Document<any> & {
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    is_deleted: boolean;
    created_at: string;
    schedule_id: any;
    speaker_id: any;
} & {
    schedule_id?: unknown;
    speaker_id?: unknown;
}> & {
    [name: string]: Function;
};
export default ScheduleSpeaker;
