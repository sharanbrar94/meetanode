/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const EventSchedule: import("mongoose").Model<import("mongoose").Document<any> & {
    date: Date;
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    is_deleted: boolean;
    created_at: string;
    event_id: any;
    start_time: string;
    end_time: string;
    venue: string;
} & {
    event_id?: unknown;
}> & {
    [name: string]: Function;
};
export default EventSchedule;
