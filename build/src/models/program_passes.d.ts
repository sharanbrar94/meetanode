/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const ProgramPasses: import("mongoose").Model<import("mongoose").Document<any> & {
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    created_at: string;
    pass_id: any;
    program_id: any;
} & {
    pass_id?: unknown;
    program_id?: unknown;
}> & {
    [name: string]: Function;
};
export default ProgramPasses;
