/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const Users: import("mongoose").Model<import("mongoose").Document<any> & {
    name: string;
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    image: string;
    email: string;
    is_blocked: boolean;
    is_deleted: boolean;
    created_at: string;
    social_type: string;
    social_token: string;
    cover_image: string;
    username: string;
} & {}> & {
    [name: string]: Function;
};
export default Users;
