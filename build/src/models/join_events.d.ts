/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const JoinEvents: import("mongoose").Model<import("mongoose").Document<any> & {
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    is_deleted: boolean;
    created_at: string;
    user_id: any;
    event_id: any;
} & {
    user_id?: unknown;
    event_id?: unknown;
}> & {
    [name: string]: Function;
};
export default JoinEvents;
