"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts_mongoose_1 = require("ts-mongoose");
const FaqSchema = (0, ts_mongoose_1.createSchema)({
    question: ts_mongoose_1.Type.string({ default: null }),
    answer: ts_mongoose_1.Type.string({ default: null }),
    created_at: ts_mongoose_1.Type.string({ default: +new Date() }),
});
const Faq = (0, ts_mongoose_1.typedModel)('faqs', FaqSchema);
exports.default = Faq;
