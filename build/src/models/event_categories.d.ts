/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const EventCategories: import("mongoose").Model<import("mongoose").Document<any> & {
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    is_deleted: boolean;
    created_at: string;
    event_id: any;
    category_id: any;
} & {
    event_id?: unknown;
    category_id?: unknown;
}> & {
    [name: string]: Function;
};
export default EventCategories;
