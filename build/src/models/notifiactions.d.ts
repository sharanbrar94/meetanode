/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const Notifications: import("mongoose").Model<import("mongoose").Document<any> & {
    message: string;
    type: string;
    read: boolean;
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    created_at: string;
    user_id: any;
    title: string;
    product_id: any;
    clear: boolean;
} & {
    user_id?: unknown;
    product_id?: unknown;
}> & {
    [name: string]: Function;
};
export default Notifications;
