/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const Products: import("mongoose").Model<import("mongoose").Document<any> & {
    name: string;
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    is_blocked: boolean;
    is_deleted: boolean;
    created_at: string;
    added_by: any;
    image_url: string;
    price: number;
    sold: boolean;
} & {
    added_by?: unknown;
}> & {
    [name: string]: Function;
};
export default Products;
