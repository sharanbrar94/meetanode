/// <reference types="mongoose" />
/// <reference types="ts-mongoose/plugin" />
declare const EventPasses: import("mongoose").Model<import("mongoose").Document<any> & {
    name: string;
    _id: import("mongoose").Types.ObjectId;
    __v: number;
    is_deleted: boolean;
    created_at: string;
    price: string;
    event_id: any;
} & {
    event_id?: unknown;
}> & {
    [name: string]: Function;
};
export default EventPasses;
