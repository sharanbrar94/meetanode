"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.get_user_profile = exports.edit_profile_data = exports.create_new_user = exports.set_user_data = exports.verify_user_info = exports.block_delete_data = exports.make_user_response = exports.save_session_data = exports.update_otp = exports.fetch_user_token = exports.generate_user_token = void 0;
const DAO = __importStar(require("../../DAO"));
const Models = __importStar(require("../../models"));
const mongoose_1 = __importDefault(require("mongoose"));
const index_1 = require("../../config/index");
const index_2 = require("../../middlewares/index");
const user_scope = index_1.app_constant.scope.user;
const get_user_profile = (_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //let query = { _id }
        let projection = { __v: 0, password: 0, otp: 0 };
        // let options = { lean: true }
        //let response: any = await DAO.get_data(Models.Events, query, projection, options)
        let query = [
            yield match_user_by_id(_id),
            yield look_up_event_events(),
            yield look_up_join_events()
        ];
        let options = { lean: true };
        let fetch_data = yield DAO.aggregate_data(Models.Users, query, options);
        return fetch_data;
    }
    catch (err) {
        throw err;
    }
});
exports.get_user_profile = get_user_profile;
const look_up_event_events = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $lookup: {
                from: "events",
                let: { user_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$user_id", "$$user_id"]
                            }
                        }
                    }
                ],
                as: "created_events"
            }
        };
    }
    catch (err) {
        throw err;
    }
});
const look_up_join_events = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $lookup: {
                from: "join_events",
                let: { user_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$user_id", "$$user_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "events",
                            let: { event_id: "$event_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$event_id"]
                                        }
                                    }
                                }
                            ],
                            as: "events"
                        }
                    },
                    {
                        $unwind: "$events"
                    },
                    {
                        $group: {
                            _id: "$events._id",
                            event_name: { "$last": "$events.event_name" },
                            slug: { "$last": "$events.slug" },
                            start_date: { "$last": "$events.start_date" },
                            end_date: { "$last": "$events.end_date" },
                            country: { "$last": "$events.country" },
                            state: { "$last": "$events.state" },
                            city: { "$last": "$events.city" },
                            address: { "$last": "$events.address" }
                            //total_event_attendees: await fetch_total_event_attendees("$events._id")
                        }
                    }
                ],
                as: "joined_events"
            }
        };
    }
    catch (err) {
        throw err;
    }
});
const fetch_total_event_attendees = (event_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { event_id };
        let projection = { __v: 0 };
        let options = { lean: true };
        let response = yield DAO.get_data(Models.JoinEvents, query, projection, options);
        return response.length;
    }
    catch (err) {
        throw err;
    }
});
const match_user_by_id = (user_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $match: {
                _id: mongoose_1.default.Types.ObjectId(user_id)
            }
        };
    }
    catch (err) {
        throw err;
    }
});
const generate_user_token = (_id, req_data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let token_data = {
            _id: _id,
            scope: user_scope,
            collection: Models.Users,
            token_gen_at: +new Date()
        };
        let response = yield fetch_user_token(token_data, req_data);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.generate_user_token = generate_user_token;
const fetch_user_token = (token_data, req_data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let access_token = yield (0, index_2.generate_token)(token_data);
        let response = yield save_session_data(access_token, token_data, req_data);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.fetch_user_token = fetch_user_token;
const update_otp = (user_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // generate otp
        let otp = yield index_2.helpers.generate_otp();
        let query = { _id: user_id };
        let update = { otp: otp };
        let options = { new: true };
        return yield DAO.find_and_update(Models.Users, query, update, options);
    }
    catch (err) {
        throw err;
    }
});
exports.update_otp = update_otp;
const save_session_data = (access_token, token_data, req_data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id: user_id, token_gen_at } = token_data, { device_type, fcm_token } = req_data;
        let set_data = {
            type: "USER",
            user_id: user_id,
            access_token: access_token,
            token_gen_at: token_gen_at,
            created_at: +new Date()
        };
        if (device_type != null || device_type != undefined) {
            set_data.device_type = device_type;
        }
        if (fcm_token != null || fcm_token != undefined) {
            set_data.fcm_token = fcm_token;
        }
        let response = yield DAO.save_data(Models.Sessions, set_data);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.save_session_data = save_session_data;
const make_user_response = (token_data, language) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { user_id, access_token, device_type, fcm_token, token_gen_at } = token_data;
        let query = { _id: user_id };
        let projection = { __v: 0, password: 0, otp: 0 };
        let options = { lean: true };
        let response = yield DAO.get_data(Models.Users, query, projection, options);
        if (response.length) {
            response[0].access_token = access_token;
            response[0].device_type = device_type;
            response[0].fcm_token = fcm_token;
            response[0].token_gen_at = token_gen_at;
            return response[0];
        }
        else {
            throw yield (0, index_2.handle_custom_error)('INVALID_OBJECT_ID', language);
        }
    }
    catch (err) {
        throw err;
    }
});
exports.make_user_response = make_user_response;
const block_delete_data = (data, collection) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, is_blocked, is_deleted } = data;
        let query = { _id: _id };
        let data_to_update = {};
        if (typeof is_blocked !== "undefined" && is_blocked !== null) {
            data_to_update.is_blocked = is_blocked;
        }
        if (typeof is_deleted !== "undefined" && is_deleted !== null) {
            data_to_update.is_deleted = is_deleted;
        }
        let options = { new: true };
        let response = yield DAO.find_and_update(collection, query, data_to_update, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.block_delete_data = block_delete_data;
const verify_user_info = (query) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Users, query, projection, options);
        return fetch_data;
    }
    catch (err) {
        throw err;
    }
});
exports.verify_user_info = verify_user_info;
const set_user_data = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { name, email, country_code, phone_no, password } = data;
        // bycryt password
        let hassed_password = yield index_2.helpers.bcrypt_password(password);
        // fetch otp
        let otp = yield index_2.helpers.generate_otp();
        let data_to_save = {
            name: name,
            email: email.toLowerCase(),
            country_code: country_code,
            phone_no: phone_no,
            password: hassed_password,
            otp: otp,
            created_at: +new Date()
        };
        if (data.profile_pic) {
            data_to_save.profile_pic = data.profile_pic;
        }
        let response = yield DAO.save_data(Models.Users, data_to_save);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.set_user_data = set_user_data;
const create_new_user = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { social_type, social_token, name, email, username, image, cover_image } = data;
        let data_to_save = {
            social_type: social_type,
            social_token: social_token,
            username: username,
            image: image,
            cover_image: cover_image,
            created_at: +new Date()
        };
        if (email != null || email != undefined) {
            data_to_save.email = email.toLowerCase();
        }
        if (name != null || email != undefined) {
            data_to_save.name = name;
        }
        let response = yield DAO.save_data(Models.Users, data_to_save);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.create_new_user = create_new_user;
const edit_profile_data = (data, user_data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let set_data = {}, { _id: user_id } = user_data;
        if (data.image) {
            set_data.image = data.image;
        }
        if (data.cover_image) {
            set_data.cover_image = data.cover_image;
        }
        return set_data;
    }
    catch (err) {
        throw err;
    }
});
exports.edit_profile_data = edit_profile_data;
