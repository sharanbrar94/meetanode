"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.list_content = exports.contact_us = exports.logout = exports.change_password = exports.edit_profile = exports.view_my_profile = exports.set_new_password = exports.verify_otp = exports.forgot_password = exports.login = exports.social_login = exports.resend_otp = exports.email_verification = exports.signup = void 0;
const DAO = __importStar(require("../../DAO"));
const Models = __importStar(require("../../models"));
const user_services = __importStar(require("./user_services"));
const email_services = __importStar(require("./email_services"));
const index_1 = require("../../middlewares/index");
const signup = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { email, phone_no, language } = req.body;
        // verify email address
        let query_email = { email: email.toLowerCase() };
        let fetch_data = yield user_services.verify_user_info(query_email);
        if (fetch_data.length) {
            throw yield (0, index_1.handle_custom_error)('EMAIL_ALREADY_EXISTS', language);
        }
        else {
            // verify phone_no
            let query_phone_no = { phone_no: phone_no };
            let verify_data = yield user_services.verify_user_info(query_phone_no);
            if (verify_data.length) {
                throw yield (0, index_1.handle_custom_error)('PHONE_NO_ALREADY_EXISTS', language);
            }
            else {
                // create new user
                let create_user = yield user_services.set_user_data(req.body);
                let { _id } = create_user;
                // generate access token
                let generate_token = yield user_services.generate_user_token(_id, req.body);
                // fetch user response
                let response = yield user_services.make_user_response(generate_token, language);
                // send welcome email to user
                yield email_services.send_welcome_mail(create_user, language);
                // return response
                (0, index_1.handle_success)(res, response);
            }
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.signup = signup;
const email_verification = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { otp: input_otp, language } = req.body, { _id, otp } = req.user_data, session_data = req.session_data;
        if (otp == input_otp) {
            let query = { _id: _id };
            let update = { email_verified: true };
            let options = { new: true };
            yield DAO.find_and_update(Models.Users, query, update, options);
            // fetch user response
            let response = yield user_services.make_user_response(session_data, language);
            // return response
            (0, index_1.handle_success)(res, response);
        }
        else {
            throw yield (0, index_1.handle_custom_error)('WRONG_OTP', language);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.email_verification = email_verification;
const resend_otp = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { email, language } = req.body;
        let query_email = { email: email.toLowerCase() };
        let fetch_data = yield user_services.verify_user_info(query_email);
        if (fetch_data.length) {
            let { _id } = fetch_data[0];
            // update user otp
            let update_data = yield user_services.update_otp(_id);
            // generate access token
            let generate_token = yield user_services.generate_user_token(_id, req.body);
            // fetch user response
            let response = yield user_services.make_user_response(generate_token, language);
            // send email
            yield email_services.resend_otp_mail(update_data, language);
            // return response
            (0, index_1.handle_success)(res, response);
        }
        else {
            throw (0, index_1.handle_catch)('EMAIL_NOT_REGISTERED', language);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.resend_otp = resend_otp;
const social_login = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // console.log("---------------------req.body--",req.body)
        let { social_type, social_token, name, username, email, image, cover_image } = req.body;
        let language = "ENGLISH";
        if (social_type == undefined && social_token == undefined) {
            console.log("hello");
            throw ("Social type and social token is required");
        }
        let query = {
            social_type: social_type,
            social_token: social_token,
            is_deleted: false
        };
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Users, query, projection, options);
        if (fetch_data.length > 0) {
            var id = fetch_data[0]._id;
            // generate new token
            let generate_token = yield user_services.generate_user_token(id, req.body);
            // populate user_data
            let response = yield user_services.make_user_response(generate_token, language);
            // return response
            (0, index_1.handle_success)(res, response);
        }
        else {
            var response = yield user_services.create_new_user(req.body);
            let { _id } = response;
            // generate new token
            let generate_token = yield user_services.generate_user_token(_id, req.body);
            // populate user_data
            let populate_data = yield user_services.make_user_response(generate_token, language);
            // return response
            (0, index_1.handle_success)(res, populate_data);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.social_login = social_login;
const login = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { email, password } = req.body;
        let language = "ENGLISH";
        let query = { email: email.toLowerCase(), is_deleted: false };
        let projection = {};
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Users, query, projection, options);
        if (fetch_data.length) {
            let { _id, password: hash, is_blocked } = fetch_data[0];
            let decrypt = yield index_1.helpers.decrypt_password(password, hash);
            if (decrypt != true) {
                throw yield (0, index_1.handle_custom_error)('INCORRECT_PASSWORD', language);
            }
            if (is_blocked == true) {
                throw yield (0, index_1.handle_custom_error)('ACCOUNT_BLOCKED', language);
            }
            else {
                // generate access token
                let generate_token = yield user_services.generate_user_token(_id, req.body);
                // fetch user response
                let response = yield user_services.make_user_response(generate_token, language);
                // return response
                (0, index_1.handle_success)(res, response);
            }
        }
        else {
            throw yield (0, index_1.handle_custom_error)('EMAIL_NOT_REGISTERED', language);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.login = login;
const forgot_password = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { email } = req.body;
        let language = "ENGLISH";
        // verify email address
        let query_email = { email: email.toLowerCase() };
        let fetch_data = yield user_services.verify_user_info(query_email);
        if (fetch_data.length) {
            let { _id, full_name } = fetch_data[0];
            let unique_code = yield index_1.helpers.gen_unique_code();
            let query = { _id: _id };
            let update = { unique_code: unique_code };
            let options = { new: true };
            let update_data = yield DAO.find_and_update(Models.Users, query, update, options);
            // send email
            yield email_services.forgot_password_mail(update_data, language);
            let message = "Mail sent sucessfully";
            let response = { message: message };
            // return response
            (0, index_1.handle_success)(res, response);
        }
        else {
            throw yield (0, index_1.handle_custom_error)('EMAIL_NOT_REGISTERED', language);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.forgot_password = forgot_password;
const verify_otp = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { otp: input_otp, email } = req.body;
        let language = "ENGLISH";
        // verify email address
        let query_email = { email: email.toLowerCase() };
        let fetch_data = yield user_services.verify_user_info(query_email);
        if (fetch_data.length) {
            let { _id, fp_otp } = fetch_data[0];
            if (input_otp != fp_otp) {
                throw yield (0, index_1.handle_custom_error)('WRONG_OTP', language);
            }
            else {
                // generate unique code
                let unique_code = yield index_1.helpers.gen_unique_code();
                let query = { _id: _id };
                let update = { unique_code: unique_code };
                let options = { new: true };
                yield DAO.find_and_update(Models.Users, query, update, options);
                // generate new token
                // let generate_token = await user_services.generate_user_token(_id, req.body)
                // fetch user response
                // let response = await user_services.make_user_response(generate_token)
                let response = { unique_code: unique_code };
                // return response
                (0, index_1.handle_success)(res, response);
            }
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.verify_otp = verify_otp;
const set_new_password = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { password, unique_code, language } = req.body;
        let query = {
            $and: [
                { unique_code: { $ne: null } },
                { unique_code: unique_code }
            ]
        };
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Users, query, projection, options);
        if (fetch_data.length) {
            let { _id } = fetch_data[0];
            // bycryt password
            let bycryt_password = yield index_1.helpers.bcrypt_password(password);
            let query = { _id: _id };
            let update = {
                password: bycryt_password,
                unique_code: null,
                fp_otp: 0
            };
            let options = { new: true };
            yield DAO.find_and_update(Models.Users, query, update, options);
            // // generate new token
            // let generate_token = await user_services.generate_user_token(_id, req.body)
            // // fetch user response
            // let response = await user_services.make_user_response(generate_token)
            let message = "Password Changed Sucessfully";
            let response = { message: message };
            // return password
            (0, index_1.handle_success)(res, response);
        }
        else {
            throw yield (0, index_1.handle_custom_error)('NO_DATA_FOUND', language);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.set_new_password = set_new_password;
const view_my_profile = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.user_data;
        let response = yield user_services.get_user_profile(_id);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.view_my_profile = view_my_profile;
const edit_profile = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.user_data, session_data = req.session_data;
        let language = "ENGLISH";
        let query = { _id: _id };
        let update = yield user_services.edit_profile_data(req.body, req.user_data);
        let options = { new: true };
        yield DAO.find_and_update(Models.Users, query, update, options);
        // fetch response
        let response = yield user_services.make_user_response(session_data, language);
        // return respone
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.edit_profile = edit_profile;
const change_password = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { old_password, new_password, language } = req.body, { _id } = req.user_data, session_data = req.session_data;
        let query = { _id: _id };
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Users, query, projection, options);
        if (fetch_data.length) {
            let { _id, password } = fetch_data[0];
            let decrypt = yield index_1.helpers.decrypt_password(old_password, password);
            if (decrypt != true) {
                throw yield (0, index_1.handle_custom_error)('OLD_PASSWORD_MISMATCH', language);
            }
            else {
                // bycryt password
                let bycryt_password = yield index_1.helpers.bcrypt_password(new_password);
                let query = { _id: _id };
                let update = { password: bycryt_password };
                let options = { new: true };
                yield DAO.find_and_update(Models.Users, query, update, options);
                // fetch response
                let response = yield user_services.make_user_response(session_data, language);
                // return password
                (0, index_1.handle_success)(res, response);
            }
        }
        else {
            throw yield (0, index_1.handle_custom_error)('UNAUTHORIZED', language);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.change_password = change_password;
const logout = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.session_data;
        let query = { _id: _id };
        yield DAO.remove_data(Models.Sessions, query);
        let message = "Logout Sucessfull";
        let response = { message: message };
        // return response
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.logout = logout;
const contact_us = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { name, email, country_code, phone_no, message } = req.body, { _id } = req.session_data;
        let data_to_save = {
            user_id: _id,
            name: name,
            email: email.toLowerCase(),
            country_code: country_code,
            phone_no: phone_no,
            message: message,
            created_at: +new Date()
        };
        let response = yield DAO.save_data(Models.ContactUs, data_to_save);
        // return response
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.contact_us = contact_us;
const list_content = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { type } = req.query, query = {};
        if (type != undefined) {
            query.type = type;
        }
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Content, query, projection, options);
        // return data
        (0, index_1.handle_success)(res, fetch_data);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_content = list_content;
