"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const user_controller = __importStar(require("./user_controller"));
const authenticator_1 = __importDefault(require("../../middlewares/authenticator"));
const router = express_1.default.Router();
router.post("/email_verification", authenticator_1.default, user_controller.email_verification);
router.post("/resend_otp", user_controller.resend_otp);
router.post("/social_login", user_controller.social_login);
router.put("/forgot_password", user_controller.forgot_password);
router.post("/verify_otp", user_controller.verify_otp);
router.post("/set_new_password", user_controller.set_new_password);
router.get("/view_my_profile", authenticator_1.default, user_controller.view_my_profile);
router.put("/edit_profile", authenticator_1.default, user_controller.edit_profile);
router.put("/change_password", authenticator_1.default, user_controller.change_password);
router.put("/logout", authenticator_1.default, user_controller.logout);
router.post("/contact_us", authenticator_1.default, user_controller.contact_us);
router.get("/list_content", user_controller.list_content);
exports.default = router;
