"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.forgot_password_mail = exports.resend_otp_mail = exports.send_welcome_mail = void 0;
const DAO = __importStar(require("../../DAO"));
const Models = __importStar(require("../../models"));
const index_1 = require("../../middlewares/index");
const fetch_template = (data, type, language) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { type: type };
        let projection = { __v: 0 };
        let options = { lean: true };
        let populate = [{ path: 'variables', select: 'name' }];
        let fetch_data = yield DAO.populate_data(Models.Templates, query, projection, options, populate);
        if (fetch_data.length) {
            let { subject, html, variables } = fetch_data[0];
            let variable_names = [];
            if (variables.length) {
                for (let value of variables) {
                    console.log("---------value---", value);
                    variable_names.push(value.name);
                }
            }
            for (let key of variable_names) {
                console.log("---------key---", key);
                let data_key = key.toLowerCase();
                html = html.replace(`%${key}%`, data[data_key]);
            }
            return {
                subject: subject,
                html: html
            };
        }
        else {
            throw yield (0, index_1.handle_custom_error)('NO_DATA_FOUND', language);
        }
    }
    catch (err) {
        throw err;
    }
});
const send_welcome_mail = (data, language) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { name, email, otp } = data;
        let template_data = {
            user_name: name,
            otp: otp
        };
        let type = "WELCOME_EMAIL";
        let fetch_data = yield fetch_template(template_data, type, language);
        let { subject, html } = fetch_data;
        yield (0, index_1.send_email)(email, subject, html);
    }
    catch (err) {
        throw err;
    }
});
exports.send_welcome_mail = send_welcome_mail;
const resend_otp_mail = (data, language) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { name, email, otp } = data;
        let template_data = {
            user_name: name,
            otp: otp
        };
        let type = "RESEND_OTP_MAIL";
        let fetch_data = yield fetch_template(template_data, type, language);
        let { subject, html } = fetch_data;
        yield (0, index_1.send_email)(email, subject, html);
    }
    catch (err) {
        throw err;
    }
});
exports.resend_otp_mail = resend_otp_mail;
const forgot_password_mail = (data, language) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { email, unique_code, name } = data;
        // let url = `https://nftyit.com:3000/forgot-password?unique-code=${unique_code}`
        let type = "RESET_PASSWORD_MAIL";
        let template_data = {
            user_name: name,
            url: unique_code
        };
        let fetch_data = yield fetch_template(template_data, type, language);
        let { subject, html } = fetch_data;
        yield (0, index_1.send_email)(email, subject, html);
    }
    catch (err) {
        throw err;
    }
});
exports.forgot_password_mail = forgot_password_mail;
