"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addPass = exports.getEventDetail = exports.backup_db = exports.send_notification = exports.getEvents = exports.deleteCategory = exports.getCategories = exports.getCategoryDetail = exports.editCategory = exports.addCategory = exports.deleteRequirement = exports.getRequirements = exports.getRequirementDetail = exports.editRequirement = exports.addRequirement = exports.getAllEvents = exports.delete_templates = exports.list_templates = exports.add_edit_templates = exports.delete_variables = exports.list_variables = exports.add_edit_variables = exports.manage_contact_us = exports.add_contact_us = exports.list_contact_us = exports.delete_faqs = exports.list_faqs = exports.add_edit_faqs = exports.list_content = exports.add_edit_content = exports.delete_res_messages = exports.list_res_messages = exports.add_edit_res_msgs = exports.manage_products = exports.list_product_details = exports.list_products = exports.login_as_user = exports.manage_users = exports.list_user_details = exports.list_users = exports.mamage_staff_members = exports.staff_members_details = exports.list_staff_members = exports.add_edit_staff_members = exports.dashboard = exports.logout = exports.change_password = exports.edit_profile = exports.access_token_login = exports.login = void 0;
exports.deleteProgramPass = exports.deleteProgram = exports.addProgram = exports.deletePass = exports.getEventPasses = exports.editPass = void 0;
const DAO = __importStar(require("../../DAO/index"));
const Models = __importStar(require("../../models/index"));
const index_1 = require("../../middlewares/index");
const admin_services = __importStar(require("./admin_service"));
const moment_timezone_1 = __importDefault(require("moment-timezone"));
const index_helpers_1 = require("../../../helpers/index.helpers");
const index_functions_1 = require("./functions/index.functions");
let do_spaces_url = process.env.DO_SPACES_URL + 'backup/';
const login = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { email, password: input_password, language } = req.body;
        let query = { email: email, is_deleted: false };
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Admin, query, projection, options);
        if (fetch_data.length) {
            let { _id, password, is_blocked } = fetch_data[0];
            if (is_blocked == true) {
                throw yield (0, index_1.handle_custom_error)('ACCOUNT_BLOCKED', language);
            }
            else {
                let decrypt = yield index_1.helpers.decrypt_password(input_password, password);
                if (decrypt != true) {
                    throw yield (0, index_1.handle_custom_error)('INCORRECT_PASSWORD', language);
                }
                else {
                    // generate token 
                    let generate_token = yield admin_services.generate_admin_token(_id);
                    let response = yield admin_services.make_admin_response(generate_token, language);
                    // return response
                    (0, index_1.handle_success)(res, response);
                }
            }
        }
        else {
            throw yield (0, index_1.handle_custom_error)('NO_DATA_FOUND', language);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.login = login;
const access_token_login = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // return response
        (0, index_1.handle_success)(res, req.user_data);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.access_token_login = access_token_login;
const edit_profile = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { name, image } = req.body, { _id: admin_id } = req.user_data;
        let query = { _id: admin_id };
        let set_data = {};
        if (name != undefined) {
            set_data.name = name;
        }
        if (image != undefined) {
            set_data.image = image;
        }
        let options = { new: true };
        let response = yield DAO.find_and_update(Models.Admin, query, set_data, options);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.edit_profile = edit_profile;
const change_password = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { old_password, new_password, language } = req.body, { _id: admin_id } = req.user_data;
        let query = { _id: admin_id };
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Admin, query, projection, options);
        if (fetch_data.length) {
            let { _id, password } = fetch_data[0];
            let decrypt = yield index_1.helpers.decrypt_password(old_password, password);
            if (decrypt != true) {
                throw yield (0, index_1.handle_custom_error)('OLD_PASSWORD_MISMATCH', language);
            }
            else {
                // bycryt password
                let bycryt_password = yield index_1.helpers.bcrypt_password(new_password);
                let query = { _id: _id };
                let update = { password: bycryt_password };
                let options = { new: true };
                let response = yield DAO.find_and_update(Models.Admin, query, update, options);
                // return password
                (0, index_1.handle_success)(res, response);
            }
        }
        else {
            throw yield (0, index_1.handle_custom_error)('UNAUTHORIZED', language);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.change_password = change_password;
const logout = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id: admin_id } = req.user_data;
        let query = { type: "ADMIN", admin_id: admin_id };
        yield DAO.remove_data(Models.Sessions, query);
        // return response
        (0, index_1.handle_success)(res, {});
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.logout = logout;
const dashboard = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // fetch user count
        let query = { is_deleted: false };
        let total_users = yield admin_services.fetch_total_count(Models.Users, query);
        // fetch products count
        let total_products = yield admin_services.fetch_total_count(Models.Products, query);
        // fetch recent users
        let recent_users = yield admin_services.fetch_recent_users();
        let response = {
            total_users: total_users,
            total_products: total_products,
            recent_users: recent_users
        };
        // return response
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.dashboard = dashboard;
const add_edit_staff_members = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, language } = req.body, response;
        if (_id != undefined) {
            let query = { _id: _id };
            let set_data = yield admin_services.set_staff_data(req.body, language, 'UPDATE');
            let options = { new: true };
            response = yield DAO.find_and_update(Models.Admin, query, set_data, options);
        }
        else {
            let set_data = yield admin_services.set_staff_data(req.body, language, 'ADD');
            set_data.created_at = +new Date();
            response = yield DAO.save_data(Models.Admin, set_data);
        }
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.add_edit_staff_members = add_edit_staff_members;
const list_staff_members = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { search, pagination, limit } = req.query;
        let query = { email: { $ne: "admin@gmail.com" }, is_deleted: false };
        if (search != undefined) {
            query.$or = [
                { name: { $regex: search, $options: 'i' } },
                { email: { $regex: search, $options: 'i' } }
            ];
        }
        let projection = { __v: 0 };
        let options = yield index_1.helpers.set_options(pagination, limit);
        let fetch_data = yield DAO.get_data(Models.Admin, query, projection, options);
        // fetch total count
        // let query_email = { email: { $ne: "admin@gmail.com" } }
        let total_count = yield admin_services.fetch_total_count(Models.Admin, query);
        let response = {
            total_count: total_count,
            data: fetch_data
        };
        // return response
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_staff_members = list_staff_members;
const staff_members_details = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.query;
        let query = { _id: _id };
        let options = { lean: true };
        let projection = { __v: 0 };
        let response = yield DAO.get_data(Models.Admin, query, projection, options);
        // return response
        (0, index_1.handle_success)(res, response[0]);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.staff_members_details = staff_members_details;
const mamage_staff_members = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let collection = Models.Admin;
        let update_data = yield admin_services.block_delete_data(req.body, collection);
        // return response
        (0, index_1.handle_success)(res, update_data);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.mamage_staff_members = mamage_staff_members;
const list_users = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { search, pagination, limit } = req.query;
        let query = { is_deleted: false };
        if (search != undefined) {
            query.$or = [
                { name: { $regex: search, $options: 'i' } },
                { email: { $regex: search, $options: 'i' } }
            ];
        }
        let options = yield index_1.helpers.set_options(pagination, limit);
        let fetch_data = yield admin_services.fetch_user_data(query, options);
        // fetch total count
        let total_count = yield admin_services.fetch_total_count(Models.Users, query);
        let response = {
            total_count: total_count,
            data: fetch_data
        };
        // return response
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_users = list_users;
const list_user_details = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.query;
        let response = yield admin_services.fetch_user_detail(_id);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_user_details = list_user_details;
const manage_users = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { type, language } = req.body, fetch_data;
        if (type == 'BLOCK/DELETE') {
            fetch_data = yield admin_services.block_delete_data(req.body, Models.Users);
        }
        else if (type == 'VERIFY/UNVERIFY') {
            fetch_data = yield admin_services.verify_unverify(req.body);
        }
        else if (type == 'ACTIVATE/DEACTIVATE') {
            fetch_data = yield admin_services.activate_deactivate(req.body);
        }
        else {
            throw yield (0, index_1.handle_custom_error)('INVALID_OBJECT_ID', language);
        }
        if (fetch_data) {
            let { _id } = fetch_data;
            let query = { _id: _id };
            let options = { lean: true };
            let response = yield admin_services.fetch_user_data(query, options);
            // return response
            (0, index_1.handle_success)(res, response[0]);
        }
        else {
            throw yield (0, index_1.handle_custom_error)('INVALID_OBJECT_ID', language);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.manage_users = manage_users;
const login_as_user = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, language } = req.body;
        let query = { _id: _id, is_deleted: false };
        let projection = {};
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Users, query, projection, options);
        if (fetch_data.length) {
            // generate access token
            let generate_token = yield admin_services.generate_user_token(_id, req.body);
            // fetch user response
            let response = yield admin_services.make_user_response(generate_token);
            // return response
            (0, index_1.handle_success)(res, response);
        }
        else {
            throw yield (0, index_1.handle_custom_error)('INVALID_OBJECT_ID', language);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.login_as_user = login_as_user;
/*==============Event module=============*/
const getAllEvents = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { page, limit } = req.query, query = {};
        // if (search != undefined) { query.name = { $regex: search, $options: 'i' } }
        let projection = { __v: 0 };
        let options = yield index_helpers_1.common_helpers.set_options(page, limit);
        let fetch_data = yield DAO.get_data(Models.Events, query, projection, options);
        if (fetch_data.length) {
            for (let value of fetch_data) {
                value.total_event_attendees = 0;
                let event_id = value._id;
                let getAttendees = yield admin_services.fetch_total_event_attendees(event_id);
                if (getAttendees.length) {
                    value.total_event_attendees = getAttendees.length;
                }
            }
        }
        let total_count = yield admin_services.fetch_total_count(Models.Events, query);
        let response = {
            total_count: total_count,
            events: fetch_data
        };
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getAllEvents = getAllEvents;
const list_products = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, search, start_date, end_date, timezone, pagination, limit } = req.query;
        let set_start_date = null, set_end_date = null;
        let time_zone = 'Asia/Kolkata';
        if (timezone) {
            time_zone = timezone;
        }
        if (start_date) {
            set_start_date = (0, moment_timezone_1.default)(start_date, "DD/MM/YYYY").tz(time_zone).startOf('day').format('x');
        }
        if (set_end_date) {
            set_end_date = (0, moment_timezone_1.default)(end_date, "DD/MM/YYYY").tz(time_zone).endOf('day').format('x');
        }
        let query = { is_deleted: false };
        if (_id != undefined) {
            query._id = _id;
        }
        if (search != undefined) {
            query.name = { $regex: search, $options: 'i' };
        }
        if (_id != undefined) {
            query._id = _id;
        }
        if (set_start_date != null && set_end_date != null) {
            query.$and = [
                { created_at: { $gte: set_start_date } },
                { created_at: { $lte: set_end_date } }
            ];
        }
        let options = yield index_1.helpers.set_options(pagination, limit);
        let fetch_data = yield admin_services.make_products_response(query, options);
        // fetch total count
        let total_count = yield admin_services.fetch_total_count(Models.Products, query);
        let response = {
            total_count: total_count,
            data: fetch_data
        };
        // return data
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_products = list_products;
const list_product_details = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.query;
        let query = { _id: _id };
        let options = { lean: true };
        let response = yield admin_services.make_products_response(query, options);
        // return response
        (0, index_1.handle_success)(res, response[0]);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_product_details = list_product_details;
const manage_products = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let collection = Models.Products;
        let update_data = yield admin_services.block_delete_data(req.body, collection);
        let { _id } = update_data;
        let query = { _id: _id };
        let options = { lean: true };
        let response = yield admin_services.make_products_response(query, options);
        // return response
        (0, index_1.handle_success)(res, response[0]);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.manage_products = manage_products;
const add_edit_res_msgs = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, type, status_code, message_type, msg_in_english, msg_in_arabic } = req.body, response;
        let set_data = {
            type: type,
            status_code: status_code,
            message_type: message_type.toUpperCase(),
            msg_in_english: msg_in_english,
            msg_in_arabic: msg_in_arabic
        };
        if (_id != undefined) {
            let query = { _id: _id };
            let options = { new: true };
            response = yield DAO.find_and_update(Models.ResMessages, query, set_data, options);
        }
        else {
            set_data.created_at = +new Date();
            response = yield DAO.save_data(Models.ResMessages, set_data);
        }
        // return response
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.add_edit_res_msgs = add_edit_res_msgs;
const list_res_messages = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { search, pagination, limit } = req.query;
        let query = {};
        if (search != undefined) {
            query.$or = [
                { type: { $regex: search, $options: 'i' } },
                { message_type: { $regex: search, $options: 'i' } },
                { msg_in_english: { $regex: search, $options: 'i' } },
                { msg_in_arabic: { $regex: search, $options: 'i' } }
            ];
        }
        let projection = { __v: 0 };
        let options = yield index_1.helpers.set_options(pagination, limit);
        let fetch_data = yield DAO.get_data(Models.ResMessages, query, projection, options);
        // fetch total count
        let total_count = yield admin_services.fetch_total_count(Models.ResMessages, query);
        let response = {
            total_count: total_count,
            data: fetch_data
        };
        // return response
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_res_messages = list_res_messages;
const delete_res_messages = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.params;
        let query = { _id: _id };
        let response = yield DAO.remove_many(Models.ResMessages, query);
        if (response.deletedCount > 0) {
            let data = { message: `Response message deleted successfully...` };
            (0, index_1.handle_success)(res, data);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.delete_res_messages = delete_res_messages;
const add_edit_content = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { type, description } = req.body, response;
        let set_data = {
            type: type,
            description: description
        };
        // check already added or not
        let fetch_content = yield admin_services.check_content(type);
        if (fetch_content.length) {
            let { _id } = fetch_content[0];
            let query = { _id: _id };
            let options = { new: true };
            response = yield DAO.find_and_update(Models.Content, query, set_data, options);
        }
        else {
            set_data.created_at = +new Date();
            response = yield DAO.save_data(Models.Content, set_data);
        }
        // return response
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.add_edit_content = add_edit_content;
const list_content = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { type } = req.query, query = {};
        if (type != undefined) {
            query.type = type;
        }
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Content, query, projection, options);
        // return data
        (0, index_1.handle_success)(res, fetch_data);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_content = list_content;
const add_edit_faqs = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, question, answer } = req.body, response;
        let set_data = {
            question: question,
            answer: answer
        };
        if (_id != undefined) {
            let query = { _id: _id };
            let options = { new: true };
            response = yield DAO.find_and_update(Models.Faqs, query, set_data, options);
        }
        else {
            set_data.created_at = +new Date();
            response = yield DAO.save_data(Models.Faqs, set_data);
        }
        // return data
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.add_edit_faqs = add_edit_faqs;
const list_faqs = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, pagination, limit } = req.query, query = {};
        if (_id != undefined) {
            query._id = _id;
        }
        let projection = { __v: 0 };
        let options = yield index_1.helpers.set_options(pagination, limit);
        let fetch_data = yield DAO.get_data(Models.Faqs, query, projection, options);
        // fetch total count
        let total_count = yield admin_services.fetch_total_count(Models.Faqs, query);
        let response = {
            total_count: total_count,
            data: fetch_data
        };
        // return data
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_faqs = list_faqs;
const delete_faqs = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.params;
        let query = { _id: _id };
        let response = yield DAO.remove_many(Models.Faqs, query);
        if (response.deletedCount > 0) {
            let data = { message: `Faq deleted successfully...` };
            (0, index_1.handle_success)(res, data);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.delete_faqs = delete_faqs;
const add_edit_variables = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, name } = req.body, response;
        let set_data = { name: name };
        if (_id != undefined) {
            let query = { _id: _id };
            let options = { new: true };
            response = yield DAO.find_and_update(Models.Variables, query, set_data, options);
        }
        else {
            set_data.created_at = +new Date();
            response = yield DAO.save_data(Models.Variables, set_data);
        }
        // return data
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.add_edit_variables = add_edit_variables;
const list_variables = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, search, pagination, limit } = req.query, query = {};
        if (_id != undefined) {
            query._id = _id;
        }
        if (search != undefined) {
            query.name = { $regex: search, $options: 'i' };
        }
        let projection = { __v: 0 };
        let options = yield index_1.helpers.set_options(pagination, limit);
        let fetch_data = yield DAO.get_data(Models.Variables, query, projection, options);
        // fetch total count
        let total_count = yield admin_services.fetch_total_count(Models.Variables, query);
        let response = {
            total_count: total_count,
            data: fetch_data
        };
        // return data
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_variables = list_variables;
const delete_variables = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.params;
        let query = { _id: _id };
        let response = yield DAO.remove_many(Models.Variables, query);
        if (response.deletedCount > 0) {
            let data = { message: `Variable deleted successfully...` };
            (0, index_1.handle_success)(res, data);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.delete_variables = delete_variables;
const add_edit_templates = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, title, subject, html, variables } = req.body, response;
        let set_data = {
            title: title,
            type: title.toUpperCase(),
            subject: subject,
            html: html,
            variables: variables
        };
        if (_id != undefined) {
            let query = { _id: _id };
            let options = { new: true };
            response = yield DAO.find_and_update(Models.Templates, query, set_data, options);
        }
        else {
            set_data.created_at = +new Date();
            response = yield DAO.save_data(Models.Templates, set_data);
        }
        // return data
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.add_edit_templates = add_edit_templates;
const list_templates = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, search, pagination, limit } = req.query, query = {};
        if (_id != undefined) {
            query._id = _id;
        }
        if (search != undefined) {
            query.$and = [
                { title: { $regex: search, $options: 'i' } },
                { short_content: { $regex: search, $options: 'i' } },
                { subject: { $regex: search, $options: 'i' } }
            ];
        }
        let projection = { __v: 0 };
        let options = yield index_1.helpers.set_options(pagination, limit);
        let populate = [{ path: 'variables', select: 'name' }];
        let fetch_data = yield DAO.populate_data(Models.Templates, query, projection, options, populate);
        // fetch total count
        let total_count = yield admin_services.fetch_total_count(Models.Templates, query);
        let response = {
            total_count: total_count,
            data: fetch_data
        };
        // return data
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_templates = list_templates;
const delete_templates = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.params;
        let query = { _id: _id };
        let response = yield DAO.remove_many(Models.Templates, query);
        if (response.deletedCount > 0) {
            let data = { message: `Template deleted successfully...` };
            (0, index_1.handle_success)(res, data);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.delete_templates = delete_templates;
/*=================Requirement CRUD===========*/
const addRequirement = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { name } = req.body;
        let set_data = {
            name: name,
            created_at: +new Date()
        };
        let response = yield DAO.save_data(Models.Requirements, set_data);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.addRequirement = addRequirement;
const editRequirement = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, name } = req.body;
        let set_data = {
            name: name
        };
        let query = { _id: _id };
        let options = { new: true };
        let response = yield DAO.find_and_update(Models.Requirements, query, set_data, options);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.editRequirement = editRequirement;
const getRequirementDetail = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.query;
        let query = { _id: _id };
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Requirements, query, projection, options);
        (0, index_1.handle_success)(res, fetch_data[0]);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getRequirementDetail = getRequirementDetail;
const getRequirements = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { page, limit, search } = req.query, query = {};
        query = { is_deleted: false };
        if (search != undefined) {
            query.name = { $regex: search, $options: 'i' };
        }
        let options = yield index_helpers_1.common_helpers.set_options(page, limit);
        let projection = { __v: 0 };
        let fetch_data = yield DAO.get_data(Models.Requirements, query, projection, options);
        (0, index_1.handle_success)(res, fetch_data);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getRequirements = getRequirements;
const deleteRequirement = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.body;
        let set_data = {};
        let query = { _id: _id };
        let options = { new: true };
        set_data.is_deleted = true;
        let response = yield DAO.find_and_update(Models.Requirements, query, set_data, options);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.deleteRequirement = deleteRequirement;
/*================Category CRUD=============*/
const addCategory = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { name } = req.body;
        let set_data = {
            name: name,
            created_at: +new Date()
        };
        let response = yield DAO.save_data(Models.Categories, set_data);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.addCategory = addCategory;
const editCategory = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, name } = req.body;
        let set_data = {
            name: name
        };
        let query = { _id: _id };
        let options = { new: true };
        let response = yield DAO.find_and_update(Models.Categories, query, set_data, options);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.editCategory = editCategory;
const getCategoryDetail = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.query;
        let query = { _id: _id };
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Categories, query, projection, options);
        (0, index_1.handle_success)(res, fetch_data[0]);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getCategoryDetail = getCategoryDetail;
const getCategories = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { page, limit, search } = req.query, query = {};
        query = { is_deleted: false };
        if (search != undefined) {
            query.name = { $regex: search, $options: 'i' };
        }
        let options = yield index_helpers_1.common_helpers.set_options(page, limit);
        let projection = { __v: 0 };
        let fetch_data = yield DAO.get_data(Models.Categories, query, projection, options);
        (0, index_1.handle_success)(res, fetch_data);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getCategories = getCategories;
const deleteCategory = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.body;
        let set_data = {};
        let query = { _id: _id };
        let options = { new: true };
        set_data.is_deleted = true;
        let response = yield DAO.find_and_update(Models.Categories, query, set_data, options);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.deleteCategory = deleteCategory;
/*==============Event module===========*/
const getEvents = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { pagination, limit, search } = req.query, query = {};
        if (search != undefined) {
            query.event_name = { $regex: search, $options: 'i' };
        }
        let projection = { __v: 0 };
        let options = yield index_helpers_1.common_helpers.set_options(pagination, limit);
        let fetch_data = yield DAO.get_data(Models.Events, query, projection, options);
        if (fetch_data.length) {
            for (let value of fetch_data) {
                value.total_event_attendees = 0;
                let event_id = value._id;
                let getAttendees = yield admin_services.fetch_total_event_attendees(event_id);
                if (getAttendees.length) {
                    value.total_event_attendees = getAttendees.length;
                }
            }
        }
        let total_count = yield admin_services.fetch_total_count(Models.Events, query);
        let response = {
            total_count: total_count,
            events: fetch_data
        };
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getEvents = getEvents;
const getEventDetail = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id } = req.query;
        let language = "ENGLISH";
        let response = yield admin_services.event_detail(event_id, language);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getEventDetail = getEventDetail;
/*=============Ticket detail module========*/
const addPass = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id, name, price } = req.body;
        let set_data = {
            event_id: event_id,
            name: name,
            price: price,
            created_at: +new Date()
        };
        let response = yield DAO.save_data(Models.EventPasses, set_data);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.addPass = addPass;
const editPass = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, name, price } = req.body;
        let set_data = {
            name: name,
            price: price
        };
        let query = { _id: _id };
        let options = { new: true };
        let response = yield DAO.find_and_update(Models.EventPasses, query, set_data, options);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.editPass = editPass;
const getEventPasses = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id, page, limit } = req.query, query = {};
        query = { event_id: event_id };
        let options = yield index_helpers_1.common_helpers.set_options(page, limit);
        let projection = { __v: 0 };
        let fetch_data = yield DAO.get_data(Models.EventPasses, query, projection, options);
        (0, index_1.handle_success)(res, fetch_data);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getEventPasses = getEventPasses;
const deletePass = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.body;
        let query = { _id: _id };
        let response = yield DAO.remove_data(Models.EventPasses, query);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.deletePass = deletePass;
const addProgram = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id, program_name, passes } = req.body;
        let set_data = {
            event_id: event_id,
            program_name: program_name,
            created_at: +new Date()
        };
        let response = yield DAO.save_data(Models.EventPrograms, set_data);
        if (passes) {
            let reqQuery = { program_id: response._id };
            yield DAO.remove_many(Models.ProgramPasses, reqQuery);
            yield passes.forEach((x) => __awaiter(void 0, void 0, void 0, function* () {
                let program_id = response._id;
                let data_to_save = {
                    pass_id: x.pass_id,
                    program_id: program_id,
                    created_at: +new Date()
                };
                yield DAO.save_data(Models.ProgramPasses, data_to_save);
            }));
        }
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.addProgram = addProgram;
const deleteProgram = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id } = req.body;
        let query = { _id: _id };
        let response = yield DAO.remove_data(Models.EventPrograms, query);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.deleteProgram = deleteProgram;
const deleteProgramPass = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { program_id, pass_id } = req.body;
        let query = { program_id: program_id, pass_id: pass_id };
        let response = yield DAO.remove_data(Models.ProgramPasses, query);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.deleteProgramPass = deleteProgramPass;
/*===========notification module==========*/
const send_notification = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { type } = req.body;
        if (type == 'EMAIL') {
            yield index_functions_1.email_functions.send_broadcast_email(req.body);
        }
        else {
            yield index_functions_1.notification_functions.send_broadcast_push(req.body);
        }
        let response = "Notification sent sucessfully!!!";
        // return data
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.send_notification = send_notification;
/*==============Contact us module============*/
const add_contact_us = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { name, email, country_code, phone_number, message } = req.body;
        let set_data = {
            name: name,
            email: email,
            country_code: country_code,
            phone_number: phone_number,
            message: message,
            created_at: +new Date()
        };
        let response = yield DAO.save_data(Models.ContactUs, set_data);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.add_contact_us = add_contact_us;
const list_contact_us = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { search, pagination, limit } = req.query;
        let query = { is_deleted: false };
        if (search != null || search != undefined) {
            query.$or = [
                { name: { $regex: search, $options: 'i' } },
                { email: { $regex: search, $options: 'i' } }
            ];
        }
        let projection = { __v: 0 };
        let options = yield index_1.helpers.set_options(pagination, limit);
        let fetch_data = yield DAO.get_data(Models.ContactUs, query, projection, options);
        // fetch total orders
        let total_count = yield admin_services.fetch_total_count(Models.ContactUs, query);
        let response = {
            total_count: total_count,
            data: fetch_data
        };
        // return data
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.list_contact_us = list_contact_us;
const manage_contact_us = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, type } = req.body, response;
        let update = {};
        if (type == 'RESOLVE') {
            update = { resolved: true };
        }
        else if (type == 'DELETE') {
            update = { is_deleted: true };
        }
        let query = { _id: _id };
        let options = { new: true };
        response = yield DAO.find_and_update(Models.ContactUs, query, update, options);
        // return data
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.manage_contact_us = manage_contact_us;
const backup_db = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { language } = req.body;
        let create_backup = yield index_functions_1.backup_functions.backup_using_cron(language);
        let { file_url } = create_backup;
        let response = {
            base_url: do_spaces_url,
            folders: ['backup'],
            file_name: file_url
        };
        // return data
        yield (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.backup_db = backup_db;
