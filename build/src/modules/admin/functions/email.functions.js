"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.send_broadcast_email = void 0;
const index_helpers_1 = require("../../../../helpers/index.helpers");
const DAO = __importStar(require("../../../DAO/index"));
const Models = __importStar(require("../../../models"));
const fetch_users = (query) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let projection = { __v: 0 };
        let options = { lean: true, sort: { _id: -1 } };
        let fetch_data = yield DAO.get_data(Models.Users, query, projection, options);
        let user_ids = [];
        if (fetch_data.length) {
            for (let value of fetch_data) {
                user_ids.push(value._id);
            }
        }
        return user_ids;
    }
    catch (err) {
        throw err;
    }
});
const send_broadcast_email = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { send_to, user_ids, subject, message } = data;
        let users;
        if (send_to == 'ALL_USERS') {
            let query = { is_deleted: false };
            users = yield fetch_users(query);
        }
        else {
            let query = { _id: { $in: user_ids }, is_deleted: false };
            users = yield fetch_users(query);
        }
        if (users.length) {
            for (let value of users) {
                let { email } = value;
                yield (0, index_helpers_1.send_email)(email, subject, message);
            }
        }
    }
    catch (err) {
        throw err;
    }
});
exports.send_broadcast_email = send_broadcast_email;
