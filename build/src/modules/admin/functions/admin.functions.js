"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.make_products_response = exports.set_staff_data = exports.verify_admin_info = exports.check_content = exports.fetch_user_data = exports.make_user_response = exports.generate_user_token = exports.fetch_recent_users = exports.fetch_total_count = exports.activate_deactivate = exports.verify_unverify = exports.block_delete_data = exports.make_admin_response = exports.update_language = exports.save_session_data = exports.fetch_admin_token = exports.generate_admin_token = void 0;
const DAO = __importStar(require("../../../DAO/index"));
const Models = __importStar(require("../../../models"));
const index_1 = require("../../../middlewares/index");
const index_2 = require("../../../config/index");
const Moment = require('moment-timezone');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const salt_rounds = index_2.app_constant.salt_rounds;
const admin_scope = index_2.app_constant.scope.admin;
const user_scope = index_2.app_constant.scope.user;
const generate_admin_token = (_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let token_data = {
            _id: _id,
            scope: admin_scope,
            collection: Models.Admin,
            token_gen_at: +new Date()
        };
        let response = yield fetch_admin_token(token_data);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.generate_admin_token = generate_admin_token;
const fetch_admin_token = (token_data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let access_token = yield (0, index_1.generate_token)(token_data);
        let response = yield save_session_data(access_token, token_data);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.fetch_admin_token = fetch_admin_token;
const save_session_data = (access_token, token_data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id: admin_id, token_gen_at } = token_data;
        let set_data = {
            type: "ADMIN",
            admin_id: admin_id,
            access_token: access_token,
            token_gen_at: token_gen_at,
            created_at: +new Date()
        };
        let response = yield DAO.save_data(Models.Sessions, set_data);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.save_session_data = save_session_data;
const update_language = (_id, language) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { _id: _id };
        let update = { language: language };
        let options = { new: true };
        yield DAO.find_and_update(Models.Admin, query, update, options);
    }
    catch (err) {
        throw err;
    }
});
exports.update_language = update_language;
const make_admin_response = (data, language) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { admin_id, access_token, token_gen_at } = data;
        let query = { _id: admin_id };
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Admin, query, projection, options);
        if (fetch_data.length) {
            fetch_data[0].access_token = access_token;
            fetch_data[0].token_gen_at = token_gen_at;
            return fetch_data[0];
        }
        else {
            throw yield (0, index_1.handle_custom_error)('UNAUTHORIZED', language);
        }
    }
    catch (err) {
        throw err;
    }
});
exports.make_admin_response = make_admin_response;
const block_delete_data = (data, collection) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, is_blocked, is_deleted } = data;
        let query = { _id: _id };
        let data_to_update = {};
        if (typeof is_blocked !== "undefined" && is_blocked !== null) {
            data_to_update.is_blocked = is_blocked;
        }
        if (typeof is_deleted !== "undefined" && is_deleted !== null) {
            data_to_update.is_deleted = is_deleted;
        }
        let options = { new: true };
        let response = yield DAO.find_and_update(collection, query, data_to_update, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.block_delete_data = block_delete_data;
const verify_unverify = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, admin_verified } = data;
        let query = { _id: _id };
        let update = {};
        if (typeof admin_verified !== "undefined" && admin_verified !== null) {
            update.admin_verified = admin_verified;
        }
        let options = { new: true };
        let response = yield DAO.find_and_update(Models.Users, query, update, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.verify_unverify = verify_unverify;
const activate_deactivate = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id, account_status } = data;
        let query = { _id: _id };
        let update = { account_status: account_status };
        let options = { new: true };
        let response = yield DAO.find_and_update(Models.Users, query, update, options);
        // remove login details
        if (account_status == 'DEACTIVATED') {
            let query = { user_id: _id };
            yield DAO.remove_many(Models.Sessions, query);
        }
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.activate_deactivate = activate_deactivate;
const fetch_total_count = (collection, query) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let response = yield DAO.count_data(collection, query);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.fetch_total_count = fetch_total_count;
const fetch_recent_users = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { is_deleted: false };
        let projection = { __v: 0, password: 0, otp: 0, fp_otp: 0 };
        let options = { lean: true, sort: { _id: -1 }, limit: 20 };
        let response = yield DAO.get_data(Models.Users, query, projection, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.fetch_recent_users = fetch_recent_users;
const generate_user_token = (_id, req_data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let token_data = {
            _id: _id,
            scope: user_scope,
            collection: Models.Users,
            token_gen_at: +new Date()
        };
        let response = yield fetch_user_token(token_data, req_data);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.generate_user_token = generate_user_token;
const fetch_user_token = (token_data, req_data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let access_token = yield (0, index_1.generate_token)(token_data);
        let response = yield save_user_session_data(access_token, token_data, req_data);
        return response;
    }
    catch (err) {
        throw err;
    }
});
const save_user_session_data = (access_token, token_data, req_data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id: user_id, token_gen_at } = token_data, { device_type, fcm_token } = req_data;
        let set_data = {
            type: "USER",
            user_id: user_id,
            access_token: access_token,
            token_gen_at: token_gen_at,
            created_at: +new Date()
        };
        if (device_type != null || device_type != undefined) {
            set_data.device_type = device_type;
        }
        if (fcm_token != null || fcm_token != undefined) {
            set_data.fcm_token = fcm_token;
        }
        let response = yield DAO.save_data(Models.Sessions, set_data);
        return response;
    }
    catch (err) {
        throw err;
    }
});
const make_user_response = (token_data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { user_id, access_token, device_type, fcm_token, language, token_gen_at } = token_data;
        let query = { _id: user_id };
        let projection = { __v: 0, password: 0, otp: 0, fp_otp: 0, unique_code: 0 };
        let options = { lean: true };
        let response = yield DAO.get_data(Models.Users, query, projection, options);
        if (response.length) {
            response[0].access_token = access_token;
            response[0].device_type = device_type;
            response[0].fcm_token = fcm_token;
            response[0].token_gen_at = token_gen_at;
            return response[0];
        }
        else {
            throw yield (0, index_1.handle_custom_error)('INVALID_OBJECT_ID', language);
        }
    }
    catch (err) {
        throw err;
    }
});
exports.make_user_response = make_user_response;
const fetch_user_data = (query, options) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let projection = { password: 0, otp: 0, fp_otp: 0, unique_code: 0, __v: 0 };
        let response = yield DAO.get_data(Models.Users, query, projection, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.fetch_user_data = fetch_user_data;
const check_content = (type) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { type: type };
        let projection = { __v: 0 };
        let options = { lean: true };
        return yield DAO.get_data(Models.Content, query, projection, options);
    }
    catch (err) {
        throw err;
    }
});
exports.check_content = check_content;
const verify_admin_info = (query) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let projection = { __v: 0 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Admin, query, projection, options);
        return fetch_data;
    }
    catch (err) {
        throw err;
    }
});
exports.verify_admin_info = verify_admin_info;
const set_staff_data = (data, language, type) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let set_data = {
            name: data.name,
            roles: data.roles
        };
        if (data.password) {
            let hassed_password = yield index_1.helpers.bcrypt_password(data.password);
            set_data.password = hassed_password;
        }
        if (data.image) {
            set_data.image = data.image;
        }
        if (data.email) {
            // check other email
            let email = data.email.toLowerCase();
            if (type == 'UPDATE') {
                let query = { _id: { $ne: data._id }, email: email };
                let fetch_data = yield verify_admin_info(query);
                if (fetch_data.length) {
                    throw yield (0, index_1.handle_custom_error)('EMAIL_ALREADY_EXISTS', language);
                }
                else {
                    set_data.email = email;
                }
            }
            else {
                let query = { email: email };
                let fetch_data = yield verify_admin_info(query);
                if (fetch_data.length) {
                    throw yield (0, index_1.handle_custom_error)('EMAIL_ALREADY_EXISTS', language);
                }
                else {
                    set_data.email = email;
                }
            }
        }
        return set_data;
    }
    catch (err) {
        throw err;
    }
});
exports.set_staff_data = set_staff_data;
const make_products_response = (query, options) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let projection = { __v: 0 };
        let populate = [
            { path: 'added_by', select: 'profile_pic name' }
        ];
        let respone = yield DAO.populate_data(Models.Products, query, projection, options, populate);
        return respone;
    }
    catch (err) {
        throw err;
    }
});
exports.make_products_response = make_products_response;
