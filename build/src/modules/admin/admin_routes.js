"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const admin_controller = __importStar(require("./admin_controller"));
const authenticator_1 = __importDefault(require("../../middlewares/authenticator"));
const router = express_1.default.Router();
router.post("/login", admin_controller.login);
router.get("/access_token_login", authenticator_1.default, admin_controller.access_token_login);
router.put("/edit_profile", authenticator_1.default, admin_controller.edit_profile);
router.put("/change_password", authenticator_1.default, admin_controller.change_password);
router.put("/logout", authenticator_1.default, admin_controller.logout);
router.get("/dashboard", authenticator_1.default, admin_controller.dashboard);
router.post("/staff_members/add_edit", authenticator_1.default, admin_controller.add_edit_staff_members);
router.get("/staff_members/listing", authenticator_1.default, admin_controller.list_staff_members);
router.get("/staff_members/details", authenticator_1.default, admin_controller.staff_members_details);
router.put("/staff_members/block_delete", authenticator_1.default, admin_controller.mamage_staff_members);
//user module
router.get("/users/listing", authenticator_1.default, admin_controller.list_users);
router.get("/users/details", authenticator_1.default, admin_controller.list_user_details);
router.put("/users/manage_users", authenticator_1.default, admin_controller.manage_users);
router.post("/users/login_as_user", authenticator_1.default, admin_controller.login_as_user);
//static data module
router.post("/requirement", authenticator_1.default, admin_controller.addRequirement);
router.put("/requirement", authenticator_1.default, admin_controller.editRequirement);
router.get("/requirement", authenticator_1.default, admin_controller.getRequirementDetail);
router.get("/requirements", authenticator_1.default, admin_controller.getRequirements);
router.post("/delete-requirement", authenticator_1.default, admin_controller.deleteRequirement);
router.post("/category", authenticator_1.default, admin_controller.addCategory);
router.put("/category", authenticator_1.default, admin_controller.editCategory);
router.get("/category", authenticator_1.default, admin_controller.getCategoryDetail);
router.get("/categories", authenticator_1.default, admin_controller.getCategories);
router.post("/delete-category", authenticator_1.default, admin_controller.deleteCategory);
//event module
router.get("/events", authenticator_1.default, admin_controller.getEvents);
router.get("/event", authenticator_1.default, admin_controller.getEventDetail);
//router.get("/event-approve-disapprove", authenticator, admin_controller.changeEventStatus)
//ticket detail module
router.post("/pass", authenticator_1.default, admin_controller.addPass);
router.put("/pass", authenticator_1.default, admin_controller.editPass);
router.get("/pass", authenticator_1.default, admin_controller.getEventPasses);
router.delete("/pass", authenticator_1.default, admin_controller.deletePass);
router.post("/program", authenticator_1.default, admin_controller.addProgram);
router.delete("/program", authenticator_1.default, admin_controller.deleteProgram);
router.delete("/program-pass", authenticator_1.default, admin_controller.deleteProgramPass);
//router.get("/pass", authenticator, admin_controller.getEventPasses)
//router.delete("/pass", authenticator, admin_controller.deletePass)
//notification module
router.post("/notification/broadcast", authenticator_1.default, admin_controller.send_notification);
//contact us module
router.post("/contact_us/add", authenticator_1.default, admin_controller.add_contact_us);
router.get("/contact_us/listing", authenticator_1.default, admin_controller.list_contact_us);
router.put("/contact_us/resolve_delete", authenticator_1.default, admin_controller.manage_contact_us);
router.post("/backup/database", authenticator_1.default, admin_controller.backup_db);
router.get("/products/listing", authenticator_1.default, admin_controller.list_products);
router.get("/products/details", authenticator_1.default, admin_controller.list_product_details);
router.put("/products/block_delete", authenticator_1.default, admin_controller.manage_products);
router.post("/res_messages/add_edit", authenticator_1.default, admin_controller.add_edit_res_msgs);
router.get("/res_messages/listing", authenticator_1.default, admin_controller.list_res_messages);
router.delete("/res_messages/delete/:_id", authenticator_1.default, admin_controller.delete_res_messages);
router.post("/content/add_edit", authenticator_1.default, admin_controller.add_edit_content);
router.get("/content/listing", authenticator_1.default, admin_controller.list_content);
router.post("/faqs/add_edit", authenticator_1.default, admin_controller.add_edit_faqs);
router.get("/faqs/listing", authenticator_1.default, admin_controller.list_faqs);
router.delete("/faqs/delete/:_id", authenticator_1.default, admin_controller.delete_faqs);
router.post("/variables/add_edit", authenticator_1.default, admin_controller.add_edit_variables);
router.get("/variables/listing", authenticator_1.default, admin_controller.list_variables);
router.post("/email_templates/add_edit", authenticator_1.default, admin_controller.add_edit_templates);
router.get("/email_templates/listing", authenticator_1.default, admin_controller.list_templates);
exports.default = router;
