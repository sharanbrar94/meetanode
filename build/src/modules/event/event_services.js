"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.get_event_faqs = exports.delete_faq = exports.edit_faq = exports.add_faq = exports.unwind_data = exports.group_data = exports.get_schedule_speakers = exports.speaker_detail = exports.delete_schedule_speaker = exports.add_schedule_speaker = exports.edit_schedule = exports.add_schedule = exports.fetch_total_event_attendees = exports.join_event = exports.match_event_by_id = exports.get_static_data = exports.getCategories = exports.make_speaker_response = exports.event_speakers = exports.delete_speaker = exports.edit_speaker = exports.add_speaker = exports.fetch_total_count = exports.delete_event = exports.set_event_data = exports.edit_event_data = exports.make_event_response = void 0;
const DAO = __importStar(require("../../DAO"));
const mongoose_1 = __importDefault(require("mongoose"));
const Models = __importStar(require("../../models"));
const index_1 = require("../../config/index");
const index_2 = require("../../middlewares/index");
const user_scope = index_1.app_constant.scope.user;
const make_event_response = (slug, language) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let eventQuery = { slug: slug };
        let projection = { __v: 0 };
        let eventOptions = { lean: true };
        let response = yield DAO.get_data(Models.Events, eventQuery, projection, eventOptions);
        let event_id = response[0]._id;
        let query = [
            yield match_event_by_id(event_id),
            yield look_up_event_requirements(),
            yield look_up_event_categories(),
            yield look_up_speakers(),
            yield look_up_event_faqs(),
            yield look_up_event_schedules(),
            yield look_up_event_attendees(),
        ];
        let options = { lean: true };
        let fetch_data = yield DAO.aggregate_data(Models.Events, query, options);
        console.log("ddsa", fetch_data);
        return fetch_data[0];
        // if (fetch_data.length) {
        //     return fetch_data[0]
        // } else {
        //     throw await handle_custom_error('INVALID_OBJECT_ID', language)
        // }
    }
    catch (err) {
        throw err;
    }
});
exports.make_event_response = make_event_response;
const look_up_event_schedules = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $lookup: {
                from: "event_schedules",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "schedule_speakers",
                            let: { schedule_id: "$_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$schedule_id", "$$schedule_id"]
                                        },
                                    },
                                },
                                {
                                    $lookup: {
                                        from: "event_speakers",
                                        let: { speaker_id: "$speaker_id" },
                                        pipeline: [
                                            {
                                                $match: {
                                                    $expr: {
                                                        $eq: ["$_id", "$$speaker_id"]
                                                    },
                                                },
                                            }
                                        ],
                                        as: "speakers"
                                    }
                                },
                                {
                                    "$unwind": "$speakers"
                                },
                                {
                                    $group: {
                                        _id: "$_id",
                                        name: { "$last": "$speakers.name" },
                                        title: { "$last": "$speakers.title" },
                                        topic: { "$last": "$speakers.topic" }
                                    }
                                }
                            ],
                            as: "event_speakers"
                        }
                    }
                ],
                as: "event_schedules"
            }
        };
    }
    catch (err) {
        throw err;
    }
});
const look_up_event_attendees = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $lookup: {
                from: "join_events",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "users",
                            let: { user_id: "$user_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$user_id"]
                                        },
                                    },
                                }
                            ],
                            as: "users"
                        }
                    },
                    {
                        "$unwind": "$users"
                    },
                    {
                        $group: {
                            _id: "$_id",
                            username: { "$last": "$users.username" },
                            name: { "$last": "$users.name" },
                            image: { "$last": "$users.image" }
                        }
                    }
                ],
                as: "event_attendees"
            }
        };
    }
    catch (err) {
        throw err;
    }
});
const look_up_event_categories = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $lookup: {
                from: "event_categories",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "categories",
                            let: { category_id: "$category_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$category_id"]
                                        },
                                    },
                                }
                            ],
                            as: "categories"
                        }
                    },
                    {
                        "$unwind": "$categories"
                    },
                    {
                        $group: {
                            _id: "$_id",
                            name: { "$last": "$categories.name" }
                        }
                    }
                ],
                as: "event_categories"
            }
        };
    }
    catch (err) {
        throw err;
    }
});
const look_up_event_faqs = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $lookup: {
                from: "event_faqs",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    }
                ],
                as: "event_faqs"
            }
        };
    }
    catch (err) {
        throw err;
    }
});
const look_up_speakers = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $lookup: {
                from: "event_speakers",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    }
                ],
                as: "event_speakers"
            }
        };
    }
    catch (err) {
        throw err;
    }
});
const look_up_event_requirements = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $lookup: {
                from: "event_requirements",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "requirements",
                            let: { requirement_id: "$requirement_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$requirement_id"]
                                        },
                                    },
                                }
                            ],
                            as: "requirements"
                        }
                    },
                    {
                        "$unwind": "$requirements"
                    },
                    {
                        $group: {
                            _id: "$_id",
                            name: { "$last": "$requirements.name" }
                        }
                    }
                ],
                as: "event_requirements"
            }
        };
    }
    catch (err) {
        throw err;
    }
});
const match_event_by_id = (id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        console.log("test", id);
        return {
            $match: {
                _id: mongoose_1.default.Types.ObjectId(id)
            }
        };
    }
    catch (err) {
        throw err;
    }
});
exports.match_event_by_id = match_event_by_id;
const fetch_total_event_attendees = (event_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { event_id };
        let projection = { __v: 0 };
        let options = { lean: true };
        let response = yield DAO.get_data(Models.JoinEvents, query, projection, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.fetch_total_event_attendees = fetch_total_event_attendees;
const fetch_total_count = (collection, query) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let response = yield DAO.count_data(collection, query);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.fetch_total_count = fetch_total_count;
const set_event_data = (data, user_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_name, slug } = data;
        let query = { slug: slug };
        let projection = { __v: 0 };
        let options = { lean: true };
        let checkEvent = yield DAO.get_data(Models.Events, query, projection, options);
        if (checkEvent.length > 0) {
            throw ("Slug already exists");
        }
        let data_to_save = {
            event_name: event_name,
            slug: slug,
            user_id: user_id,
            created_at: +new Date()
        };
        let response = yield DAO.save_data(Models.Events, data_to_save);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.set_event_data = set_event_data;
const edit_event_data = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id, event_name, country, state, city, address, event_category, slug, start_date, end_date, tagline, overview, requirements, cover_photo, twitter, website, event_type, private_event } = data;
        console.log("requirements", requirements);
        console.log("event_category", event_category);
        let set_data = {};
        let query = { _id: event_id };
        let options = { new: true };
        set_data.event_name = event_name;
        set_data.country = country,
            set_data.state = state,
            set_data.city = city,
            set_data.address = address;
        set_data.slug = slug;
        set_data.start_date = start_date;
        set_data.end_date = end_date;
        set_data.tagline = tagline;
        set_data.overview = overview;
        set_data.cover_photo = cover_photo;
        set_data.twitter = twitter;
        set_data.website = website;
        set_data.event_type = event_type;
        set_data.private_event = private_event;
        let response = yield DAO.find_and_update(Models.Events, query, set_data, options);
        if (requirements) {
            yield requirements.forEach((r) => __awaiter(void 0, void 0, void 0, function* () {
                let get_event_id = event_id;
                let req_query = { event_id: get_event_id, requirement_id: r.requirements };
                let projection = { __v: 0 };
                let options = { lean: true };
                let checkOldReq = yield DAO.get_data(Models.EventRequirements, req_query, projection, options);
                if (checkOldReq.length == 0) {
                    let event_id = response._id;
                    let data_to_save_req = {
                        event_id: event_id,
                        requirement_id: r.requirements,
                        created_at: +new Date()
                    };
                    yield DAO.save_data(Models.EventRequirements, data_to_save_req);
                }
            }));
        }
        if (event_category) {
            let get_event_id = event_id;
            yield event_category.forEach((e) => __awaiter(void 0, void 0, void 0, function* () {
                let cat_query = { event_id: get_event_id, category_id: e.category_id };
                let projection = { __v: 0 };
                let options = { lean: true };
                let checkOldCat = yield DAO.get_data(Models.EventCategories, cat_query, projection, options);
                if (checkOldCat.length == 0) {
                    let event_id = response._id;
                    let data_to_save_cat = {
                        event_id: event_id,
                        category_id: e.category_id,
                        created_at: +new Date()
                    };
                    yield DAO.save_data(Models.EventCategories, data_to_save_cat);
                }
            }));
        }
        let get_data = yield make_event_response(slug, "ENGLISH");
        return get_data;
    }
    catch (err) {
        throw err;
    }
});
exports.edit_event_data = edit_event_data;
const delete_event = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id } = data;
        let set_data = {};
        let query = { _id: event_id };
        let options = { new: true };
        set_data.deleted_at = true;
        let response = yield DAO.find_and_update(Models.Events, query, set_data, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.delete_event = delete_event;
const join_event = (data, user_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id } = data;
        let data_to_save = {
            event_id: event_id,
            user_id: user_id,
            created_at: +new Date()
        };
        let response = yield DAO.save_data(Models.JoinEvents, data_to_save);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.join_event = join_event;
/*============FAQ module===========*/
const add_faq = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id, question, answer } = data;
        let data_to_save = {
            event_id: event_id,
            question: question,
            answer: answer,
            created_at: +new Date()
        };
        let response = yield DAO.save_data(Models.EventFaqs, data_to_save);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.add_faq = add_faq;
const edit_faq = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { faq_id, question, answer } = data;
        let set_data = {};
        let query = { _id: faq_id };
        let options = { new: true };
        set_data.question = question;
        set_data.answer = answer;
        let response = yield DAO.find_and_update(Models.EventFaqs, query, set_data, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.edit_faq = edit_faq;
const get_event_faqs = (event_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { event_id };
        let projection = { __v: 0, password: 0, otp: 0 };
        let options = { lean: true };
        let response = yield DAO.get_data(Models.EventFaqs, query, projection, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.get_event_faqs = get_event_faqs;
const delete_faq = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { faq_id } = data;
        let query = { _id: faq_id };
        let response = yield DAO.remove_many(Models.EventFaqs, query);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.delete_faq = delete_faq;
//speaker module
const add_speaker = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id, name, title, topic } = data;
        let data_to_save = {
            event_id: event_id,
            name: name,
            title: title,
            topic: topic,
            created_at: +new Date()
        };
        let response = yield DAO.save_data(Models.EventSpeakers, data_to_save);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.add_speaker = add_speaker;
const edit_speaker = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { speaker_id, name, title, topic } = data;
        console.log(speaker_id);
        let set_data = {};
        let query = { _id: speaker_id };
        let options = { new: true };
        set_data.name = name;
        set_data.title = title;
        set_data.topic = topic;
        let response = yield DAO.find_and_update(Models.EventSpeakers, query, set_data, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.edit_speaker = edit_speaker;
const speaker_detail = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { speaker_id } = data;
        let query = { _id: speaker_id };
        let projection = { __v: 0 };
        let options = { lean: true };
        return yield DAO.get_data(Models.EventSpeakers, query, projection, options);
    }
    catch (err) {
        throw err;
    }
});
exports.speaker_detail = speaker_detail;
const delete_speaker = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { speaker_id } = data;
        let query = { _id: speaker_id };
        let schedule_query = { speaker_id: speaker_id };
        let response = yield DAO.remove_many(Models.EventSpeakers, query);
        //await DAO.remove_many(Models.ScheduleSpeaker, schedule_query)
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.delete_speaker = delete_speaker;
const event_speakers = (event_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { event_id };
        let projection = { __v: 0, password: 0, otp: 0 };
        let options = { lean: true };
        let response = yield DAO.get_data(Models.EventSpeakers, query, projection, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.event_speakers = event_speakers;
const make_speaker_response = (_id, language) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { _id };
        let projection = { __v: 0, password: 0, otp: 0 };
        let options = { lean: true };
        let response = yield DAO.get_data(Models.EventSpeakers, query, projection, options);
        if (response.length) {
            return response[0];
        }
        else {
            throw yield (0, index_2.handle_custom_error)('INVALID_OBJECT_ID', language);
        }
    }
    catch (err) {
        throw err;
    }
});
exports.make_speaker_response = make_speaker_response;
//category module
const getCategories = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { is_deleted: false };
        let projection = { __v: 0 };
        let options = { lean: true };
        return yield DAO.get_data(Models.Categories, query, projection, options);
    }
    catch (err) {
        throw err;
    }
});
exports.getCategories = getCategories;
//requirements module
const get_static_data = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { is_deleted: false };
        let projection = { __v: 0 };
        let options = { lean: true };
        let requirements = yield DAO.get_data(Models.Requirements, query, projection, options);
        let categories = yield DAO.get_data(Models.Categories, query, projection, options);
        return ({ requirements: requirements, categories: categories });
    }
    catch (err) {
        throw err;
    }
});
exports.get_static_data = get_static_data;
/*=================Add,edit,delete,listing schedule==============*/
const add_schedule = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { date, start_time, end_time, venue, event_id } = data;
        let data_to_save = {
            date: date,
            start_time: start_time,
            end_time: end_time,
            venue: venue,
            event_id: event_id,
            created_at: +new Date()
        };
        var addSchedule = yield DAO.save_data(Models.EventSchedule, data_to_save);
        return addSchedule;
    }
    catch (err) {
        throw err;
    }
});
exports.add_schedule = add_schedule;
const edit_schedule = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { schedule_id, date, start_time, end_time, venue } = data;
        let set_data = {};
        let query = { _id: schedule_id };
        let options = { new: true };
        set_data.date = date;
        set_data.start_time = start_time;
        set_data.end_time = end_time;
        set_data.venue = venue;
        let response = yield DAO.find_and_update(Models.EventSchedule, query, set_data, options);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.edit_schedule = edit_schedule;
const add_schedule_speaker = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { schedule_id, speaker_id } = data;
        let data_to_save = {
            schedule_id: schedule_id,
            speaker_id: speaker_id,
            created_at: +new Date()
        };
        var addScheduleSpeaker = yield DAO.save_data(Models.ScheduleSpeaker, data_to_save);
        return addScheduleSpeaker;
    }
    catch (err) {
        throw err;
    }
});
exports.add_schedule_speaker = add_schedule_speaker;
const get_schedule_speakers = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { schedule_id } = data;
        // let query = { schedule_id : schedule_id }
        // let projection = { __v: 0 }
        // let options = { lean: true }
        // let response = await DAO.get_data(Models.ScheduleSpeaker, query, projection, options)
        // return response
        let query = [
            yield match_schedule_speakers(schedule_id),
            yield look_up_schedule_speakers(),
            yield unwind_data(),
            yield group_data()
        ];
        let options = { lean: true };
        let fetch_data = yield DAO.aggregate_data(Models.ScheduleSpeaker, query, options);
        return fetch_data;
    }
    catch (err) {
        throw err;
    }
});
exports.get_schedule_speakers = get_schedule_speakers;
const look_up_schedule_speakers = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $lookup: {
                from: "event_speakers",
                let: { speaker_id: "$speaker_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$_id", "$$speaker_id"]
                            }
                        }
                    }
                ],
                as: "speaker"
            }
        };
    }
    catch (err) {
        throw err;
    }
});
const unwind_data = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            "$unwind": "$speaker"
        };
    }
    catch (err) {
        throw err;
    }
});
exports.unwind_data = unwind_data;
const group_data = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $group: {
                _id: "$_id",
                speaker_id: { "$last": "$speaker._id" },
                topic: { "$last": "$speaker.topic" },
                title: { "$last": "$speaker.title" },
                name: { "$last": "$speaker.name" }
            }
        };
    }
    catch (err) {
        throw err;
    }
});
exports.group_data = group_data;
const match_schedule_speakers = (id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return {
            $match: {
                schedule_id: mongoose_1.default.Types.ObjectId(id)
            }
        };
    }
    catch (err) {
        throw err;
    }
});
const delete_schedule_speaker = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { schedule_speaker_id } = data;
        let query = { _id: schedule_speaker_id };
        let response = yield DAO.remove_many(Models.ScheduleSpeaker, query);
        return response;
    }
    catch (err) {
        throw err;
    }
});
exports.delete_schedule_speaker = delete_schedule_speaker;
