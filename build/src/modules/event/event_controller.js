"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getEventFaqs = exports.deleteFaq = exports.editFaq = exports.addFaq = exports.getScheduleSpeakers = exports.getSpeakerDetail = exports.deleteScheduleSpeaker = exports.addScheduleSpeaker = exports.editSchedule = exports.addSchedule = exports.joinEvent = exports.getEventSpeakers = exports.getStaticData = exports.getAllEvents = exports.deleteSpeaker = exports.editSpeaker = exports.addSpeaker = exports.getEvent = exports.deleteEvent = exports.editEvent = exports.addEvent = void 0;
const DAO = __importStar(require("../../DAO"));
const Models = __importStar(require("../../models"));
const event_services = __importStar(require("./event_services"));
const index_1 = require("../../middlewares/index");
const index_helpers_1 = require("../../../helpers/index.helpers");
/*============Get category list which are not deleted to add event==========*/
const getStaticData = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let response = yield event_services.get_static_data();
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getStaticData = getStaticData;
/*============Add event api==========*/
const addEvent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id: user_id } = req.user_data;
        let { event_name, slug } = req.body;
        let language = "ENGLISH";
        let create_event = yield event_services.set_event_data(req.body, user_id);
        let response = yield event_services.make_event_response(slug, language);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.addEvent = addEvent;
/*============Edit event api==========*/
const editEvent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id, event_name, country, state, city, address, event_category, slug, start_date, end_date, tagline, overview, requirements, cover_photo, twitter, website, event_type, private_event } = req.body;
        let create_event = yield event_services.edit_event_data(req.body);
        (0, index_1.handle_success)(res, create_event);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.editEvent = editEvent;
/*============Get event by event id api==========*/
const getEvent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { slug } = req.query;
        let language = "ENGLISH";
        let response = yield event_services.make_event_response(slug, language);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getEvent = getEvent;
/*============Get all events api==========*/
const getAllEvents = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { page, limit } = req.query, query = {};
        // if (search != undefined) { query.name = { $regex: search, $options: 'i' } }
        let projection = { __v: 0 };
        let options = yield index_helpers_1.common_helpers.set_options(page, limit);
        let fetch_data = yield DAO.get_data(Models.Events, query, projection, options);
        if (fetch_data.length) {
            for (let value of fetch_data) {
                value.total_event_attendees = 0;
                let event_id = value._id;
                let getAttendees = yield event_services.fetch_total_event_attendees(event_id);
                if (getAttendees.length) {
                    value.total_event_attendees = getAttendees.length;
                }
            }
        }
        let total_count = yield event_services.fetch_total_count(Models.Events, query);
        let response = {
            total_count: total_count,
            events: fetch_data
        };
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getAllEvents = getAllEvents;
/*============Delete event api==========*/
const deleteEvent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id } = req.body;
        let response = yield event_services.delete_event(event_id);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.deleteEvent = deleteEvent;
/*============Joine event by user api==========*/
const joinEvent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { _id: user_id } = req.user_data;
        let { event_id } = req.body;
        let response = yield event_services.join_event(req.body, user_id);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.joinEvent = joinEvent;
/*==================FAQ module==============*/
const addFaq = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id, question, answer } = req.body;
        let response = yield event_services.add_faq(req.body);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.addFaq = addFaq;
const editFaq = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { faq_id, question, answer } = req.body;
        let response = yield event_services.edit_faq(req.body);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.editFaq = editFaq;
const getEventFaqs = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id } = req.query;
        let response = yield event_services.get_event_faqs(event_id);
        // return response
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getEventFaqs = getEventFaqs;
const deleteFaq = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { faq_id } = req.body;
        let response = yield event_services.delete_faq(req.body);
        if (response.deletedCount > 0) {
            let data = { message: `Faq deleted successfully...` };
            (0, index_1.handle_success)(res, data);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.deleteFaq = deleteFaq;
//speaker module
const addSpeaker = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { name, title, topic } = req.body;
        let language = "ENGLISH";
        let create_speaker = yield event_services.add_speaker(req.body);
        let { _id } = create_speaker;
        let response = yield event_services.make_speaker_response(_id, language);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.addSpeaker = addSpeaker;
const editSpeaker = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { speaker_id, event, title, topic } = req.body;
        let language = "ENGLISH";
        let response = yield event_services.edit_speaker(req.body);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.editSpeaker = editSpeaker;
const getSpeakerDetail = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { speaker_id } = req.query;
        let response = yield event_services.speaker_detail(req.query);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getSpeakerDetail = getSpeakerDetail;
const deleteSpeaker = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { speaker_id } = req.body;
        let response = yield event_services.delete_speaker(req.body);
        if (response.deletedCount > 0) {
            let data = { message: `Speaker deleted successfully...` };
            (0, index_1.handle_success)(res, data);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.deleteSpeaker = deleteSpeaker;
const getEventSpeakers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { event_id } = req.query;
        let language = "ENGLISH";
        // get not deleted categories
        let response = yield event_services.event_speakers(event_id);
        // return response
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getEventSpeakers = getEventSpeakers;
//Schedule module
const addSchedule = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { date, start_time, end_time, venue, event_id } = req.body;
        let response = yield event_services.add_schedule(req.body);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.addSchedule = addSchedule;
const editSchedule = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { schedule_id, date, start_time, end_time, venue } = req.body;
        let response = yield event_services.edit_schedule(req.body);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.editSchedule = editSchedule;
const addScheduleSpeaker = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { schedule_id, speaker_id } = req.body;
        let response = yield event_services.add_schedule_speaker(req.body);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.addScheduleSpeaker = addScheduleSpeaker;
const getScheduleSpeakers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { schedule_id } = req.query;
        let response = yield event_services.get_schedule_speakers(req.query);
        (0, index_1.handle_success)(res, response);
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.getScheduleSpeakers = getScheduleSpeakers;
const deleteScheduleSpeaker = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { schedule_speaker_id } = req.body;
        let response = yield event_services.delete_schedule_speaker(req.body);
        if (response.deletedCount > 0) {
            let data = { message: `Deleted successfully...` };
            (0, index_1.handle_success)(res, data);
        }
    }
    catch (err) {
        (0, index_1.handle_catch)(res, err);
    }
});
exports.deleteScheduleSpeaker = deleteScheduleSpeaker;
