"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const event_controller = __importStar(require("./event_controller"));
const authenticator_1 = __importDefault(require("../../middlewares/authenticator"));
const router = express_1.default.Router();
//static data
router.get("/static-data", event_controller.getStaticData);
router.put("/event", authenticator_1.default, event_controller.editEvent);
router.post("/event", authenticator_1.default, event_controller.addEvent);
router.get("/event", event_controller.getEvent);
router.get("/get_events", event_controller.getAllEvents);
router.post("/delete-event", authenticator_1.default, event_controller.deleteEvent);
router.post("/join_event", authenticator_1.default, event_controller.joinEvent);
//faq module
router.post("/faq", event_controller.addFaq);
router.put("/faq", event_controller.editFaq);
//router.get("/faq",event_controller.getSpeakerDetail)
router.get("/event-faqs", event_controller.getEventFaqs);
router.delete("/faq", event_controller.deleteFaq);
//speaker module
router.put("/speaker", event_controller.editSpeaker);
router.post("/speaker", event_controller.addSpeaker);
router.get("/speaker", event_controller.getSpeakerDetail);
router.get("/event-speakers", event_controller.getEventSpeakers);
router.post("/delete-speaker", event_controller.deleteSpeaker);
//schedule module
router.post("/schedule", event_controller.addSchedule);
router.put("/schedule", event_controller.editSchedule);
router.post("/schedule-speaker", event_controller.addScheduleSpeaker);
router.delete("/schedule-speaker", event_controller.deleteScheduleSpeaker);
router.get("/schedule-speaker", event_controller.getScheduleSpeakers);
//schedule module
//router.post("/schedule",event_controller.addSchedule)
exports.default = router;
