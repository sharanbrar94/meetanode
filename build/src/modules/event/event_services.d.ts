import mongoose from 'mongoose';
declare const make_event_response: (slug: any, language: string) => Promise<any>;
declare const match_event_by_id: (id: string) => Promise<{
    $match: {
        _id: mongoose.Types.ObjectId;
    };
}>;
declare const fetch_total_event_attendees: (event_id: any) => Promise<any>;
declare const fetch_total_count: (collection: any, query: any) => Promise<unknown>;
declare const set_event_data: (data: any, user_id: any) => Promise<any>;
declare const edit_event_data: (data: any) => Promise<any>;
declare const delete_event: (data: any) => Promise<any>;
declare const join_event: (data: any, user_id: any) => Promise<any>;
declare const add_faq: (data: any) => Promise<any>;
declare const edit_faq: (data: any) => Promise<any>;
declare const get_event_faqs: (event_id: any) => Promise<any>;
declare const delete_faq: (data: any) => Promise<unknown>;
declare const add_speaker: (data: any) => Promise<any>;
declare const edit_speaker: (data: any) => Promise<any>;
declare const speaker_detail: (data: any) => Promise<unknown>;
declare const delete_speaker: (data: any) => Promise<unknown>;
declare const event_speakers: (event_id: any) => Promise<any>;
declare const make_speaker_response: (_id: any, language: string) => Promise<any>;
declare const getCategories: () => Promise<unknown>;
declare const get_static_data: () => Promise<{
    requirements: unknown;
    categories: unknown;
}>;
declare const add_schedule: (data: any) => Promise<unknown>;
declare const edit_schedule: (data: any) => Promise<any>;
declare const add_schedule_speaker: (data: any) => Promise<unknown>;
declare const get_schedule_speakers: (data: any) => Promise<any>;
declare const unwind_data: () => Promise<{
    $unwind: string;
}>;
declare const group_data: () => Promise<{
    $group: {
        _id: string;
        speaker_id: {
            $last: string;
        };
        topic: {
            $last: string;
        };
        title: {
            $last: string;
        };
        name: {
            $last: string;
        };
    };
}>;
declare const delete_schedule_speaker: (data: any) => Promise<unknown>;
export { make_event_response, edit_event_data, set_event_data, delete_event, fetch_total_count, add_speaker, edit_speaker, delete_speaker, event_speakers, make_speaker_response, getCategories, get_static_data, match_event_by_id, join_event, fetch_total_event_attendees, add_schedule, edit_schedule, add_schedule_speaker, delete_schedule_speaker, speaker_detail, get_schedule_speakers, group_data, unwind_data, add_faq, edit_faq, delete_faq, get_event_faqs };
