declare const set_options: (pagination: any, limit: any) => Promise<any>;
declare const generate_otp: () => Promise<number>;
declare const gen_unique_code: () => Promise<string>;
declare const bcrypt_password: (password: string) => Promise<any>;
declare const decrypt_password: (password: string, hash: string) => Promise<any>;
export { set_options, generate_otp, gen_unique_code, bcrypt_password, decrypt_password };
