declare const send_notification: (data: any, fcm_token: string) => Promise<void>;
export default send_notification;
