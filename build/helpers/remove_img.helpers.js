"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fetch_unwanted_keys = void 0;
const DAO = __importStar(require("../src/DAO"));
const Models = __importStar(require("../src/models"));
const do_spaces_controller_1 = require("../src/modules/uploads/do_spaces.controller");
const fetch_admin_images = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { image: { $ne: null } };
        let projection = { image: 1 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Admin, query, projection, options);
        let images = [];
        if (fetch_data.length) {
            for (let value of fetch_data) {
                let original = `original/${value.image}`;
                let medium = `medium/${value.image}`;
                let small = `small/${value.image}`;
                images.push(original, medium, small);
            }
        }
        return images;
    }
    catch (err) {
        throw err;
    }
});
const fetch_user_images = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { profile_pic: { $ne: null } };
        let projection = { profile_pic: 1 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Users, query, projection, options);
        let images = [];
        if (fetch_data.length) {
            for (let value of fetch_data) {
                let original = `original/${value.profile_pic}`;
                let medium = `medium/${value.profile_pic}`;
                let small = `small/${value.profile_pic}`;
                images.push(original, medium, small);
            }
        }
        return images;
    }
    catch (err) {
        throw err;
    }
});
const fetch_backup_files = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { file_url: { $ne: null } };
        let projection = { file_url: 1 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.BackupLogs, query, projection, options);
        let files = [];
        if (fetch_data.length) {
            for (let value of fetch_data) {
                files.push(`backups/${value.file_url}`);
            }
        }
        return files;
    }
    catch (err) {
        throw err;
    }
});
const fetch_product_images = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let query = { image_url: { $ne: null } };
        let projection = { image_url: 1 };
        let options = { lean: true };
        let fetch_data = yield DAO.get_data(Models.Products, query, projection, options);
        let files = [];
        if (fetch_data.length) {
            for (let value of fetch_data) {
                let original = `original/${value.image_url}`;
                let medium = `medium/${value.image_url}`;
                let small = `small/${value.image_url}`;
                files.push(original, medium, small);
            }
        }
        return files;
    }
    catch (err) {
        throw err;
    }
});
const fetch_db_keys = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let admin_images = yield fetch_admin_images();
        let user_images = yield fetch_user_images();
        let backup_files = yield fetch_backup_files();
        let product_images = yield fetch_product_images();
        console.log("------------------admin_images-->", admin_images.length);
        console.log("------------------user_images-->", user_images.length);
        console.log("-----------------backup_files-->", backup_files.length);
        console.log("---------------product_images-->", product_images.length);
        let all_images = [
            ...admin_images,
            ...user_images,
            ...backup_files,
            ...product_images
        ];
        let filter_null = all_images.filter((value) => { return value != null; });
        let unique_keys = filter_null.filter((value, index) => {
            return filter_null.indexOf(value) === index;
        });
        return unique_keys;
    }
    catch (err) {
        throw err;
    }
});
const fetch_s3_keys = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let fetch_data = yield (0, do_spaces_controller_1.list_all_files)();
        let { Contents } = fetch_data;
        let keys = [];
        if (Contents.length) {
            for (let value of Contents) {
                keys.push(value.Key);
            }
        }
        return keys;
    }
    catch (err) {
        throw err;
    }
});
const fetch_unwanted_keys = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let db_keys = yield fetch_db_keys();
        let s3_keys = yield fetch_s3_keys();
        // console.dir(db_keys, {'maxArrayLength': null});
        console.log("<--db_keys-->", db_keys.length);
        console.log("<--s3_keys-->", s3_keys.length);
        let not_remove_keys = [
            ...db_keys
        ];
        console.log("<--not_remove_keys-->", not_remove_keys.length);
        let keys_to_remove = s3_keys.filter((item) => !not_remove_keys.includes(item));
        console.log("<--keys_to_remove-->", keys_to_remove.length);
        // console.dir(filter_s3_keys, {'maxArrayLength': null});
        // if (keys_to_remove.length) {
        //     for (let value of keys_to_remove) {
        //         await backup_functions.delete_backup(value)
        //     }
        // }
    }
    catch (err) {
        throw err;
    }
});
exports.fetch_unwanted_keys = fetch_unwanted_keys;
