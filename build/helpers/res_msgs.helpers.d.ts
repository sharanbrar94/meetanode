declare const handle_catch: (res: express.Response, error: any) => void;
declare const handle_success: (res: express.Response, response: any) => void;
declare const handle_custom_error: (type: string, language: string) => Promise<{
    type: any;
    status_code: any;
    error_message: string;
}>;
declare const handle_failure: (res: express.Response, type: string, language: string) => Promise<void>;
export { handle_catch, handle_success, handle_custom_error, handle_failure };
