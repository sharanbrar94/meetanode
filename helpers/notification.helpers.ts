import FCM from 'fcm-push';
import { config } from 'dotenv';
config();
const server_key = process.env.NOTIFICATION_KEY

const send_notification = async (data: any, fcm_token: string) => {
    try {

        const fcm = new FCM(server_key)

        let message = {
            to: fcm_token,
            data: data,
            notification: {
                title: data.title,
                body: data.message,
                push_type: data.type,
                sound: 'default',
                badge: 0
            }
        }

        fcm.send(message, function (err: any, result: any) {
            if (err) { console.log("error", err) }
            else { console.log("success", result) }
        });

    }
    catch (err) {
        throw err;
    }
}


export default send_notification