
import lodash from 'lodash';
import * as DAO from '../src/DAO';
import * as Models from '../src/models';
import { list_all_files } from '../src/modules/uploads/do_spaces.controller';
import { backup_functions } from '../src/modules/admin/functions/index.functions';


const fetch_admin_images = async () => {
    try {

        let query = { image: { $ne: null } }
        let projection = { image: 1 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Admin, query, projection, options)

        let images: any = []

        if (fetch_data.length) {
            for (let value of fetch_data) {
                let original = `original/${value.image}`
                let medium = `medium/${value.image}`
                let small = `small/${value.image}`
                images.push(original, medium, small)
            }
        }

        return images

    }
    catch (err) {
        throw err;
    }
}

const fetch_user_images = async () => {
    try {

        let query = { profile_pic: { $ne: null } }
        let projection = { profile_pic: 1 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Users, query, projection, options)

        let images: any = []

        if (fetch_data.length) {
            for (let value of fetch_data) {
                let original = `original/${value.profile_pic}`
                let medium = `medium/${value.profile_pic}`
                let small = `small/${value.profile_pic}`
                images.push(original, medium, small)
            }
        }

        return images

    }
    catch (err) {
        throw err;
    }
}


const fetch_backup_files = async () => {
    try {

        let query = { file_url: { $ne: null } }
        let projection = { file_url: 1 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.BackupLogs, query, projection, options)

        let files: any = []

        if (fetch_data.length) {
            for (let value of fetch_data) {
                files.push(`backups/${value.file_url}`)
            }
        }

        return files

    }
    catch (err) {
        throw err;
    }
}


const fetch_product_images = async () => {
    try {

        let query = { image_url: { $ne: null } }
        let projection = { image_url: 1 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Products, query, projection, options)

        let files: any = []

        if (fetch_data.length) {
            for (let value of fetch_data) {
                let original = `original/${value.image_url}`
                let medium = `medium/${value.image_url}`
                let small = `small/${value.image_url}`
                files.push(original, medium, small)
            }
        }

        return files

    }
    catch (err) {
        throw err;
    }
}


const fetch_db_keys = async () => {
    try {

        let admin_images = await fetch_admin_images()
        let user_images = await fetch_user_images()
        let backup_files = await fetch_backup_files()
        let product_images = await fetch_product_images()

        console.log("------------------admin_images-->", admin_images.length)
        console.log("------------------user_images-->", user_images.length)
        console.log("-----------------backup_files-->", backup_files.length)
        console.log("---------------product_images-->", product_images.length)


        let all_images: any = [
            ...admin_images,
            ...user_images,
            ...backup_files,
            ...product_images
        ]

        let filter_null = all_images.filter((value: string) => { return value != null });
        let unique_keys = filter_null.filter((value: any, index: any) => {
            return filter_null.indexOf(value) === index;
        });

        return unique_keys

    }
    catch (err) {
        throw err;
    }
}

const fetch_s3_keys = async () => {
    try {

        let fetch_data: any = await list_all_files()
        let { Contents } = fetch_data

        let keys: any = []

        if (Contents.length) {
            for (let value of Contents) {
                keys.push(value.Key)
            }
        }

        return keys

    }
    catch (err) {
        throw err;
    }
}

const fetch_unwanted_keys = async () => {
    try {

        let db_keys = await fetch_db_keys()
        let s3_keys = await fetch_s3_keys()
        // console.dir(db_keys, {'maxArrayLength': null});

        console.log("<--db_keys-->", db_keys.length)
        console.log("<--s3_keys-->", s3_keys.length)

        let not_remove_keys = [
            ...db_keys
        ]
        console.log("<--not_remove_keys-->", not_remove_keys.length)

        let keys_to_remove = s3_keys.filter((item: any) => !not_remove_keys.includes(item))
        console.log("<--keys_to_remove-->", keys_to_remove.length)
        // console.dir(filter_s3_keys, {'maxArrayLength': null});

        // if (keys_to_remove.length) {
        //     for (let value of keys_to_remove) {
        //         await backup_functions.delete_backup(value)
        //     }
        // }


    }
    catch (err) {
        throw err;
    }
}


export {
    fetch_unwanted_keys
}