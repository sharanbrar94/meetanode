
import express from 'express';
import * as do_spaces_controller from './do_spaces.controller';
const router = express.Router();

router.post("/do_spaces_file_upload",  do_spaces_controller.upload_file)


export default router;