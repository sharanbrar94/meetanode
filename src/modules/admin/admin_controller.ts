import express from 'express';
import * as DAO from '../../DAO/index';
import * as Models from '../../models/index';
import { handle_success, handle_catch, handle_custom_error, helpers } from '../../middlewares/index';
import * as admin_services from './admin_service';
import * as admin_validator from './admin_validator'
import moment_tz from 'moment-timezone';
import {
    common_helpers
} from '../../../helpers/index.helpers';
import {
    admin_functions,
    email_functions,
    notification_functions,
    backup_functions
} from './functions/index.functions'
let do_spaces_url = process.env.DO_SPACES_URL+'backup/'

const login = async (req: express.Request, res: express.Response) => {
    try {

        let { email, password: input_password, language } = req.body;

        let query = { email: email, is_deleted: false }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Admin, query, projection, options)

        if (fetch_data.length) {

            let { _id, password, is_blocked } = fetch_data[0]
            if (is_blocked == true) {
                throw await handle_custom_error('ACCOUNT_BLOCKED', language)
            }
            else {
                let decrypt = await helpers.decrypt_password(input_password, password)
                if (decrypt != true) {
                    throw await handle_custom_error('INCORRECT_PASSWORD', language)
                }
                else {


                    // generate token 
                    let generate_token = await admin_services.generate_admin_token(_id)
                    let response = await admin_services.make_admin_response(generate_token, language)

                    // return response
                    handle_success(res, response)

                }
            }

        } else {
            throw await handle_custom_error('NO_DATA_FOUND', language)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}


const access_token_login = async (req: any, res: express.Response) => {
    try {

        // return response
        handle_success(res, req.user_data)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const edit_profile = async (req: any, res: express.Response) => {
    try {

        let { name, image } = req.body, { _id: admin_id } = req.user_data;

        let query = { _id: admin_id }

        let set_data: any = {}
        if (name != undefined) { set_data.name = name }
        if (image != undefined) { set_data.image = image }

        let options = { new: true }
        let response = await DAO.find_and_update(Models.Admin, query, set_data, options)

        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const change_password = async (req: any, res: express.Response) => {
    try {

        let { old_password, new_password, language } = req.body, { _id: admin_id } = req.user_data;

        let query = { _id: admin_id }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Admin, query, projection, options)

        if (fetch_data.length) {

            let { _id, password } = fetch_data[0]
            let decrypt = await helpers.decrypt_password(old_password, password)
            if (decrypt != true) {
                throw await handle_custom_error('OLD_PASSWORD_MISMATCH', language)
            }
            else {

                // bycryt password
                let bycryt_password = await helpers.bcrypt_password(new_password)

                let query = { _id: _id }
                let update = { password: bycryt_password }
                let options = { new: true }
                let response: any = await DAO.find_and_update(Models.Admin, query, update, options)

                // return password
                handle_success(res, response)

            }

        } else {
            throw await handle_custom_error('UNAUTHORIZED', language)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const logout = async (req: any, res: express.Response) => {
    try {

        let { _id: admin_id } = req.user_data;

        let query = { type: "ADMIN", admin_id: admin_id }
        await DAO.remove_data(Models.Sessions, query)

        // return response
        handle_success(res, {})

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const dashboard = async (req: any, res: express.Response) => {
    try {

        // fetch user count
        let query = { is_deleted: false }
        let total_users = await admin_services.fetch_total_count(Models.Users, query)

        // fetch products count
        let total_products = await admin_services.fetch_total_count(Models.Products, query)

        // fetch recent users
        let recent_users = await admin_services.fetch_recent_users()

        let response = {
            total_users: total_users,
            total_products: total_products,
            recent_users: recent_users
        }

        // return response
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const add_edit_staff_members = async (req: any, res: express.Response) => {
    try {

        let { _id, language } = req.body, response: any;

        if (_id != undefined) {

            let query = { _id: _id }
            let set_data = await admin_services.set_staff_data(req.body, language, 'UPDATE')
            let options = { new: true }
            response = await DAO.find_and_update(Models.Admin, query, set_data, options)

        } else {

            let set_data = await admin_services.set_staff_data(req.body, language, 'ADD')
            set_data.created_at = +new Date()
            response = await DAO.save_data(Models.Admin, set_data)

        }

        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const list_staff_members = async (req: any, res: express.Response) => {
    try {

        let { search, pagination, limit } = req.query;

        let query: any = { email: { $ne: "admin@gmail.com" }, is_deleted: false }
        if (search != undefined) {
            query.$or = [
                { name: { $regex: search, $options: 'i' } },
                { email: { $regex: search, $options: 'i' } }
            ]
        }
        let projection = { __v: 0 }
        let options = await helpers.set_options(pagination, limit)
        let fetch_data: any = await DAO.get_data(Models.Admin, query, projection, options)

        // fetch total count
        // let query_email = { email: { $ne: "admin@gmail.com" } }
        let total_count = await admin_services.fetch_total_count(Models.Admin, query)

        let response = {
            total_count: total_count,
            data: fetch_data
        }

        // return response
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const staff_members_details = async (req: any, res: express.Response) => {
    try {

        let { _id } = req.query;
        let query: any = { _id: _id }
        let options = { lean: true }
        let projection = { __v: 0 }
        let response = await DAO.get_data(Models.Admin, query, projection, options)

        // return response
        handle_success(res, response[0])

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const mamage_staff_members = async (req: any, res: express.Response) => {
    try {

        let collection = Models.Admin
        let update_data: any = await admin_services.block_delete_data(req.body, collection)

        // return response
        handle_success(res, update_data)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const list_users = async (req: any, res: express.Response) => {
    try {

        let { search,pagination, limit } = req.query;
        let query: any = { is_deleted: false }
        if (search != undefined) {
            query.$or = [
                { name: { $regex: search, $options: 'i' } },
                { email: { $regex: search, $options: 'i' } }
            ]
        }
        let options = await helpers.set_options(pagination, limit)
        let fetch_data: any = await admin_services.fetch_user_data(query,options)

        // fetch total count
        let total_count = await admin_services.fetch_total_count(Models.Users, query)

        let response = {
            total_count: total_count,
            data: fetch_data
        }

        // return response
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const list_user_details = async (req: any, res: express.Response) => {
    try {
        let { _id } = req.query
        let response = await admin_services.fetch_user_detail(_id)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

const manage_users = async (req: any, res: express.Response) => {
    try {

        let { type, language } = req.body, fetch_data: any;

        if (type == 'BLOCK/DELETE') {
            fetch_data = await admin_services.block_delete_data(req.body, Models.Users)
        }
        else if (type == 'VERIFY/UNVERIFY') {
            fetch_data = await admin_services.verify_unverify(req.body)
        }
        else if (type == 'ACTIVATE/DEACTIVATE') {
            fetch_data = await admin_services.activate_deactivate(req.body)
        }
        else {
            throw await handle_custom_error('INVALID_OBJECT_ID', language)
        }

        if (fetch_data) {

            let { _id } = fetch_data
            let query: any = { _id: _id }
            let options = { lean: true }
            let response = await admin_services.fetch_user_data(query,options)

            // return response
            handle_success(res, response[0])

        } else {
            throw await handle_custom_error('INVALID_OBJECT_ID', language)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}


const login_as_user = async (req: any, res: express.Response) => {
    try {

        let { _id, language } = req.body;

        let query = { _id: _id, is_deleted: false }
        let projection = {}
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Users, query, projection, options)

        if (fetch_data.length) {

            // generate access token
            let generate_token: any = await admin_services.generate_user_token(_id, req.body)

            // fetch user response
            let response = await admin_services.make_user_response(generate_token)

            // return response
            handle_success(res, response)

        } else {
            throw await handle_custom_error('INVALID_OBJECT_ID', language)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*==============Event module=============*/
const getAllEvents = async (req: express.Request, res: express.Response) => {
    try {
        let {  page, limit } = req.query, query: any = {}
        // if (search != undefined) { query.name = { $regex: search, $options: 'i' } }

        let projection = { __v: 0 }
        let options = await common_helpers.set_options(page, limit)
        let fetch_data:any = await DAO.get_data(Models.Events, query, projection, options)
        if (fetch_data.length) {
            for (let value of fetch_data) {
                value.total_event_attendees = 0
                let event_id = value._id
                let getAttendees: any = await admin_services.fetch_total_event_attendees(event_id)
                if(getAttendees.length){
                    value.total_event_attendees = getAttendees.length
                }
            }
        }
        let total_count = await admin_services.fetch_total_count(Models.Events, query)
        let response = {
            total_count: total_count,
            events : fetch_data
        }
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

const list_products = async (req: any, res: express.Response) => {
    try {

        let { _id, search, start_date, end_date, timezone, pagination, limit } = req.query;
        let set_start_date = null, set_end_date = null;

        let time_zone = 'Asia/Kolkata';
        if (timezone) { time_zone = timezone }

        if (start_date) {
            set_start_date = moment_tz(start_date, "DD/MM/YYYY").tz(time_zone).startOf('day').format('x')
        }
        if (set_end_date) {
            set_end_date = moment_tz(end_date, "DD/MM/YYYY").tz(time_zone).endOf('day').format('x')
        }

        let query: any = { is_deleted: false }
        if (_id != undefined) { query._id = _id }
        if (search != undefined) { query.name = { $regex: search, $options: 'i' } }
        if (_id != undefined) { query._id = _id }
        if (set_start_date != null && set_end_date != null) {
            query.$and = [
                { created_at: { $gte: set_start_date } },
                { created_at: { $lte: set_end_date } }
            ]
        }

        let options = await helpers.set_options(pagination, limit)
        let fetch_data = await admin_services.make_products_response(query, options)

        // fetch total count
        let total_count = await admin_services.fetch_total_count(Models.Products, query)

        let response = {
            total_count: total_count,
            data: fetch_data
        }

        // return data
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const list_product_details = async (req: any, res: express.Response) => {
    try {

        let { _id } = req.query;

        let query: any = { _id: _id }
        let options = { lean: true }
        let response = await admin_services.make_products_response(query, options)

        // return response
        handle_success(res, response[0])

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const manage_products = async (req: any, res: express.Response) => {
    try {

        let collection = Models.Products
        let update_data: any = await admin_services.block_delete_data(req.body, collection)
        let { _id } = update_data

        let query: any = { _id: _id }
        let options = { lean: true }
        let response = await admin_services.make_products_response(query, options)

        // return response
        handle_success(res, response[0])

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const add_edit_res_msgs = async (req: any, res: express.Response) => {
    try {

        let { _id, type, status_code, message_type, msg_in_english, msg_in_arabic } = req.body, response: any;

        let set_data: any = {
            type: type,
            status_code: status_code,
            message_type: message_type.toUpperCase(),
            msg_in_english: msg_in_english,
            msg_in_arabic: msg_in_arabic
        }

        if (_id != undefined) {

            let query = { _id: _id }
            let options = { new: true }
            response = await DAO.find_and_update(Models.ResMessages, query, set_data, options)

        } else {
            set_data.created_at = +new Date()
            response = await DAO.save_data(Models.ResMessages, set_data)
        }

        // return response
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const list_res_messages = async (req: any, res: express.Response) => {
    try {

        let { search, pagination, limit } = req.query;

        let query: any = {}
        if (search != undefined) {
            query.$or = [
                { type: { $regex: search, $options: 'i' } },
                { message_type: { $regex: search, $options: 'i' } },
                { msg_in_english: { $regex: search, $options: 'i' } },
                { msg_in_arabic: { $regex: search, $options: 'i' } }
            ]
        }
        let projection = { __v: 0 }
        let options = await helpers.set_options(pagination, limit)
        let fetch_data: any = await DAO.get_data(Models.ResMessages, query, projection, options)

        // fetch total count
        let total_count = await admin_services.fetch_total_count(Models.ResMessages, query)

        let response = {
            total_count: total_count,
            data: fetch_data
        }

        // return response
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const delete_res_messages = async (req: any, res: express.Response) => {
    try {

        let { _id } = req.params
        let query = { _id: _id }
        let response: any = await DAO.remove_many(Models.ResMessages, query)

        if (response.deletedCount > 0) {
            let data = { message: `Response message deleted successfully...` }
            handle_success(res, data)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const add_edit_content = async (req: express.Request, res: express.Response) => {
    try {

        let { type, description } = req.body, response: any;

        let set_data: any = {
            type: type,
            description: description
        }

        // check already added or not
        let fetch_content: any = await admin_services.check_content(type)

        if (fetch_content.length) {

            let { _id } = fetch_content[0]

            let query = { _id: _id }
            let options = { new: true }
            response = await DAO.find_and_update(Models.Content, query, set_data, options)

        } else {
            set_data.created_at = +new Date()
            response = await DAO.save_data(Models.Content, set_data)
        }

        // return response
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const list_content = async (req: express.Request, res: express.Response) => {
    try {

        let { type } = req.query, query: any = {}
        if (type != undefined) { query.type = type }

        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data = await DAO.get_data(Models.Content, query, projection, options)

        // return data
        handle_success(res, fetch_data)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const add_edit_faqs = async (req: express.Request, res: express.Response) => {
    try {

        let { _id, question, answer } = req.body, response: any;

        let set_data: any = {
            question: question,
            answer: answer
        }

        if (_id != undefined) {

            let query = { _id: _id }
            let options = { new: true }
            response = await DAO.find_and_update(Models.Faqs, query, set_data, options)

        } else {

            set_data.created_at = +new Date()
            response = await DAO.save_data(Models.Faqs, set_data)

        }

        // return data
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const list_faqs = async (req: express.Request, res: express.Response) => {
    try {

        let { _id, pagination, limit } = req.query, query: any = {}
        if (_id != undefined) { query._id = _id }

        let projection = { __v: 0 }
        let options = await helpers.set_options(pagination, limit)
        let fetch_data = await DAO.get_data(Models.Faqs, query, projection, options)

        // fetch total count
        let total_count = await admin_services.fetch_total_count(Models.Faqs, query)

        let response = {
            total_count: total_count,
            data: fetch_data
        }

        // return data
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const delete_faqs = async (req: any, res: express.Response) => {
    try {

        let { _id } = req.params
        let query = { _id: _id }
        let response: any = await DAO.remove_many(Models.Faqs, query)

        if (response.deletedCount > 0) {
            let data = { message: `Faq deleted successfully...` }
            handle_success(res, data)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}



const add_edit_variables = async (req: any, res: express.Response) => {
    try {

        let { _id, name } = req.body, response: any;

        let set_data: any = { name: name }

        if (_id != undefined) {

            let query = { _id: _id }
            let options = { new: true }
            response = await DAO.find_and_update(Models.Variables, query, set_data, options)

        } else {

            set_data.created_at = +new Date()
            response = await DAO.save_data(Models.Variables, set_data)

        }

        // return data
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const list_variables = async (req: express.Request, res: express.Response) => {
    try {

        let { _id, search, pagination, limit } = req.query, query: any = {}
        if (_id != undefined) { query._id = _id }
        if (search != undefined) { query.name = { $regex: search, $options: 'i' } }

        let projection = { __v: 0 }
        let options = await helpers.set_options(pagination, limit)
        let fetch_data = await DAO.get_data(Models.Variables, query, projection, options)

        // fetch total count
        let total_count = await admin_services.fetch_total_count(Models.Variables, query)

        let response = {
            total_count: total_count,
            data: fetch_data
        }

        // return data
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const delete_variables = async (req: any, res: express.Response) => {
    try {

        let { _id } = req.params
        let query = { _id: _id }
        let response: any = await DAO.remove_many(Models.Variables, query)

        if (response.deletedCount > 0) {
            let data = { message: `Variable deleted successfully...` }
            handle_success(res, data)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const add_edit_templates = async (req: express.Request, res: express.Response) => {
    try {

        let { _id, title, subject, html, variables } = req.body, response: any;

        let set_data: any = {
            title: title,
            type: title.toUpperCase(),
            subject: subject,
            html: html,
            variables: variables
        }

        if (_id != undefined) {

            let query = { _id: _id }
            let options = { new: true }
            response = await DAO.find_and_update(Models.Templates, query, set_data, options)

        } else {

            set_data.created_at = +new Date()
            response = await DAO.save_data(Models.Templates, set_data)

        }

        // return data
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const list_templates = async (req: any, res: express.Response) => {
    try {

        let { _id, search, pagination, limit } = req.query, query: any = {}
        if (_id != undefined) { query._id = _id }
        if (search != undefined) {
            query.$and = [
                { title: { $regex: search, $options: 'i' } },
                { short_content: { $regex: search, $options: 'i' } },
                { subject: { $regex: search, $options: 'i' } }
            ]
        }

        let projection = { __v: 0 }
        let options = await helpers.set_options(pagination, limit)
        let populate = [{ path: 'variables', select: 'name' }]
        let fetch_data = await DAO.populate_data(Models.Templates, query, projection, options, populate)

        // fetch total count
        let total_count = await admin_services.fetch_total_count(Models.Templates, query)

        let response = {
            total_count: total_count,
            data: fetch_data
        }

        // return data
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const delete_templates = async (req: any, res: express.Response) => {
    try {

        let { _id } = req.params
        let query = { _id: _id }
        let response: any = await DAO.remove_many(Models.Templates, query)

        if (response.deletedCount > 0) {
            let data = { message: `Template deleted successfully...` }
            handle_success(res, data)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*=================Requirement CRUD===========*/
const addRequirement = async (req: express.Request, res: express.Response) => {
    try {

        let { name} = req.body;

        let set_data: any = {
            name: name,
            created_at : +new Date()
        }
        let response = await DAO.save_data(Models.Requirements, set_data)
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}
const editRequirement = async (req: express.Request, res: express.Response) => {
    try {

        let { _id, name} = req.body
        let set_data: any = {
            name: name
        }
        let query = { _id: _id }
        let options = { new: true }
        let response = await DAO.find_and_update(Models.Requirements, query, set_data, options)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const getRequirementDetail = async (req: express.Request, res: express.Response) => {
    try {

        let { _id } = req.query
        let query = { _id: _id }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data:any = await DAO.get_data(Models.Requirements, query, projection, options)
        handle_success(res, fetch_data[0])

    }
    catch (err) {
        handle_catch(res, err);
    }
}
const getRequirements = async (req: express.Request, res: express.Response) => {
    try {
        let {  page, limit,search } = req.query,query: any = {}
        query = { is_deleted: false }
        if (search != undefined) { query.name = { $regex: search, $options: 'i' } }
        let options = await common_helpers.set_options(page, limit)
        let projection = { __v: 0 }
        let fetch_data:any = await DAO.get_data(Models.Requirements, query, projection, options)
        handle_success(res, fetch_data)

    }
    catch (err) {
        handle_catch(res, err);
    }
}
const deleteRequirement = async (req: express.Request, res: express.Response) => {
    try {
        let {_id} = req.body;
        let set_data: any = {}
        let query = { _id: _id }
        let options = { new: true }
        
        set_data.is_deleted= true
        let response: any = await DAO.find_and_update(Models.Requirements,query, set_data,options)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*================Category CRUD=============*/
const addCategory = async (req: express.Request, res: express.Response) => {
    try {

        let { name} = req.body;

        let set_data: any = {
            name: name,
            created_at : +new Date()
        }
        let response = await DAO.save_data(Models.Categories, set_data)
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}
const editCategory = async (req: express.Request, res: express.Response) => {
    try {

        let { _id, name} = req.body
        let set_data: any = {
            name: name
        }
        let query = { _id: _id }
        let options = { new: true }
        let response = await DAO.find_and_update(Models.Categories, query, set_data, options)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const getCategoryDetail = async (req: express.Request, res: express.Response) => {
    try {

        let { _id } = req.query
        let query = { _id: _id }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data:any = await DAO.get_data(Models.Categories, query, projection, options)
        handle_success(res, fetch_data[0])

    }
    catch (err) {
        handle_catch(res, err);
    }
}
const getCategories = async (req: express.Request, res: express.Response) => {
    try {
        let {  page, limit,search } = req.query,query: any = {}
        query = { is_deleted: false }
        if (search != undefined) { query.name = { $regex: search, $options: 'i' } }
        let options = await common_helpers.set_options(page, limit)
        let projection = { __v: 0 }
        let fetch_data:any = await DAO.get_data(Models.Categories, query, projection, options)
        handle_success(res, fetch_data)

    }
    catch (err) {
        handle_catch(res, err);
    }
}
const deleteCategory = async (req: express.Request, res: express.Response) => {
    try {
        let {_id} = req.body;
        let set_data: any = {}
        let query = { _id: _id }
        let options = { new: true }
        
        set_data.is_deleted= true
        let response: any = await DAO.find_and_update(Models.Categories,query, set_data,options)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*==============Event module===========*/
const getEvents = async (req: express.Request, res: express.Response) => {
    try {
        let {  pagination, limit,search } = req.query, query: any = {}
        if (search != undefined) { query.event_name = { $regex: search, $options: 'i' } }

        let projection = { __v: 0 }
        let options = await common_helpers.set_options(pagination, limit)
        let fetch_data:any = await DAO.get_data(Models.Events, query, projection, options)
        if (fetch_data.length) {
            for (let value of fetch_data) {
                value.total_event_attendees = 0
                let event_id = value._id
                let getAttendees: any = await admin_services.fetch_total_event_attendees(event_id)
                if(getAttendees.length){
                    value.total_event_attendees = getAttendees.length
                }
            }
        }
        let total_count = await admin_services.fetch_total_count(Models.Events, query)
        let response = {
            total_count: total_count,
            events : fetch_data
        }
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const getEventDetail = async (req: express.Request, res: express.Response) => {
    try {
        let {event_id} = req.query;
        let language = "ENGLISH"
        let response = await admin_services.event_detail(event_id,language)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*=============Ticket detail module========*/
const addPass = async (req: express.Request, res: express.Response) => {
    try {

        let { event_id,name , price} = req.body;

        let set_data: any = {
            event_id:event_id,
            name: name,
            price:price,
            created_at : +new Date()
        }
        let response = await DAO.save_data(Models.EventPasses, set_data)
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}
const editPass = async (req: express.Request, res: express.Response) => {
    try {

        let { _id, name,price} = req.body
        let set_data: any = {
            name: name,
            price:price
        }
        let query = { _id: _id }
        let options = { new: true }
        let response = await DAO.find_and_update(Models.EventPasses, query, set_data, options)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
} 
const getEventPasses = async (req: express.Request, res: express.Response) => {
    try {
        let {  event_id,page, limit } = req.query,query: any = {}
        query = { event_id : event_id }
        let options = await common_helpers.set_options(page, limit)
        let projection = { __v: 0 }
        let fetch_data:any = await DAO.get_data(Models.EventPasses, query, projection, options)
        handle_success(res, fetch_data)

    }
    catch (err) {
        handle_catch(res, err);
    }
}
const deletePass = async (req: express.Request, res: express.Response) => {
    try {
        let { _id } = req.body;

        let query = { _id: _id}
        let response: any = await DAO.remove_data(Models.EventPasses, query)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

const addProgram = async (req: express.Request, res: express.Response) => {
    try {

        let { event_id,program_name,passes} = req.body;

        let set_data: any = {
            event_id:event_id,
            program_name: program_name,
            created_at : +new Date()
        }
        let response :any = await DAO.save_data(Models.EventPrograms, set_data)
        if(passes){
            let reqQuery = { program_id: response._id }
            await DAO.remove_many(Models.ProgramPasses, reqQuery)

            await passes.forEach(async x => {
            
                let program_id = response._id
                let data_to_save: any = {
                    pass_id: x.pass_id,
                    program_id:program_id,
                    created_at: +new Date()
                }
                await DAO.save_data(Models.ProgramPasses, data_to_save)
            })
        }
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}
const deleteProgram = async (req: express.Request, res: express.Response) => {
    try {
        let { _id } = req.body;

        let query = { _id: _id}
        let response: any = await DAO.remove_data(Models.EventPrograms, query)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const deleteProgramPass = async (req: express.Request, res: express.Response) => {
    try {
        let { program_id,pass_id } = req.body;

        let query = { program_id:program_id,pass_id:pass_id}
        let response: any = await DAO.remove_data(Models.ProgramPasses, query)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*===========notification module==========*/
const send_notification = async (req: any, res: express.Response) => {
    try {

        let { type } = req.body;
        if (type == 'EMAIL') { await email_functions.send_broadcast_email(req.body) }
        else { await notification_functions.send_broadcast_push(req.body) }

        let response = "Notification sent sucessfully!!!"

        // return data
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*==============Contact us module============*/
const add_contact_us = async (req: express.Request, res: express.Response) => {
    try {

        let { name,email,country_code,phone_number,message} = req.body;

        let set_data: any = {
            name: name,
            email:email,
            country_code:country_code,
            phone_number:phone_number,
            message:message,
            created_at : +new Date()
        }
        let response = await DAO.save_data(Models.ContactUs, set_data)
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}
const list_contact_us = async (req: express.Request, res: express.Response) => {
    try {

        let { search, pagination, limit } = req.query

        let query: any = { is_deleted: false }
        if (search != null || search != undefined) {
            query.$or = [
                { name: { $regex: search, $options: 'i' } },
                { email: { $regex: search, $options: 'i' } }
            ]
        }
        let projection = { __v: 0 }
        let options = await helpers.set_options(pagination, limit)
        let fetch_data = await DAO.get_data(Models.ContactUs, query, projection, options)

        // fetch total orders
        let total_count = await admin_services.fetch_total_count(Models.ContactUs, query)

        let response = {
            total_count: total_count,
            data: fetch_data
        }

        // return data
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const manage_contact_us = async (req: any, res: express.Response) => {
    try {

        let { _id, type } = req.body, response: any;

        let update = {}
        if (type == 'RESOLVE') { update = { resolved: true } }
        else if (type == 'DELETE') { update = { is_deleted: true } }

        let query = { _id: _id }
        let options = { new: true }
        response = await DAO.find_and_update(Models.ContactUs, query, update, options)

        // return data
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const backup_db = async (req: any, res: express.Response) => {
    try {

        let { language } = req.body

        let create_backup = await backup_functions.backup_using_cron(language)
        let { file_url } = create_backup

        let response = {
            base_url: do_spaces_url,
            folders: ['backup'],
            file_name: file_url
        }

        // return data
        await handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}
    



export {
    login,
    access_token_login,
    edit_profile,
    change_password,
    logout,
    dashboard,
    add_edit_staff_members,
    list_staff_members,
    staff_members_details,
    mamage_staff_members,
    list_users,
    list_user_details,
    manage_users,
    login_as_user,
    list_products,
    list_product_details,
    manage_products,
    add_edit_res_msgs,
    list_res_messages,
    delete_res_messages,
    add_edit_content,
    list_content,
    add_edit_faqs,
    list_faqs,
    delete_faqs,
    list_contact_us,
    add_contact_us,
    manage_contact_us,
    add_edit_variables,
    list_variables,
    delete_variables,
    add_edit_templates,
    list_templates,
    delete_templates,
    getAllEvents,
    addRequirement,
    editRequirement,
    getRequirementDetail,
    getRequirements,
    deleteRequirement,
    addCategory,
    editCategory,
    getCategoryDetail,
    getCategories,
    deleteCategory,
    getEvents,
    send_notification,
    backup_db,
    getEventDetail,
    addPass,
    editPass,
    getEventPasses,
    deletePass,
    addProgram,
    deleteProgram,
    deleteProgramPass
}