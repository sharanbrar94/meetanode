import express from 'express';
import * as admin_controller from './admin_controller';
import authenticator from '../../middlewares/authenticator';
const router = express.Router();

router.post("/login", admin_controller.login)
router.get("/access_token_login", authenticator, admin_controller.access_token_login)
router.put("/edit_profile", authenticator, admin_controller.edit_profile)
router.put("/change_password", authenticator, admin_controller.change_password)
router.put("/logout", authenticator, admin_controller.logout)
router.get("/dashboard", authenticator, admin_controller.dashboard)
router.post("/staff_members/add_edit", authenticator, admin_controller.add_edit_staff_members)
router.get("/staff_members/listing", authenticator, admin_controller.list_staff_members)
router.get("/staff_members/details", authenticator, admin_controller.staff_members_details)
router.put("/staff_members/block_delete", authenticator, admin_controller.mamage_staff_members)

//user module
router.get("/users/listing", authenticator, admin_controller.list_users)
router.get("/users/details", authenticator, admin_controller.list_user_details)
router.put("/users/manage_users", authenticator, admin_controller.manage_users)
router.post("/users/login_as_user", authenticator, admin_controller.login_as_user)

//static data module
router.post("/requirement", authenticator, admin_controller.addRequirement)
router.put("/requirement", authenticator, admin_controller.editRequirement)
router.get("/requirement", authenticator, admin_controller.getRequirementDetail)
router.get("/requirements", authenticator, admin_controller.getRequirements)
router.post("/delete-requirement", authenticator, admin_controller.deleteRequirement)

router.post("/category", authenticator, admin_controller.addCategory)
router.put("/category", authenticator, admin_controller.editCategory)
router.get("/category", authenticator, admin_controller.getCategoryDetail)
router.get("/categories", authenticator, admin_controller.getCategories)
router.post("/delete-category", authenticator, admin_controller.deleteCategory)

//event module
router.get("/events", authenticator, admin_controller.getEvents)
router.get("/event", authenticator, admin_controller.getEventDetail)
//router.get("/event-approve-disapprove", authenticator, admin_controller.changeEventStatus)

//ticket detail module
router.post("/pass", authenticator, admin_controller.addPass)
router.put("/pass", authenticator, admin_controller.editPass)
router.get("/pass", authenticator, admin_controller.getEventPasses)
router.delete("/pass", authenticator, admin_controller.deletePass)

router.post("/program", authenticator, admin_controller.addProgram)
router.delete("/program", authenticator, admin_controller.deleteProgram)
router.delete("/program-pass", authenticator, admin_controller.deleteProgramPass)
//router.get("/pass", authenticator, admin_controller.getEventPasses)
//router.delete("/pass", authenticator, admin_controller.deletePass)


//notification module
router.post("/notification/broadcast", authenticator, admin_controller.send_notification)

//contact us module
router.post("/contact_us/add", authenticator, admin_controller.add_contact_us)
router.get("/contact_us/listing", authenticator, admin_controller.list_contact_us)
router.put("/contact_us/resolve_delete", authenticator, admin_controller.manage_contact_us)

router.post("/backup/database", authenticator, admin_controller.backup_db)

router.get("/products/listing", authenticator, admin_controller.list_products)
router.get("/products/details", authenticator, admin_controller.list_product_details)
router.put("/products/block_delete", authenticator, admin_controller.manage_products)
router.post("/res_messages/add_edit", authenticator, admin_controller.add_edit_res_msgs)
router.get("/res_messages/listing", authenticator, admin_controller.list_res_messages)
router.delete("/res_messages/delete/:_id", authenticator, admin_controller.delete_res_messages)
router.post("/content/add_edit", authenticator, admin_controller.add_edit_content)
router.get("/content/listing", authenticator, admin_controller.list_content)
router.post("/faqs/add_edit", authenticator, admin_controller.add_edit_faqs)
router.get("/faqs/listing", authenticator, admin_controller.list_faqs)
router.delete("/faqs/delete/:_id", authenticator, admin_controller.delete_faqs)

router.post("/variables/add_edit", authenticator, admin_controller.add_edit_variables)
router.get("/variables/listing", authenticator, admin_controller.list_variables)
router.post("/email_templates/add_edit", authenticator, admin_controller.add_edit_templates)
router.get("/email_templates/listing", authenticator, admin_controller.list_templates)



export default router;