
import * as DAO from '../../DAO/index';
import * as Models from '../../models';
import mongoose from 'mongoose';
import { app_constant, error } from '../../config/index';
const admin_scope = app_constant.scope.admin;
const user_scope = app_constant.scope.user;
import { generate_token, handle_custom_error, helpers } from '../../middlewares/index';

// const Moment = require('moment-timezone');
// const MomentRange = require('moment-range');
// const moment = MomentRange.extendMoment(Moment);
// const salt_rounds = app_constant.salt_rounds;
// const admin_scope = app_constant.scope.admin;
// const user_scope = app_constant.scope.user;

const generate_admin_token = async (_id: string) => {
    try {

        let token_data = {
            _id: _id,
            scope: admin_scope,
            collection: Models.Admin,
            token_gen_at: +new Date()
        }
        let response = await fetch_admin_token(token_data)
        return response

    }
    catch (err) {
        throw err;
    }
}


const fetch_admin_token = async (token_data: any) => {
    try {

        let access_token: any = await generate_token(token_data)
        let response = await save_session_data(access_token, token_data)
        return response

    }
    catch (err) {
        throw err;
    }
}


const save_session_data = async (access_token: string, token_data: any) => {
    try {

        let { _id: admin_id, token_gen_at } = token_data

        let set_data: any = {
            type: "ADMIN",
            admin_id: admin_id,
            access_token: access_token,
            token_gen_at: token_gen_at,
            created_at: +new Date()
        }
        let response = await DAO.save_data(Models.Sessions, set_data)

        return response

    }
    catch (err) {
        throw err;
    }
}

const update_language = async(_id : string, language : string) => {
    try {

        let query = { _id : _id }
        let update = { language : language }
        let options = { new : true }
        await DAO.find_and_update(Models.Admin, query, update, options)

    }
    catch(err) {
        throw err;
    }
}

const make_admin_response = async (data: any, language : string) => {
    try {

        let { admin_id, access_token, token_gen_at } = data

        let query = { _id: admin_id }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Admin, query, projection, options)

        if (fetch_data.length) {

            fetch_data[0].access_token = access_token
            fetch_data[0].token_gen_at = token_gen_at

            return fetch_data[0]
        }
        else {
            throw await handle_custom_error('UNAUTHORIZED', language)
        }

    }
    catch (err) {
        throw err;
    }
}


const block_delete_data = async (data: any, collection: any) => {
    try {

        let { _id, is_blocked, is_deleted } = data;

        let query = { _id: _id }
        let data_to_update: any = {}
        if (typeof is_blocked !== "undefined" && is_blocked !== null) {
            data_to_update.is_blocked = is_blocked
        }
        if (typeof is_deleted !== "undefined" && is_deleted !== null) {
            data_to_update.is_deleted = is_deleted
        }
        let options = { new: true }
        let response = await DAO.find_and_update(collection, query, data_to_update, options)
        return response

    }
    catch (err) {
        throw err;
    }
}

const verify_unverify = async (data: any) => {
    try {

        let { _id, admin_verified } = data;
        let query = { _id: _id }
        let update: any = {}
        if (typeof admin_verified !== "undefined" && admin_verified !== null) {
              update.admin_verified = admin_verified
        }
        let options = { new: true }
        let response = await DAO.find_and_update(Models.Users, query, update, options)
        return response

    }
    catch (err) {
        throw err;
    }
}

const activate_deactivate = async (data: any) => {
    try {

        let { _id, account_status } = data

        let query = { _id: _id }
        let update = { account_status: account_status }
        let options = { new: true }
        let response = await DAO.find_and_update(Models.Users, query, update, options)

        // remove login details
        if (account_status == 'DEACTIVATED') {
            let query = { user_id: _id }
            await DAO.remove_many(Models.Sessions, query)
        }

        return response

    }
    catch (err) {
        throw err;
    }
}

const fetch_total_count = async (collection: any, query: any) => {
    try {

        let response = await DAO.count_data(collection, query)
        return response

    }
    catch (err) {
        throw err;
    }
}

const fetch_recent_users = async () => {
    try {

        let query = { is_deleted: false }
        let projection = { __v: 0, password: 0, otp: 0, fp_otp: 0 }
        let options = { lean: true, sort: { _id: -1 }, limit: 20 }
        let response = await DAO.get_data(Models.Users, query, projection, options)
        return response

    }
    catch (err) {
        throw err;
    }
}


const generate_user_token = async (_id: string, req_data: any) => {
    try {

        let token_data = {
            _id: _id,
            scope: user_scope,
            collection: Models.Users,
            token_gen_at: +new Date()
        }
        let response = await fetch_user_token(token_data, req_data)
        return response

    }
    catch (err) {
        throw err;
    }
}


const fetch_user_token = async (token_data: any, req_data: any) => {
    try {

        let access_token: any = await generate_token(token_data)
        let response = await save_user_session_data(access_token, token_data, req_data)
        return response

    }
    catch (err) {
        throw err;
    }
}


const save_user_session_data = async (access_token: string, token_data: any, req_data: any) => {
    try {

        let { _id: user_id, token_gen_at } = token_data, { device_type, fcm_token } = req_data

        let set_data: any = {
            type: "USER",
            user_id: user_id,
            access_token: access_token,
            token_gen_at: token_gen_at,
            created_at: +new Date()
        }
        if (device_type != null || device_type != undefined) { set_data.device_type = device_type }
        if (fcm_token != null || fcm_token != undefined) { set_data.fcm_token = fcm_token }
        let response = await DAO.save_data(Models.Sessions, set_data)

        return response

    }
    catch (err) {
        throw err;
    }
}

const make_user_response = async (token_data: any) => {
    try {

        let { user_id, access_token, device_type, fcm_token, language, token_gen_at } = token_data

        let query = { _id: user_id }
        let projection = { __v: 0, password: 0, otp: 0, fp_otp: 0, unique_code: 0 }
        let options = { lean: true }
        let response: any = await DAO.get_data(Models.Users, query, projection, options)

        if (response.length) {

            response[0].access_token = access_token
            response[0].device_type = device_type
            response[0].fcm_token = fcm_token
            response[0].token_gen_at = token_gen_at
            return response[0]

        } else {
            throw await handle_custom_error('INVALID_OBJECT_ID', language) 
        }

    }
    catch (err) {
        throw err;
    }
}

const fetch_user_data = async (query: any, options: any) => {
    try {

        let projection = { password: 0, otp: 0, fp_otp: 0, unique_code: 0, __v: 0 }
        let response = await DAO.get_data(Models.Users, query, projection, options)
        return response

    }
    catch (err) {
        throw err;
    }
}
const fetch_user_detail = async (_id: any) => {
    try {

        //let query = { _id }
        let projection = { __v: 0, password: 0, otp: 0 }
       // let options = { lean: true }
        //let response: any = await DAO.get_data(Models.Events, query, projection, options)

        let query = [
            await match_user_by_id(_id),
            await look_up_event_events(),
            await look_up_join_events()
        ]
        let options = { lean: true }
        let fetch_data : any = await DAO.aggregate_data(Models.Users, query, options)
        return fetch_data
    }
    catch (err) {
        throw err;
    }
}
const match_user_by_id = async (user_id: string) => {
    try {
        return {
            $match: {
                _id : mongoose.Types.ObjectId(user_id)
            }
        }
    }
    catch (err) {
        throw err;
    }
}
const look_up_event_events = async () => {
    try {

        return {
            $lookup: {
                from: "events",
                let: { user_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$user_id", "$$user_id"]
                            }
                        }
                    }
                    
                    
                ],
                as: "created_events"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_join_events = async () => {
    try {

        return {
            $lookup: {
                from: "join_events",
                let: { user_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$user_id", "$$user_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "events",
                            let: { event_id: "$event_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$event_id"]
                                        }
                                    }
                                }
                            ],
                            as: "events"
                        }
                    },
                    {
                        $unwind:"$events"
                    },
                    {
                        $group: {
                            _id: "$events._id",
                            event_name: {"$last":"$events.event_name"},
                            slug: {"$last":"$events.slug"},
                            start_date: {"$last":"$events.start_date"},
                            end_date: {"$last":"$events.end_date"},
                            country: {"$last":"$events.country"},
                            state: {"$last":"$events.state"},
                            city: {"$last":"$events.city"},
                            address: {"$last":"$events.address"}
                            //total_event_attendees: await fetch_total_event_attendees("$events._id")
                        } 
                    }
                ],
                as: "joined_events"
            }
        }

    }
    catch (err) {
        throw err;
    }
}


const check_content = async (type: string) => {
    try {

        let query = { type: type }
        let projection = { __v: 0 }
        let options = { lean: true }
        return await DAO.get_data(Models.Content, query, projection, options)

    }
    catch (err) {
        throw err;
    }
}

const verify_admin_info = async (query: any) => {
    try {

        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data = await DAO.get_data(Models.Admin, query, projection, options)
        return fetch_data

    }
    catch (err) {
        throw err;
    }
}

const set_staff_data = async (data: any, language : string, type: string) => {
    try {

        let set_data: any = {
            name: data.name,
            roles: data.roles
        }
        if (data.password) { 
            let hassed_password = await helpers.bcrypt_password(data.password)
            set_data.password = hassed_password
        }
        if (data.image) { set_data.image = data.image }
        if (data.email) {

            // check other email
            let email = data.email.toLowerCase()
            if (type == 'UPDATE') {

                let query = { _id: { $ne: data._id }, email: email }
                let fetch_data: any = await verify_admin_info(query)
                if (fetch_data.length) { 
                    throw await handle_custom_error('EMAIL_ALREADY_EXISTS', language)  
                }
                else { set_data.email = email }

            } else {

                let query = { email: email }
                let fetch_data: any = await verify_admin_info(query)
                if (fetch_data.length) { 
                    throw await handle_custom_error('EMAIL_ALREADY_EXISTS', language)  
                }
                else { set_data.email = email }

            }

        }

        return set_data

    }
    catch (err) {
        throw err;
    }
}


const make_products_response = async (query: any, options: any) => {
    try {

        let projection = { __v: 0 }
        let populate = [
            { path : 'added_by', select : 'profile_pic name' }
        ]
        let respone = await DAO.populate_data(Models.Products, query, projection, options, populate)
        return respone

    }
    catch (err) {
        throw err;
    }
}

/*==========Event module===========*/
const fetch_total_event_attendees = async (event_id: any) => {
    try {
        let query = { event_id }
        let projection = { __v: 0}
        let options = { lean: true }
        let response: any = await DAO.get_data(Models.JoinEvents, query, projection, options)
        return response
    }
    catch (err) {
        throw err;
    }
}
const event_detail = async (event_id: any, language: string) => {
    try {
        let query = [
            await match_event_by_id(event_id),
            await look_up_user_detail(),
            await unwind_user_detail(),
            await look_up_event_requirements(),
            await look_up_event_categories(),
            await look_up_speakers(),
            await look_up_event_faqs(),
            await look_up_event_schedules(),
            await look_up_event_attendees(),
            await look_up_event_passes(),
            await look_up_ticket_detail(),
        ]
        let options = { lean: true }
        let fetch_data : any = await DAO.aggregate_data(Models.Events, query, options)
        return fetch_data[0]
    }
    catch (err) {
        throw err;
    }
}
const look_up_event_passes = async() => {
    try {
        return {
            $lookup: {
                from: "event_passes",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                ],
                as: "event_passes"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_ticket_detail = async() => {
    try {
        return {
            $lookup: {
                from: "event_programs",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "program_passes",
                            let: { program_id: "$_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$program_id", "$$program_id"]
                                        },
                                    },
                                    
                                },
                                {
                                    $lookup: {
                                        from: "event_passes",
                                        let: { pass_id: "$pass_id" },
                                        pipeline: [
                                            {
                                                $match: {
                                                    $expr: {
                                                        $eq: ["$_id", "$$pass_id"]
                                                    },
                                                },
                                                
                                            }
                                        ],
                                        as: "passes"
                                    }
                                },
                                {
                                    "$unwind":"$passes"
                                },
                                {
                                    $group: {
                                        _id: "$_id",
                                        pass_id:{"$last":"$passes._id"},
                                        pass_name:{"$last":"$passes.name"}
                                    } 
                                }

                            ],
                            as: "program_passes"
                        }
                    }
                    
                ],
                as: "event_ticket_detail"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const unwind_user_detail = async () => {
    try {

        return {
            
            "$unwind":"$user"
            
        }

    }
    catch (err) {
        throw err;
    }
}

const look_up_user_detail = async() => {
    try {

        return {
            $lookup: {
                from: "users",
                let: { user_id: "$user_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$_id", "$$user_id"]
                            }
                        }
                    }
                    
                ],
                as: "user"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_event_schedules = async() =>{
    try {

        return {
            $lookup: {
                from: "event_schedules",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "schedule_speakers",
                            let: { schedule_id: "$_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$schedule_id", "$$schedule_id"]
                                        },
                                    },
                                    
                                },
                                {
                                    $lookup: {
                                        from: "event_speakers",
                                        let: { speaker_id: "$speaker_id" },
                                        pipeline: [
                                            {
                                                $match: {
                                                    $expr: {
                                                        $eq: ["$_id", "$$speaker_id"]
                                                    },
                                                },
                                                
                                            }
                                        ],
                                        as: "speakers"
                                    }
                                },
                                {
                                    "$unwind":"$speakers"
                                },
                                {
                                    $group: {
                                        _id: "$_id",
                                        name:{"$last":"$speakers.name"},
                                        title:{"$last":"$speakers.title"},
                                        topic:{"$last":"$speakers.topic"}
                                        
                                        
                                    } 
                                }

                            ],
                            as: "event_speakers"
                        }
                    }
                    
                ],
                as: "event_schedules"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_event_attendees = async () =>{
    try {

        return {
            $lookup: {
                from: "join_events",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "users",
                            let: { user_id: "$user_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$user_id"]
                                        },
                                    },
                                    
                                }
                            ],
                            as: "users"
                        }
                    },
                    {
                        "$unwind":"$users"
                    },
                    {
                        $group: {
                            _id: "$_id",
                            username:{"$last":"$users.username"},
                            name:{"$last":"$users.name"},
                            image:{"$last":"$users.image"}
                            
                            
                        } 
                    }
                ],
                as: "event_attendees"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_event_categories = async () =>{
    try {

        return {
            $lookup: {
                from: "event_categories",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "categories",
                            let: { category_id: "$category_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$category_id"]
                                        },
                                    },
                                    
                                }
                            ],
                            as: "categories"
                        }
                    },
                    {
                        "$unwind":"$categories"
                    },
                    {
                        $group: {
                            _id: "$_id",
                            name:{"$last":"$categories.name"}
                            
                            
                        } 
                    }
                ],
                as: "event_categories"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_event_faqs = async () =>{
    try {

        return {
            $lookup: {
                from: "event_faqs",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    }
                    
                ],
                as: "event_faqs"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_speakers = async () =>{
    try {

        return {
            $lookup: {
                from: "event_speakers",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    }
                    
                ],
                as: "event_speakers"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_event_requirements = async () => {
    try {

        return {
            $lookup: {
                from: "event_requirements",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "requirements",
                            let: { requirement_id: "$requirement_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$requirement_id"]
                                        },
                                    },
                                    
                                }
                            ],
                            as: "requirements"
                        }
                    },
                    {
                        "$unwind":"$requirements"
                    },
                    {
                        $group: {
                            _id: "$_id",
                            name:{"$last":"$requirements.name"}
                            
                            
                        } 
                    }
                    
                ],
                as: "event_requirements"
            }
            
        }

    }
    catch (err) {
        throw err;
    }
}
const match_event_by_id = async (id: string) => {
    try {
        console.log("test",id)

        return {
            $match: {
                _id : mongoose.Types.ObjectId(id)
            }
        }

    }
    catch (err) {
        throw err;
    }
}



export {
    generate_admin_token,
    fetch_admin_token,
    save_session_data,
    update_language,
    make_admin_response,
    block_delete_data,
    verify_unverify,
    activate_deactivate,
    fetch_total_count,
    fetch_recent_users,
    generate_user_token,
    make_user_response,
    fetch_user_data,
    check_content,
    verify_admin_info,
    set_staff_data,
    make_products_response,
    fetch_user_detail,
    fetch_total_event_attendees,
    event_detail
}