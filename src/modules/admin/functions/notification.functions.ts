import * as DAO from '../../../DAO';
import * as Models from '../../../models';
import { send_notification } from '../../../../helpers/index.helpers';
import { error, app_constant } from '../../../config/index';


const fetch_users = async (query: any) => {
    try {

        let projection = { __v: 0 }
        let options = { lean: true, sort: { _id: -1 } }
        let fetch_data: any = await DAO.get_data(Models.Users, query, projection, options)

        let user_ids: any = []
        if (fetch_data.length) {
            for (let value of fetch_data) {
                user_ids.push(value._id)
            }
        }

        return user_ids

    }
    catch (err) {
        throw err;
    }
}


const fetch_user_token = async (user_id: string) => {
    try {

        let query = { user_id: user_id, fcm_token: { $ne: null } }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Users, query, projection, options)
        return fetch_data

    }
    catch (err) {
        throw err;
    }
}

const send_broadcast_push = async (data: any) => {
    try {

        let { send_to, user_ids, subject, message } = data
        let default_title = 'New Notification Received';

        let users: any;
        if (send_to == 'ALL_USERS') {
            let query = { is_deleted: false }
            users = await fetch_users(query)
        }
        else {
            let query = { _id: { $in: user_ids }, is_deleted: false }
            users = await fetch_users(query)
        }

        if (users.length) {
            for (let value of users) {
                
                let data_to_save = {
                    user_id: value._id,
                    title: default_title,
                    message: message,
                    type: 'BROADCAST_PUSH',
                    created_at: +new Date()
                }
                await DAO.save_data(Models.Notifications, data_to_save)

                // fetch device token
                let user_token_data = await fetch_user_token(value)
                await send_push(user_token_data, data_to_save)

            }
        }


    }
    catch (err) {
        throw err;
    }
}


const send_push = async(data : any, notification_data : any) => {
    try {

        if (data.length) {
            for (let value of data) {
                let { fcm_token } = value
                if (fcm_token != undefined) {
                    await send_notification(notification_data, fcm_token)
                }
            }
        }
        
    }
    catch(err) {
        throw err;
    }
}



export {
    send_broadcast_push
}