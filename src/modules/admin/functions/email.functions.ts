import { send_email } from '../../../../helpers/index.helpers';
import * as DAO from '../../../DAO/index';
import * as Models from '../../../models';


const fetch_users = async (query: any) => {
    try {

        let projection = { __v: 0 }
        let options = { lean: true, sort: { _id: -1 } }
        let fetch_data: any = await DAO.get_data(Models.Users, query, projection, options)

        let user_ids: any = []
        if (fetch_data.length) {
            for (let value of fetch_data) {
                user_ids.push(value._id)
            }
        }

        return user_ids

    }
    catch (err) {
        throw err;
    }
}


const send_broadcast_email = async (data: any) => {
    try {

        let { send_to, user_ids, subject, message } = data
        let users: any;

        if (send_to == 'ALL_USERS') {
            let query = { is_deleted: false }
            users = await fetch_users(query)
        }
        else {
            let query = { _id: { $in: user_ids }, is_deleted: false }
            users = await fetch_users(query)
        }

        if (users.length) {
            for (let value of users) {
                let { email } = value
                await send_email(email, subject, message)
            }
        }


    }
    catch (err) {
        throw err;
    }
}



export {
    send_broadcast_email
}