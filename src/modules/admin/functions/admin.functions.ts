import bcrypt from 'bcrypt';
import * as express from 'express';
import * as DAO from '../../../DAO/index';
import * as Models from '../../../models';
import { generate_token, handle_custom_error, helpers } from '../../../middlewares/index';
import { app_constant, error } from '../../../config/index';
import random_string from "randomstring";
const Moment = require('moment-timezone');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const salt_rounds = app_constant.salt_rounds

const admin_scope = app_constant.scope.admin;
const user_scope = app_constant.scope.user;


const generate_admin_token = async (_id: string) => {
    try {

        let token_data = {
            _id: _id,
            scope: admin_scope,
            collection: Models.Admin,
            token_gen_at: +new Date()
        }
        let response = await fetch_admin_token(token_data)
        return response

    }
    catch (err) {
        throw err;
    }
}


const fetch_admin_token = async (token_data: any) => {
    try {

        let access_token: any = await generate_token(token_data)
        let response = await save_session_data(access_token, token_data)
        return response

    }
    catch (err) {
        throw err;
    }
}


const save_session_data = async (access_token: string, token_data: any) => {
    try {

        let { _id: admin_id, token_gen_at } = token_data

        let set_data: any = {
            type: "ADMIN",
            admin_id: admin_id,
            access_token: access_token,
            token_gen_at: token_gen_at,
            created_at: +new Date()
        }
        let response = await DAO.save_data(Models.Sessions, set_data)

        return response

    }
    catch (err) {
        throw err;
    }
}

const update_language = async(_id : string, language : string) => {
    try {

        let query = { _id : _id }
        let update = { language : language }
        let options = { new : true }
        await DAO.find_and_update(Models.Admin, query, update, options)

    }
    catch(err) {
        throw err;
    }
}

const make_admin_response = async (data: any, language : string) => {
    try {

        let { admin_id, access_token, token_gen_at } = data

        let query = { _id: admin_id }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Admin, query, projection, options)

        if (fetch_data.length) {

            fetch_data[0].access_token = access_token
            fetch_data[0].token_gen_at = token_gen_at

            return fetch_data[0]
        }
        else {
            throw await handle_custom_error('UNAUTHORIZED', language)
        }

    }
    catch (err) {
        throw err;
    }
}


const block_delete_data = async (data: any, collection: any) => {
    try {

        let { _id, is_blocked, is_deleted } = data;

        let query = { _id: _id }
        let data_to_update: any = {}
        if (typeof is_blocked !== "undefined" && is_blocked !== null) {
            data_to_update.is_blocked = is_blocked
        }
        if (typeof is_deleted !== "undefined" && is_deleted !== null) {
            data_to_update.is_deleted = is_deleted
        }
        let options = { new: true }
        let response = await DAO.find_and_update(collection, query, data_to_update, options)
        return response

    }
    catch (err) {
        throw err;
    }
}

const verify_unverify = async (data: any) => {
    try {

        let { _id, admin_verified } = data;
        let query = { _id: _id }
        let update: any = {}
        if (typeof admin_verified !== "undefined" && admin_verified !== null) {
              update.admin_verified = admin_verified
        }
        let options = { new: true }
        let response = await DAO.find_and_update(Models.Users, query, update, options)
        return response

    }
    catch (err) {
        throw err;
    }
}

const activate_deactivate = async (data: any) => {
    try {

        let { _id, account_status } = data

        let query = { _id: _id }
        let update = { account_status: account_status }
        let options = { new: true }
        let response = await DAO.find_and_update(Models.Users, query, update, options)

        // remove login details
        if (account_status == 'DEACTIVATED') {
            let query = { user_id: _id }
            await DAO.remove_many(Models.Sessions, query)
        }

        return response

    }
    catch (err) {
        throw err;
    }
}

const fetch_total_count = async (collection: any, query: any) => {
    try {

        let response = await DAO.count_data(collection, query)
        return response

    }
    catch (err) {
        throw err;
    }
}

const fetch_recent_users = async () => {
    try {

        let query = { is_deleted: false }
        let projection = { __v: 0, password: 0, otp: 0, fp_otp: 0 }
        let options = { lean: true, sort: { _id: -1 }, limit: 20 }
        let response = await DAO.get_data(Models.Users, query, projection, options)
        return response

    }
    catch (err) {
        throw err;
    }
}


const generate_user_token = async (_id: string, req_data: any) => {
    try {

        let token_data = {
            _id: _id,
            scope: user_scope,
            collection: Models.Users,
            token_gen_at: +new Date()
        }
        let response = await fetch_user_token(token_data, req_data)
        return response

    }
    catch (err) {
        throw err;
    }
}


const fetch_user_token = async (token_data: any, req_data: any) => {
    try {

        let access_token: any = await generate_token(token_data)
        let response = await save_user_session_data(access_token, token_data, req_data)
        return response

    }
    catch (err) {
        throw err;
    }
}


const save_user_session_data = async (access_token: string, token_data: any, req_data: any) => {
    try {

        let { _id: user_id, token_gen_at } = token_data, { device_type, fcm_token } = req_data

        let set_data: any = {
            type: "USER",
            user_id: user_id,
            access_token: access_token,
            token_gen_at: token_gen_at,
            created_at: +new Date()
        }
        if (device_type != null || device_type != undefined) { set_data.device_type = device_type }
        if (fcm_token != null || fcm_token != undefined) { set_data.fcm_token = fcm_token }
        let response = await DAO.save_data(Models.Sessions, set_data)

        return response

    }
    catch (err) {
        throw err;
    }
}

const make_user_response = async (token_data: any) => {
    try {

        let { user_id, access_token, device_type, fcm_token, language, token_gen_at } = token_data

        let query = { _id: user_id }
        let projection = { __v: 0, password: 0, otp: 0, fp_otp: 0, unique_code: 0 }
        let options = { lean: true }
        let response: any = await DAO.get_data(Models.Users, query, projection, options)

        if (response.length) {

            response[0].access_token = access_token
            response[0].device_type = device_type
            response[0].fcm_token = fcm_token
            response[0].token_gen_at = token_gen_at
            return response[0]

        } else {
            throw await handle_custom_error('INVALID_OBJECT_ID', language) 
        }

    }
    catch (err) {
        throw err;
    }
}

const fetch_user_data = async (query: any, options: any) => {
    try {

        let projection = { password: 0, otp: 0, fp_otp: 0, unique_code: 0, __v: 0 }
        let response = await DAO.get_data(Models.Users, query, projection, options)
        return response

    }
    catch (err) {
        throw err;
    }
}

const check_content = async (type: string) => {
    try {

        let query = { type: type }
        let projection = { __v: 0 }
        let options = { lean: true }
        return await DAO.get_data(Models.Content, query, projection, options)

    }
    catch (err) {
        throw err;
    }
}

const verify_admin_info = async (query: any) => {
    try {

        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data = await DAO.get_data(Models.Admin, query, projection, options)
        return fetch_data

    }
    catch (err) {
        throw err;
    }
}

const set_staff_data = async (data: any, language : string, type: string) => {
    try {

        let set_data: any = {
            name: data.name,
            roles: data.roles
        }
        if (data.password) { 
            let hassed_password = await helpers.bcrypt_password(data.password)
            set_data.password = hassed_password
        }
        if (data.image) { set_data.image = data.image }
        if (data.email) {

            // check other email
            let email = data.email.toLowerCase()
            if (type == 'UPDATE') {

                let query = { _id: { $ne: data._id }, email: email }
                let fetch_data: any = await verify_admin_info(query)
                if (fetch_data.length) { 
                    throw await handle_custom_error('EMAIL_ALREADY_EXISTS', language)  
                }
                else { set_data.email = email }

            } else {

                let query = { email: email }
                let fetch_data: any = await verify_admin_info(query)
                if (fetch_data.length) { 
                    throw await handle_custom_error('EMAIL_ALREADY_EXISTS', language)  
                }
                else { set_data.email = email }

            }

        }

        return set_data

    }
    catch (err) {
        throw err;
    }
}


const make_products_response = async (query: any, options: any) => {
    try {

        let projection = { __v: 0 }
        let populate = [
            { path : 'added_by', select : 'profile_pic name' }
        ]
        let respone = await DAO.populate_data(Models.Products, query, projection, options, populate)
        return respone

    }
    catch (err) {
        throw err;
    }
}


export {
    generate_admin_token,
    fetch_admin_token,
    save_session_data,
    update_language,
    make_admin_response,
    block_delete_data,
    verify_unverify,
    activate_deactivate,
    fetch_total_count,
    fetch_recent_users,
    generate_user_token,
    make_user_response,
    fetch_user_data,
    check_content,
    verify_admin_info,
    set_staff_data,
    make_products_response
}