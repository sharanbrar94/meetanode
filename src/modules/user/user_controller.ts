
import * as DAO from '../../DAO';
import * as express from 'express';
import * as Models from '../../models';
import * as user_services from './user_services';
import * as email_services from './email_services'
import { handle_success, handle_catch, handle_custom_error, helpers } from '../../middlewares/index';


const signup = async (req: express.Request, res: express.Response) => {
    try {

        let { email, phone_no, language } = req.body;

        // verify email address
        let query_email = { email: email.toLowerCase() }
        let fetch_data: any = await user_services.verify_user_info(query_email)
        if (fetch_data.length) {
            throw await handle_custom_error('EMAIL_ALREADY_EXISTS', language)
        }
        else {

            // verify phone_no
            let query_phone_no = { phone_no: phone_no }
            let verify_data: any = await user_services.verify_user_info(query_phone_no)
            if (verify_data.length) {
                throw await handle_custom_error('PHONE_NO_ALREADY_EXISTS', language)
            }
            else {

                // create new user
                let create_user = await user_services.set_user_data(req.body)
                let { _id } = create_user

                // generate access token
                let generate_token: any = await user_services.generate_user_token(_id, req.body)

                // fetch user response
                let response = await user_services.make_user_response(generate_token, language)

                // send welcome email to user
                await email_services.send_welcome_mail(create_user, language)

                // return response
                handle_success(res, response)

            }

        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const email_verification = async (req: any, res: express.Response) => {
    try {

        let { otp: input_otp, language } = req.body, { _id, otp } = req.user_data,
        session_data = req.session_data

        if (otp == input_otp) {

            let query = { _id: _id }
            let update = { email_verified: true }
            let options = { new: true }
            await DAO.find_and_update(Models.Users, query, update, options)

            // fetch user response
            let response = await user_services.make_user_response(session_data, language)

            // return response
            handle_success(res, response)

        } else {
            throw await handle_custom_error('WRONG_OTP', language)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const resend_otp = async (req: any, res: express.Response) => {
    try {

        let { email, language } = req.body;

        let query_email = { email: email.toLowerCase() }
        let fetch_data: any = await user_services.verify_user_info(query_email)

        if (fetch_data.length) {

            let { _id } = fetch_data[0]

            // update user otp
            let update_data = await user_services.update_otp(_id)

            // generate access token
            let generate_token: any = await user_services.generate_user_token(_id, req.body)

            // fetch user response
            let response = await user_services.make_user_response(generate_token, language)

            // send email
            await email_services.resend_otp_mail(update_data, language)

            // return response
            handle_success(res, response)


        } else {
            throw handle_catch('EMAIL_NOT_REGISTERED', language)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}



const social_login = async (req: express.Request, res: express.Response) => {
    try {

        // console.log("---------------------req.body--",req.body)

        let { social_type,social_token,name,username,email,image,cover_image } = req.body;
        let language = "ENGLISH"
        if(social_type == undefined && social_token == undefined){
            console.log("hello")
            throw("Social type and social token is required")
        }
        let query = {
            social_type: social_type,
            social_token: social_token,
            is_deleted: false
        }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Users, query, projection, options)
        if (fetch_data.length > 0) {

            var id  = fetch_data[0]._id

            // generate new token
            let generate_token: any = await user_services.generate_user_token(id, req.body)

            // populate user_data
            let response = await user_services.make_user_response(generate_token, language)

            // return response
            handle_success(res, response)

        } else {

            var response:any = await user_services.create_new_user(req.body)
            let {_id} = response
            
            // generate new token
            let generate_token = await user_services.generate_user_token(_id, req.body)

            // populate user_data
            let populate_data = await user_services.make_user_response(generate_token, language)

            // return response
            handle_success(res, populate_data)

        }


    }
    catch (err) {
        handle_catch(res, err);
    }
}


const login = async (req: express.Request, res: express.Response) => {
    try {

        let { email, password } = req.body;
        let language = "ENGLISH";

        let query = { email: email.toLowerCase(), is_deleted: false }
        let projection = {}
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Users, query, projection, options)

        if (fetch_data.length) {

            let { _id, password: hash, is_blocked } = fetch_data[0]

            let decrypt = await helpers.decrypt_password(password, hash)
            if (decrypt != true) {
                throw await handle_custom_error('INCORRECT_PASSWORD', language)
            }
            if (is_blocked == true) {
                throw await handle_custom_error('ACCOUNT_BLOCKED', language)
            }
            else {

                // generate access token
                let generate_token: any = await user_services.generate_user_token(_id, req.body)

                // fetch user response
                let response = await user_services.make_user_response(generate_token, language)

                // return response
                handle_success(res, response)

            }


        } else {
            throw await handle_custom_error('EMAIL_NOT_REGISTERED', language)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const forgot_password = async (req: any, res: express.Response) => {
    try {

        let { email } = req.body
        let language = "ENGLISH"

        // verify email address
        let query_email = { email: email.toLowerCase() }
        let fetch_data: any = await user_services.verify_user_info(query_email)

        if (fetch_data.length) {

            let { _id, full_name } = fetch_data[0]

            let unique_code = await helpers.gen_unique_code()

            let query = { _id: _id }
            let update = { unique_code: unique_code }
            let options = { new: true }
            let update_data : any = await DAO.find_and_update(Models.Users, query, update, options)

            // send email
            await email_services.forgot_password_mail(update_data, language)

            let message = "Mail sent sucessfully"
            let response = { message: message }

            // return response
            handle_success(res, response)

        } else {
            throw await handle_custom_error('EMAIL_NOT_REGISTERED', language)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}


const verify_otp = async (req: any, res: express.Response) => {
    try {

        let { otp: input_otp, email } = req.body
        let language = "ENGLISH"

        // verify email address
        let query_email = { email: email.toLowerCase() }
        let fetch_data: any = await user_services.verify_user_info(query_email)

        if (fetch_data.length) {

            let { _id, fp_otp } = fetch_data[0]
            if (input_otp != fp_otp) {
                throw await handle_custom_error('WRONG_OTP', language)
            }
            else {

                // generate unique code
                let unique_code = await helpers.gen_unique_code()

                let query = { _id: _id }
                let update = { unique_code: unique_code }
                let options = { new: true }
                await DAO.find_and_update(Models.Users, query, update, options)

                // generate new token
                // let generate_token = await user_services.generate_user_token(_id, req.body)

                // fetch user response
                // let response = await user_services.make_user_response(generate_token)

                let response = { unique_code: unique_code }

                // return response
                handle_success(res, response)

            }

        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const set_new_password = async (req: any, res: express.Response) => {
    try {

        let { password, unique_code, language } = req.body;

        let query = {
            $and: [
                { unique_code: { $ne: null } },
                { unique_code: unique_code }
            ]
        }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Users, query, projection, options)

        if (fetch_data.length) {

            let { _id } = fetch_data[0]

            // bycryt password
            let bycryt_password = await helpers.bcrypt_password(password)

            let query = { _id: _id }
            let update = {
                password: bycryt_password,
                unique_code: null,
                fp_otp: 0
            }
            let options = { new: true }
            await DAO.find_and_update(Models.Users, query, update, options)

            // // generate new token
            // let generate_token = await user_services.generate_user_token(_id, req.body)

            // // fetch user response
            // let response = await user_services.make_user_response(generate_token)

            let message = "Password Changed Sucessfully"
            let response = { message: message }

            // return password
            handle_success(res, response)

        } else {
            throw await handle_custom_error('NO_DATA_FOUND', language)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const view_my_profile = async (req: any, res: express.Response) => {
    try {
        let { _id } = req.user_data
        let response = await user_services.get_user_profile(_id)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

const edit_profile = async (req: any, res: express.Response) => {
    try {

        let { _id } = req.user_data, session_data = req.session_data
        let language = "ENGLISH"
        let query = { _id: _id }
        let update = await user_services.edit_profile_data(req.body, req.user_data)
        let options = { new: true }
        await DAO.find_and_update(Models.Users, query, update, options)

        // fetch response
        let response = await user_services.make_user_response(session_data, language)

        // return respone
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const change_password = async (req: any, res: express.Response) => {
    try {

        let { old_password, new_password, language } = req.body, { _id } = req.user_data,
        session_data = req.session_data;

        let query = { _id: _id }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data: any = await DAO.get_data(Models.Users, query, projection, options)

        if (fetch_data.length) {

            let { _id, password } = fetch_data[0]
            let decrypt = await helpers.decrypt_password(old_password, password)
            if (decrypt != true) {
                throw await handle_custom_error('OLD_PASSWORD_MISMATCH', language)
            }
            else {

                // bycryt password
                let bycryt_password = await helpers.bcrypt_password(new_password)

                let query = { _id: _id }
                let update = { password: bycryt_password }
                let options = { new: true }
                await DAO.find_and_update(Models.Users, query, update, options)

                // fetch response
                let response = await user_services.make_user_response(session_data, language)

                // return password
                handle_success(res, response)

            }

        } else {
            throw await handle_custom_error('UNAUTHORIZED', language)
        }

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const logout = async (req: any, res: express.Response) => {
    try {

        let { _id } = req.session_data;

        let query = { _id: _id }
        await DAO.remove_data(Models.Sessions, query)

        let message = "Logout Sucessfull"
        let response = { message: message }

        // return response
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

const contact_us = async (req: any, res: express.Response) => {
    try {

        let { name, email, country_code, phone_no, message } = req.body, { _id } = req.session_data;

        let data_to_save = {
            user_id: _id,
            name: name,
            email: email.toLowerCase(),
            country_code: country_code,
            phone_no: phone_no,
            message: message,
            created_at: +new Date()
        }
        let response = await DAO.save_data(Models.ContactUs, data_to_save)

        // return response
        handle_success(res, response)

    }
    catch (err) {
        handle_catch(res, err);
    }
}


const list_content = async (req: express.Request, res: express.Response) => {
    try {

        let { type } = req.query, query: any = {}
        if (type != undefined) { query.type = type }

        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data = await DAO.get_data(Models.Content, query, projection, options)

        // return data
        handle_success(res, fetch_data)

    }
    catch (err) {
        handle_catch(res, err);
    }
}

export {
    signup,
    email_verification,
    resend_otp,
    social_login,
    login,
    forgot_password,
    verify_otp,
    set_new_password,
    view_my_profile,
    edit_profile,
    change_password,
    logout,
    contact_us,
    list_content
}