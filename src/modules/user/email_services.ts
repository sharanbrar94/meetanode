import * as DAO from '../../DAO';
import * as Models from '../../models';
import { handle_custom_error, send_email } from '../../middlewares/index';


const fetch_template = async (data: any, type: string, language: string) => {
    try {

        let query = { type: type }
        let projection = { __v: 0 }
        let options = { lean: true }
        let populate = [{ path: 'variables', select: 'name' }]
        let fetch_data: any = await DAO.populate_data(Models.Templates, query, projection, options, populate)

        if (fetch_data.length) {

            let { subject, html, variables } = fetch_data[0]    

            let variable_names: any = [];
            if (variables.length) {
                for (let value of variables) {
                    console.log("---------value---",value)
                    variable_names.push(value.name)
                }
            }

            for (let key of variable_names) {

                console.log("---------key---",key)

                let data_key = key.toLowerCase()
                html = html.replace(`%${key}%`, data[data_key])

            }

            return {
                subject: subject,
                html: html
            }

        } else {
            throw await handle_custom_error('NO_DATA_FOUND', language)
        }

    }
    catch (err) {
        throw err;
    }
}

const send_welcome_mail = async (data: any, language: string) => {
    try {

        let { name, email, otp } = data
        let template_data = {
            user_name : name,
            otp : otp
        }

        let type = "WELCOME_EMAIL"
        let fetch_data = await fetch_template(template_data, type, language)
        let { subject, html } = fetch_data

        await send_email(email, subject, html)

    }
    catch (err) {
        throw err;
    }
}


const resend_otp_mail = async (data: any, language : string) => {
    try {

        let { name, email, otp } = data
        let template_data = {
            user_name : name,
            otp : otp
        }

        let type = "RESEND_OTP_MAIL"
        let fetch_data = await fetch_template(template_data, type, language)
        let { subject, html } = fetch_data

        await send_email(email, subject, html)

    }
    catch (err) {
        throw err;
    }
}


const forgot_password_mail = async (data: any, language : string) => {
    try {

        let { email, unique_code, name } = data
        // let url = `https://nftyit.com:3000/forgot-password?unique-code=${unique_code}`

        let type = "RESET_PASSWORD_MAIL"
        let template_data = {
            user_name : name,
            url : unique_code
        }

        let fetch_data = await fetch_template(template_data, type, language)
        let { subject, html } = fetch_data
        await send_email(email, subject, html)

    }
    catch (err) {
        throw err;
    }
}


export {
    send_welcome_mail,
    resend_otp_mail,
    forgot_password_mail
}