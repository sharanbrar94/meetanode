import * as DAO from '../../DAO';
import * as Models from '../../models';
import mongoose from 'mongoose';
import { app_constant, error } from '../../config/index';
import { generate_token, handle_custom_error, helpers } from '../../middlewares/index';
const user_scope = app_constant.scope.user;



const get_user_profile = async (_id: any) => {
    try {

        //let query = { _id }
        let projection = { __v: 0, password: 0, otp: 0 }
       // let options = { lean: true }
        //let response: any = await DAO.get_data(Models.Events, query, projection, options)

        let query = [
            await match_user_by_id(_id),
            await look_up_event_events(),
            await look_up_join_events()
        ]
        let options = { lean: true }
        let fetch_data : any = await DAO.aggregate_data(Models.Users, query, options)
        return fetch_data
    }
    catch (err) {
        throw err;
    }
}
const look_up_event_events = async () => {
    try {

        return {
            $lookup: {
                from: "events",
                let: { user_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$user_id", "$$user_id"]
                            }
                        }
                    }
                    
                    
                ],
                as: "created_events"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_join_events = async () => {
    try {

        return {
            $lookup: {
                from: "join_events",
                let: { user_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$user_id", "$$user_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "events",
                            let: { event_id: "$event_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$event_id"]
                                        }
                                    }
                                }
                            ],
                            as: "events"
                        }
                    },
                    {
                        $unwind:"$events"
                    },
                    {
                        $group: {
                            _id: "$events._id",
                            event_name: {"$last":"$events.event_name"},
                            slug: {"$last":"$events.slug"},
                            start_date: {"$last":"$events.start_date"},
                            end_date: {"$last":"$events.end_date"},
                            country: {"$last":"$events.country"},
                            state: {"$last":"$events.state"},
                            city: {"$last":"$events.city"},
                            address: {"$last":"$events.address"}
                            //total_event_attendees: await fetch_total_event_attendees("$events._id")
                        } 
                    }
                ],
                as: "joined_events"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const fetch_total_event_attendees = async (event_id: any) => {
    try {
        let query = { event_id }
        let projection = { __v: 0}
        let options = { lean: true }
        let response: any = await DAO.get_data(Models.JoinEvents, query, projection, options)
        return response.length
    }
    catch (err) {
        throw err;
    }
}
const match_user_by_id = async (user_id: string) => {
    try {
        return {
            $match: {
                _id : mongoose.Types.ObjectId(user_id)
            }
        }
    }
    catch (err) {
        throw err;
    }
}

const generate_user_token = async (_id: string, req_data: any) => {
    try {

        let token_data = {
            _id: _id,
            scope: user_scope,
            collection: Models.Users,
            token_gen_at: +new Date()
        }
        let response = await fetch_user_token(token_data, req_data)
        return response

    }
    catch (err) {
        throw err;
    }
}


const fetch_user_token = async (token_data: any, req_data: any) => {
    try {

        let access_token: any = await generate_token(token_data)
        let response = await save_session_data(access_token, token_data, req_data)
        return response

    }
    catch (err) {
        throw err;
    }
}

const update_otp = async (user_id: string) => {
    try {

        // generate otp
        let otp = await helpers.generate_otp()

        let query = { _id: user_id }
        let update = { otp: otp }
        let options = { new: true }
        return await DAO.find_and_update(Models.Users, query, update, options)

    }
    catch (err) {
        throw err;
    }
}

const save_session_data = async (access_token: string, token_data: any, req_data: any) => {
    try {

        let { _id: user_id, token_gen_at } = token_data, { device_type, fcm_token } = req_data

        let set_data: any = {
            type: "USER",
            user_id: user_id,
            access_token: access_token,
            token_gen_at: token_gen_at,
            created_at: +new Date()
        }
        if (device_type != null || device_type != undefined) { set_data.device_type = device_type }
        if (fcm_token != null || fcm_token != undefined) { set_data.fcm_token = fcm_token }
        let response = await DAO.save_data(Models.Sessions, set_data)

        return response

    }
    catch (err) {
        throw err;
    }
}

const make_user_response = async (token_data: any, language: string) => {
    try {

        let { user_id, access_token, device_type, fcm_token, token_gen_at } = token_data

        let query = { _id: user_id }
        let projection = { __v: 0, password: 0, otp: 0 }
        let options = { lean: true }
        let response: any = await DAO.get_data(Models.Users, query, projection, options)

        if (response.length) {

            response[0].access_token = access_token
            response[0].device_type = device_type
            response[0].fcm_token = fcm_token
            response[0].token_gen_at = token_gen_at
            return response[0]

        } else {
            throw await handle_custom_error('INVALID_OBJECT_ID', language)
        }

    }
    catch (err) {
        throw err;
    }
}


const block_delete_data = async (data: any, collection: any) => {
    try {

        let { _id, is_blocked, is_deleted } = data;

        let query = { _id: _id }
        let data_to_update: any = {}
        if (typeof is_blocked !== "undefined" && is_blocked !== null) {
            data_to_update.is_blocked = is_blocked
        }
        if (typeof is_deleted !== "undefined" && is_deleted !== null) {
            data_to_update.is_deleted = is_deleted
        }
        let options = { new: true }
        let response = await DAO.find_and_update(collection, query, data_to_update, options)

        return response

    }
    catch (err) {
        throw err;
    }
}

const verify_user_info = async (query: any) => {
    try {

        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data = await DAO.get_data(Models.Users, query, projection, options)
        return fetch_data

    }
    catch (err) {
        throw err;
    }
}

const set_user_data = async (data: any) => {
    try {

        let { name, email, country_code, phone_no, password } = data

        // bycryt password
        let hassed_password = await helpers.bcrypt_password(password)

        // fetch otp
        let otp = await helpers.generate_otp()

        let data_to_save: any = {
            name: name,
            email: email.toLowerCase(),
            country_code: country_code,
            phone_no: phone_no,
            password: hassed_password,
            otp: otp,
            created_at: +new Date()
        }
        if (data.profile_pic) { data_to_save.profile_pic = data.profile_pic }

        let response: any = await DAO.save_data(Models.Users, data_to_save)
        return response

    }
    catch (err) {
        throw err;
    }
}

const create_new_user = async (data: any) => {
    try {

        let { social_type,social_token, name,email, username, image, cover_image } = data

        let data_to_save: any = {
            social_type: social_type,
            social_token: social_token,
            username: username,
            image:image,
            cover_image:cover_image,
            created_at: +new Date()
        }
        if (email != null || email != undefined) { data_to_save.email = email.toLowerCase() }
        if (name != null || email != undefined) { data_to_save.name = name }
        let response = await DAO.save_data(Models.Users, data_to_save)
        return response

    }
    catch (err) {
        throw err;
    }
}


const edit_profile_data = async (data: any, user_data: any) => {
    try {

        let set_data: any = {}, { _id: user_id } = user_data

        if (data.image) { set_data.image = data.image }
        if (data.cover_image) { set_data.cover_image = data.cover_image }
        return set_data

    }
    catch (err) {
        throw err;
    }
}




export {
    generate_user_token,
    fetch_user_token,
    update_otp,
    save_session_data,
    make_user_response,
    block_delete_data,
    verify_user_info,
    set_user_data,
    create_new_user,
    edit_profile_data,
    get_user_profile
}