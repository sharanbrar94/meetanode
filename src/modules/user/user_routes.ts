
import express from 'express';
import * as user_controller from './user_controller';
import authenticator from '../../middlewares/authenticator';
const router = express.Router();

router.post("/email_verification", authenticator, user_controller.email_verification)
router.post("/resend_otp", user_controller.resend_otp)
router.post("/social_login", user_controller.social_login)
router.put("/forgot_password", user_controller.forgot_password)
router.post("/verify_otp", user_controller.verify_otp)
router.post("/set_new_password", user_controller.set_new_password)
router.get("/view_my_profile", authenticator, user_controller.view_my_profile)
router.put("/edit_profile", authenticator, user_controller.edit_profile)
router.put("/change_password", authenticator, user_controller.change_password)
router.put("/logout", authenticator, user_controller.logout)
router.post("/contact_us", authenticator, user_controller.contact_us)
router.get("/list_content", user_controller.list_content)


export default router;