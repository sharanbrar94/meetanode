
import * as DAO from '../../DAO';
import * as express from 'express';
import * as Models from '../../models';
import * as event_services from './event_services';
import * as email_services from './email_services'
import { handle_success, handle_catch, handle_custom_error, helpers } from '../../middlewares/index';
import {
    common_helpers
} from '../../../helpers/index.helpers';

/*============Get category list which are not deleted to add event==========*/
const getStaticData = async (req: express.Request, res: express.Response) => {
    try {
        
        let response = await event_services.get_static_data()
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}


/*============Add event api==========*/
const addEvent = async (req: express.Request, res: express.Response) => {
    try {
        let { _id: user_id } = req.user_data
        let { event_name,slug} = req.body;
        let language = "ENGLISH"
        let create_event = await event_services.set_event_data(req.body,user_id)
        let response = await event_services.make_event_response(slug,language)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*============Edit event api==========*/
const editEvent = async (req: express.Request, res: express.Response) => {
    try {

        let { event_id,event_name,country,state,city,address,event_category,slug,start_date,end_date,tagline,overview,requirements,cover_photo,twitter,website,event_type,private_event} = req.body;
        let create_event = await event_services.edit_event_data(req.body)
        handle_success(res, create_event)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*============Get event by event id api==========*/
const getEvent = async (req: express.Request, res: express.Response) => {
    try {
        let {slug} = req.query;
        let language = "ENGLISH"
        let response = await event_services.make_event_response(slug,language)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*============Get all events api==========*/
const getAllEvents = async (req: express.Request, res: express.Response) => {
    try {
        let {  page, limit } = req.query, query: any = {}
        // if (search != undefined) { query.name = { $regex: search, $options: 'i' } }

        let projection = { __v: 0 }
        let options = await common_helpers.set_options(page, limit)
        let fetch_data:any = await DAO.get_data(Models.Events, query, projection, options)
        if (fetch_data.length) {
            for (let value of fetch_data) {
                value.total_event_attendees = 0
                let event_id = value._id
                let getAttendees: any = await event_services.fetch_total_event_attendees(event_id)
                if(getAttendees.length){
                    value.total_event_attendees = getAttendees.length
                }
            }
        }
        let total_count = await event_services.fetch_total_count(Models.Events, query)
        let response = {
            total_count: total_count,
            events : fetch_data
        }
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*============Delete event api==========*/
const deleteEvent = async (req: express.Request, res: express.Response) => {
    try {
        let {event_id} = req.body;
        let response = await event_services.delete_event(event_id)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*============Joine event by user api==========*/
const joinEvent = async (req: express.Request, res: express.Response) => {
    try {
        let { _id: user_id } = req.user_data
        let { event_id} = req.body;
        let response = await event_services.join_event(req.body,user_id)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

/*==================FAQ module==============*/
const addFaq = async (req: express.Request, res: express.Response) => {
    try {
        let {event_id,question,answer} = req.body;
        let response = await event_services.add_faq(req.body)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const editFaq = async (req: express.Request, res: express.Response) => {
    try {
        let {faq_id,question,answer} = req.body;
        let response = await event_services.edit_faq(req.body)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const getEventFaqs = async (req: express.Request, res: express.Response) => {
    try {
        let {event_id} = req.query;
        let response = await event_services.get_event_faqs(event_id)
        
        // return response
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const deleteFaq = async (req: express.Request, res: express.Response) => {
    try {
        let {faq_id} = req.body;
        let response:any = await event_services.delete_faq(req.body)
        if (response.deletedCount > 0) {
            let data = { message: `Faq deleted successfully...` }
            handle_success(res, data)
        }
    }
    catch (err) {
        handle_catch(res, err);
    }
}


//speaker module
const addSpeaker = async (req: express.Request, res: express.Response) => {
    try {
        let {name,title,topic} = req.body;
        let language = "ENGLISH"
        let create_speaker = await event_services.add_speaker(req.body)
        let { _id } = create_speaker
        let response = await event_services.make_speaker_response(_id,language)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const editSpeaker = async (req: express.Request, res: express.Response) => {
    try {
        let {speaker_id,event,title,topic} = req.body;
        let language = "ENGLISH"
        let response = await event_services.edit_speaker(req.body)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const getSpeakerDetail = async (req: express.Request, res: express.Response) => {
    try {
        let {speaker_id} = req.query;
        let response = await event_services.speaker_detail(req.query)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const deleteSpeaker = async (req: express.Request, res: express.Response) => {
    try {
        let {speaker_id} = req.body;
        let response:any = await event_services.delete_speaker(req.body)
        if (response.deletedCount > 0) {
            let data = { message: `Speaker deleted successfully...` }
            handle_success(res, data)
        }
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const getEventSpeakers = async (req: express.Request, res: express.Response) => {
    try {
        let {event_id} = req.query;
        let language = "ENGLISH"
        // get not deleted categories
        let response = await event_services.event_speakers(event_id)
        
        // return response
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}

//Schedule module
const addSchedule = async (req: express.Request, res: express.Response) => {
    try {
        let {date,start_time,end_time,venue,event_id} = req.body;
        let response = await event_services.add_schedule(req.body)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const editSchedule = async (req: express.Request, res: express.Response) => {
    try {
        let {schedule_id,date,start_time,end_time,venue} = req.body;
        let response = await event_services.edit_schedule(req.body)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const addScheduleSpeaker = async (req: express.Request, res: express.Response) => {
    try {
        let {schedule_id,speaker_id} = req.body;
        let response = await event_services.add_schedule_speaker(req.body)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const getScheduleSpeakers = async (req: express.Request, res: express.Response) => {
    try {
        let {schedule_id} = req.query;
        let response = await event_services.get_schedule_speakers(req.query)
        handle_success(res, response)
    }
    catch (err) {
        handle_catch(res, err);
    }
}
const deleteScheduleSpeaker = async (req: express.Request, res: express.Response) => {
    try {
        let {schedule_speaker_id} = req.body;
        let response:any = await event_services.delete_schedule_speaker(req.body)
        if (response.deletedCount > 0) {
            let data = { message: `Deleted successfully...` }
            handle_success(res, data)
        }
    }
    catch (err) {
        handle_catch(res, err);
    }
}


//categoru module


export {
    addEvent,
    editEvent,
    deleteEvent,
    getEvent,
    addSpeaker,
    editSpeaker,
    deleteSpeaker,
    getAllEvents,
    getStaticData,
    getEventSpeakers,
    joinEvent,
    addSchedule,
    editSchedule,
    addScheduleSpeaker,
    deleteScheduleSpeaker,
    getSpeakerDetail,
    getScheduleSpeakers,
    addFaq,
    editFaq,
    deleteFaq,
    getEventFaqs
}