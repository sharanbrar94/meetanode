import * as DAO from '../../DAO';
import mongoose from 'mongoose';
import * as Models from '../../models';
import { app_constant, error } from '../../config/index';
import { generate_token, handle_custom_error, helpers } from '../../middlewares/index';
const user_scope = app_constant.scope.user;

const make_event_response = async (slug: any, language: string) => {
    try {
        let eventQuery = { slug:slug }
        let projection = { __v: 0}
        let eventOptions = { lean: true }
        let response: any = await DAO.get_data(Models.Events, eventQuery, projection, eventOptions)
        let event_id = response[0]._id
        let query = [
            await match_event_by_id(event_id),
            await look_up_event_requirements(),
            await look_up_event_categories(),
            await look_up_speakers(),
            await look_up_event_faqs(),
            await look_up_event_schedules(),
            await look_up_event_attendees(),
        ]
        let options = { lean: true }
        let fetch_data : any = await DAO.aggregate_data(Models.Events, query, options)
        console.log("ddsa",fetch_data)
        return fetch_data[0]
        // if (fetch_data.length) {
        //     return fetch_data[0]
        // } else {
        //     throw await handle_custom_error('INVALID_OBJECT_ID', language)
        // }
    }
    catch (err) {
        throw err;
    }
}
const look_up_event_schedules = async() =>{
    try {

        return {
            $lookup: {
                from: "event_schedules",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "schedule_speakers",
                            let: { schedule_id: "$_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$schedule_id", "$$schedule_id"]
                                        },
                                    },
                                    
                                },
                                {
                                    $lookup: {
                                        from: "event_speakers",
                                        let: { speaker_id: "$speaker_id" },
                                        pipeline: [
                                            {
                                                $match: {
                                                    $expr: {
                                                        $eq: ["$_id", "$$speaker_id"]
                                                    },
                                                },
                                                
                                            }
                                        ],
                                        as: "speakers"
                                    }
                                },
                                {
                                    "$unwind":"$speakers"
                                },
                                {
                                    $group: {
                                        _id: "$_id",
                                        name:{"$last":"$speakers.name"},
                                        title:{"$last":"$speakers.title"},
                                        topic:{"$last":"$speakers.topic"}
                                        
                                        
                                    } 
                                }

                            ],
                            as: "event_speakers"
                        }
                    }
                    
                ],
                as: "event_schedules"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_event_attendees = async () =>{
    try {

        return {
            $lookup: {
                from: "join_events",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "users",
                            let: { user_id: "$user_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$user_id"]
                                        },
                                    },
                                    
                                }
                            ],
                            as: "users"
                        }
                    },
                    {
                        "$unwind":"$users"
                    },
                    {
                        $group: {
                            _id: "$_id",
                            username:{"$last":"$users.username"},
                            name:{"$last":"$users.name"},
                            image:{"$last":"$users.image"}
                            
                            
                        } 
                    }
                ],
                as: "event_attendees"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_event_categories = async () =>{
    try {

        return {
            $lookup: {
                from: "event_categories",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "categories",
                            let: { category_id: "$category_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$category_id"]
                                        },
                                    },
                                    
                                }
                            ],
                            as: "categories"
                        }
                    },
                    {
                        "$unwind":"$categories"
                    },
                    {
                        $group: {
                            _id: "$_id",
                            name:{"$last":"$categories.name"}
                            
                            
                        } 
                    }
                ],
                as: "event_categories"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_event_faqs = async () =>{
    try {

        return {
            $lookup: {
                from: "event_faqs",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    }
                    
                ],
                as: "event_faqs"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_speakers = async () =>{
    try {

        return {
            $lookup: {
                from: "event_speakers",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    }
                    
                ],
                as: "event_speakers"
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const look_up_event_requirements = async () => {
    try {

        return {
            $lookup: {
                from: "event_requirements",
                let: { event_id: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$event_id", "$$event_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "requirements",
                            let: { requirement_id: "$requirement_id" },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$requirement_id"]
                                        },
                                    },
                                    
                                }
                            ],
                            as: "requirements"
                        }
                    },
                    {
                        "$unwind":"$requirements"
                    },
                    {
                        $group: {
                            _id: "$_id",
                            name:{"$last":"$requirements.name"}
                            
                            
                        } 
                    }
                    
                ],
                as: "event_requirements"
            }
            
        }

    }
    catch (err) {
        throw err;
    }
}
const match_event_by_id = async (id: string) => {
    try {
        console.log("test",id)

        return {
            $match: {
                _id : mongoose.Types.ObjectId(id)
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const fetch_total_event_attendees = async (event_id: any) => {
    try {
        let query = { event_id }
        let projection = { __v: 0}
        let options = { lean: true }
        let response: any = await DAO.get_data(Models.JoinEvents, query, projection, options)
        return response
    }
    catch (err) {
        throw err;
    }
}
const fetch_total_count = async (collection: any, query: any) => {
    try {

        let response = await DAO.count_data(collection, query)
        return response

    }
    catch (err) {
        throw err;
    }
}
const set_event_data = async (data: any,user_id:any) => {
    try {
        let { event_name,slug} = data

        let query = { slug:slug }
        let projection = { __v: 0}
        let options = { lean: true }
        let checkEvent: any = await DAO.get_data(Models.Events, query, projection, options)
        if(checkEvent.length > 0){
            throw("Slug already exists")
        }

        let data_to_save: any = {
            event_name: event_name,
            slug: slug,
            user_id:user_id,
            created_at: +new Date()
        }
        let response: any = await DAO.save_data(Models.Events, data_to_save)
        return response
    }
    catch (err) {
        throw err;
    }
}
const edit_event_data = async (data: any) => {
    try {
        let { event_id,event_name,country,state,city,address,event_category,slug,start_date,end_date,tagline,overview,requirements,cover_photo,twitter,website,event_type,private_event } = data
        console.log("requirements",requirements)
        console.log("event_category",event_category)
        let set_data: any = {}
        let query = { _id: event_id }
        let options = { new: true }
        
        set_data.event_name= event_name
        set_data.country= country,
        set_data.state= state,
        set_data.city= city,
        set_data.address= address
        set_data.slug= slug
        set_data.start_date= start_date
        set_data.end_date=end_date
        set_data.tagline= tagline
        set_data.overview=overview
        set_data.cover_photo=cover_photo
        set_data.twitter=twitter
        set_data.website=website
        set_data.event_type = event_type
        set_data.private_event = private_event
       
        let response: any = await DAO.find_and_update(Models.Events,query, set_data,options)
        if(requirements){
            await requirements.forEach(async r => {
                let get_event_id = event_id
                let req_query = { event_id: get_event_id,requirement_id:r.requirements }
                let projection = { __v: 0}
                let options = { lean: true }
                let checkOldReq: any = await DAO.get_data(Models.EventRequirements, req_query, projection, options)
                if(checkOldReq.length == 0){
                    let event_id = response._id
                    let data_to_save_req: any = {
                        event_id: event_id,
                        requirement_id: r.requirements,
                        created_at: +new Date()
                    }
                    await DAO.save_data(Models.EventRequirements, data_to_save_req)
                }
            })
        }
        if(event_category){
            let get_event_id = event_id
            await event_category.forEach(async e => {

                let cat_query = { event_id: get_event_id,category_id:e.category_id }
                let projection = { __v: 0}
                let options = { lean: true }
                let checkOldCat: any = await DAO.get_data(Models.EventCategories, cat_query, projection, options)
                if(checkOldCat.length == 0){
                    let event_id = response._id
                    let data_to_save_cat: any = {
                        event_id: event_id,
                        category_id: e.category_id,
                        created_at: +new Date()
                    }
                    await DAO.save_data(Models.EventCategories, data_to_save_cat)
                }
            })
        }
        let get_data = await make_event_response(slug,"ENGLISH")
        return get_data

    }
    catch (err) {
        throw err;
    }
}
const delete_event = async (data: any) => {
    try {
        let { event_id} = data

        let set_data: any = {}
        let query = { _id: event_id }
        let options = { new: true }
        
        set_data.deleted_at= true
        let response: any = await DAO.find_and_update(Models.Events,query, set_data,options)
        return response

    }
    catch (err) {
        throw err;
    }
}
const join_event = async (data: any,user_id:any) => {
    try {
        let { event_id } = data
        let data_to_save: any = {
            event_id:event_id,
            user_id: user_id,
            created_at: +new Date()
        }
        let response: any = await DAO.save_data(Models.JoinEvents, data_to_save)
        return response

    }
    catch (err) {
        throw err;
    }
}

/*============FAQ module===========*/
const add_faq = async (data: any) => {
    try {

        let { event_id,question,answer} = data
        
        let data_to_save: any = {
            event_id:event_id,
            question: question,
            answer:answer,
            created_at: +new Date()
        }
        let response: any = await DAO.save_data(Models.EventFaqs, data_to_save)
        return response
    }
    catch (err) {
        throw err;
    }
}
const edit_faq = async (data: any) => {
    try {
        let { faq_id,question,answer } = data
        let set_data: any = {}
        let query = { _id: faq_id}
        let options = { new: true }
        
        set_data.question= question
        set_data.answer= answer
        let response: any = await DAO.find_and_update(Models.EventFaqs,query, set_data,options)
        
        return response

    }
    catch (err) {
        throw err;
    }
}
const get_event_faqs = async (event_id: any) => {
    try {
        let query = { event_id }
        let projection = { __v: 0, password: 0, otp: 0 }
        let options = { lean: true }
        let response: any = await DAO.get_data(Models.EventFaqs, query, projection, options)
        return response
    }
    catch (err) {
        throw err;
    }
}
const delete_faq = async (data: any) => {
    try {
        let { faq_id} = data
        
        let query = { _id: faq_id }
        let response =await DAO.remove_many(Models.EventFaqs, query)
        return response

    }
    catch (err) {
        throw err;
    }
}


//speaker module
const add_speaker = async (data: any) => {
    try {

        let { event_id,name,title,topic} = data
        
        let data_to_save: any = {
            event_id:event_id,
            name: name,
            title:title,
            topic:topic,
            created_at: +new Date()
        }
        let response: any = await DAO.save_data(Models.EventSpeakers, data_to_save)
        return response
    }
    catch (err) {
        throw err;
    }
}
const edit_speaker = async (data: any) => {
    try {
        let { speaker_id,name,title,topic } = data
        console.log(speaker_id)
        let set_data: any = {}
        let query = { _id: speaker_id}
        let options = { new: true }
        
        set_data.name= name
        set_data.title= title
        set_data.topic= topic
        let response: any = await DAO.find_and_update(Models.EventSpeakers,query, set_data,options)
        
        return response

    }
    catch (err) {
        throw err;
    }
}
const speaker_detail = async (data: any) => {
    try {
        let { speaker_id} = data
        let query = { _id:speaker_id }
        let projection = { __v: 0 }
        let options = { lean: true }
        return await DAO.get_data(Models.EventSpeakers, query, projection, options)
    }
    catch (err) {
        throw err;
    }
}
const delete_speaker = async (data: any) => {
    try {
        let { speaker_id} = data
        let query = { _id: speaker_id }
        let schedule_query = { speaker_id: speaker_id }
        let response =await DAO.remove_many(Models.EventSpeakers, query)
        //await DAO.remove_many(Models.ScheduleSpeaker, schedule_query)
        return response

    }
    catch (err) {
        throw err;
    }
}
const event_speakers =async (event_id: any) => {
    try {
        let query = { event_id }
        let projection = { __v: 0, password: 0, otp: 0 }
        let options = { lean: true }
        let response: any = await DAO.get_data(Models.EventSpeakers, query, projection, options)
        return response
    }
    catch (err) {
        throw err;
    }
}
const make_speaker_response = async (_id: any, language: string) => {
    try {

        let query = { _id }
        let projection = { __v: 0, password: 0, otp: 0 }
        let options = { lean: true }
        let response: any = await DAO.get_data(Models.EventSpeakers, query, projection, options)

        if (response.length) {
            return response[0]

        } else {
            throw await handle_custom_error('INVALID_OBJECT_ID', language)
        }

    }
    catch (err) {
        throw err;
    }
}

//category module
const getCategories = async () => {
    try {

        let query = { is_deleted: false }
        let projection = { __v: 0 }
        let options = { lean: true }
        return await DAO.get_data(Models.Categories, query, projection, options)
    }
    catch (err) {
        throw err;
    }
}

//requirements module
const get_static_data = async () => {
    try {

        let query = { is_deleted: false }
        let projection = { __v: 0 }
        let options = { lean: true }
        let requirements = await DAO.get_data(Models.Requirements, query, projection, options)
        let categories = await DAO.get_data(Models.Categories, query, projection, options)
        return ({requirements:requirements,categories:categories})
    }
    catch (err) {
        throw err;
    }
}

/*=================Add,edit,delete,listing schedule==============*/
const add_schedule = async (data: any) => {
    try {

        let { date,start_time,end_time,venue,event_id} = data
        let data_to_save: any = {
            date: date,
            start_time: start_time,
            end_time: end_time,
            venue: venue,
            event_id: event_id,
            created_at: +new Date()
        }
        var addSchedule = await DAO.save_data(Models.EventSchedule, data_to_save)
        return addSchedule
    }
    catch (err) {
        throw err;
    }
}
const edit_schedule = async (data: any) => {
    try {

        let { schedule_id,date,start_time,end_time,venue} = data
        let set_data: any = {}
        let query = { _id: schedule_id}
        let options = { new: true }
        
        set_data.date= date
        set_data.start_time= start_time
        set_data.end_time= end_time
        set_data.venue= venue
        let response: any = await DAO.find_and_update(Models.EventSchedule,query, set_data,options)
        return response
    }
    catch (err) {
        throw err;
    }
}
const add_schedule_speaker = async (data: any) => {
    try {

        let { schedule_id,speaker_id} = data
        let data_to_save: any = {
            schedule_id: schedule_id,
            speaker_id: speaker_id,
            created_at: +new Date()
        }
        var addScheduleSpeaker = await DAO.save_data(Models.ScheduleSpeaker, data_to_save)
        return addScheduleSpeaker
    }
    catch (err) {
        throw err;
    }
}
const get_schedule_speakers = async (data: any) => {
    try {
         let { schedule_id} = data
        // let query = { schedule_id : schedule_id }
        // let projection = { __v: 0 }
        // let options = { lean: true }
        // let response = await DAO.get_data(Models.ScheduleSpeaker, query, projection, options)
        // return response
        let query = [
            await match_schedule_speakers(schedule_id),
            await look_up_schedule_speakers(),
            await unwind_data(),
            await group_data()
            
        ]
        let options = { lean: true }
        let fetch_data : any = await DAO.aggregate_data(Models.ScheduleSpeaker, query, options)
        return fetch_data
    }
    catch (err) {
        throw err;
    }
}
const look_up_schedule_speakers = async () =>{
    try {

        return {
            $lookup: {
                from: "event_speakers",
                let: { speaker_id: "$speaker_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$_id", "$$speaker_id"]
                            }
                        }
                    }
                    
                ],
                as: "speaker"
            }
        }
    }
    catch (err) {
        throw err;
    }
}
const unwind_data = async () => {
    try {

        return {
            
            "$unwind":"$speaker"
            
        }

    }
    catch (err) {
        throw err;
    }
}
const group_data = async () => {
    try {

        return {
            $group: {
                _id: "$_id",
                speaker_id: { "$last": "$speaker._id" },
                topic: { "$last": "$speaker.topic" },
                title: { "$last": "$speaker.title" },
                name: { "$last": "$speaker.name" }
               
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const match_schedule_speakers = async (id: string) => {
    try {
        return {
            $match: {
                schedule_id : mongoose.Types.ObjectId(id)
            }
        }

    }
    catch (err) {
        throw err;
    }
}
const delete_schedule_speaker = async (data: any) => {
    try {

        let { schedule_speaker_id} = data
        let query = { _id: schedule_speaker_id }
        let response =await DAO.remove_many(Models.ScheduleSpeaker, query)
        return response
    }
    catch (err) {
        throw err;
    }
}

export {
    make_event_response,
    edit_event_data,
    set_event_data,
    delete_event,
    fetch_total_count,
    add_speaker,
    edit_speaker,
    delete_speaker,
    event_speakers,
    make_speaker_response,
    getCategories,
    get_static_data,
    match_event_by_id,
    join_event,
    fetch_total_event_attendees,
    add_schedule,
    edit_schedule,
    add_schedule_speaker,
    delete_schedule_speaker,
    speaker_detail,
    get_schedule_speakers,
    group_data,
    unwind_data,
    add_faq,
    edit_faq,
    delete_faq,
    get_event_faqs
}