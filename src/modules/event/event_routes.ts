
import express from 'express';
import * as event_controller from './event_controller';
import authenticator from '../../middlewares/authenticator';
const router = express.Router();

//static data
router.get("/static-data", event_controller.getStaticData)

router.put("/event",authenticator,event_controller.editEvent)
router.post("/event",authenticator,event_controller.addEvent)
router.get("/event", event_controller.getEvent)
router.get("/get_events", event_controller.getAllEvents)
router.post("/delete-event",authenticator, event_controller.deleteEvent)
router.post("/join_event",authenticator, event_controller.joinEvent)

//faq module
router.post("/faq",event_controller.addFaq)
router.put("/faq",event_controller.editFaq)
//router.get("/faq",event_controller.getSpeakerDetail)
router.get("/event-faqs",event_controller.getEventFaqs)
router.delete("/faq",event_controller.deleteFaq)

//speaker module
router.put("/speaker",event_controller.editSpeaker)
router.post("/speaker",event_controller.addSpeaker)
router.get("/speaker",event_controller.getSpeakerDetail)
router.get("/event-speakers",event_controller.getEventSpeakers)
router.post("/delete-speaker",event_controller.deleteSpeaker)

//schedule module
router.post("/schedule",event_controller.addSchedule)
router.put("/schedule",event_controller.editSchedule)
router.post("/schedule-speaker",event_controller.addScheduleSpeaker)
router.delete("/schedule-speaker",event_controller.deleteScheduleSpeaker)
router.get("/schedule-speaker",event_controller.getScheduleSpeakers)


//schedule module
//router.post("/schedule",event_controller.addSchedule)



export default router;