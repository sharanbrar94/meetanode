import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';
const EventCategoriesSchema = createSchema({
    event_id: Type.ref(Type.objectId({ default: null })).to('events', <any>Models.Events),
    category_id: Type.ref(Type.objectId({ default: null })).to('categories', <any>Models.Categories),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const EventCategories = typedModel('event_categories', EventCategoriesSchema);
export default EventCategories