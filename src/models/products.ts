import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';

const ProductsSchema = createSchema({
    added_by : Type.ref(Type.objectId({ default: null })).to('users', <any>Models.Users),
    name: Type.string({ default: null }),
    image_url : Type.string({ default: null }),
    price: Type.number({ default: 0 }),
    sold : Type.boolean({ default: false }),
    is_blocked: Type.boolean({ default: false }),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() }),
})

const Products = typedModel('products', ProductsSchema);
export default Products