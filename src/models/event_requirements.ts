import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';
const EventRequirementsSchema = createSchema({
    event_id: Type.ref(Type.objectId({ default: null })).to('events', <any>Models.Events),
    requirement_id: Type.ref(Type.objectId({ default: null })).to('requirements', <any>Models.Requirements),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const EventRequirements = typedModel('event_requirements', EventRequirementsSchema);
export default EventRequirements