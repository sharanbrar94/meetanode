import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';
const EventSpeakersSchema = createSchema({
    event_id: Type.ref(Type.objectId({ default: null })).to('events', <any>Models.Events),
    name: Type.string({ default: null }),
    title: Type.string({ default: null }),
    topic: Type.string({ default: null }),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const EventSpeakers = typedModel('event_speakers', EventSpeakersSchema);
export default EventSpeakers