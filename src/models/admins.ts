
import { createSchema, Type, typedModel } from 'ts-mongoose';

const AdminSchema = createSchema({
      name: Type.string({ default: null }),
      image: Type.string({ default: null }),
      email: Type.string({ default: null }),
      password: Type.string({ default: null }),
      roles: Type.array().of(Type.string({ default: [] })),
      super_admin : Type.boolean({ default: false }),
      is_blocked: Type.boolean({ default: false }),
      is_deleted: Type.boolean({ default: false }),
      created_at: Type.string({ default: +new Date() })
})

const Admin = typedModel('admins', AdminSchema);
export default Admin