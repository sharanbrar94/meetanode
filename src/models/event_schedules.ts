import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';
const EventScheduleSchema = createSchema({
    event_id: Type.ref(Type.objectId({ default: null })).to('events', <any>Models.Events),
    date: Type.date({ default: null }),
    start_time: Type.string({ default: null }),
    end_time: Type.string({ default: null }),
    venue: Type.string({ default: null }),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const EventSchedule = typedModel('event_schedule', EventScheduleSchema);
export default EventSchedule