import { createSchema, Type, typedModel } from 'ts-mongoose';

const RequirementSchema = createSchema({
    name : Type.string({ default: null }),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const Requirements = typedModel('requirements', RequirementSchema);
export default Requirements