import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';
const type = [ 'ABOUT_US', 'PRIVACY_POLICY', 'TERMS_AND_CONDITIONS' ]

const ContentSchema = createSchema({
    type : Type.string({ enum : type }),
    description : Type.string({ default : null }),
    created_at : Type.string({ default : +new Date() }),
})

const Content = typedModel('content', ContentSchema);
export default Content