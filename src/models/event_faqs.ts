import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';
const EventFaqSchema = createSchema({
    event_id: Type.ref(Type.objectId({ default: null })).to('events', <any>Models.Events),
    question: Type.string({ default: null }),
    answer: Type.string({ default: null }),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const EventFaqs = typedModel('event_faqs', EventFaqSchema);
export default EventFaqs