import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';

const type = [
    'PRODUCT_LIKED', 
    'NEW_PRODUCT_ADDED', 
    'NEW_MESSAGE',
    'BROADCAST_PUSH'
]


const NotificationSchema = createSchema({
    user_id: Type.ref(Type.objectId({ default: null })).to('users', <any>Models.Users),
    product_id: Type.ref(Type.objectId({ default: null })).to('products', <any>Models.Products),
    type: Type.string({ default: null, enum: type }),
    title: Type.string({ default: null }),
    message: Type.string({ default: null }),
    read : Type.boolean({ default : false }),
    clear : Type.boolean({ default : false }),
    created_at: Type.string({ default: +new Date() })
})


const Notifications = typedModel('notifications', NotificationSchema);
export default Notifications