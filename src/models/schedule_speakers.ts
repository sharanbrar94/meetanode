import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';
const ScheduleSpeakerSchema = createSchema({
    schedule_id: Type.ref(Type.objectId({ default: null })).to('event_schedules', <any>Models.EventSchedule),
    speaker_id: Type.ref(Type.objectId({ default: null })).to('event_speakers', <any>Models.EventSpeakers),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const ScheduleSpeaker = typedModel('schedule_speakers', ScheduleSpeakerSchema);
export default ScheduleSpeaker