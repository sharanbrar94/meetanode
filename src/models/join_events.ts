import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';
const JoinEventsSchema = createSchema({
    event_id: Type.ref(Type.objectId({ default: null })).to('events', <any>Models.Events),
    user_id: Type.ref(Type.objectId({ default: null })).to('users', <any>Models.Users),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const JoinEvents = typedModel('join_events', JoinEventsSchema);
export default JoinEvents