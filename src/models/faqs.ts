import { createSchema, Type, typedModel } from 'ts-mongoose';

const FaqSchema = createSchema({
    question : Type.string({ default : null }),
    answer : Type.string({ default : null }),
    created_at : Type.string({ default : +new Date() }),
})

const Faq = typedModel('faqs', FaqSchema);
export default Faq