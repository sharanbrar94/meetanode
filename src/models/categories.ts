import { createSchema, Type, typedModel } from 'ts-mongoose';

const CategorySchema = createSchema({
    name : Type.string({ default: null }),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const Categories = typedModel('categories', CategorySchema);
export default Categories