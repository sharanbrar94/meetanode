import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';
const EventPassesSchema = createSchema({
    event_id: Type.ref(Type.objectId({ default: null })).to('events', <any>Models.Events),
    name: Type.string({ default: null }),
    price: Type.string({ default: null }),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const EventPasses = typedModel('event_passes', EventPassesSchema);
export default EventPasses