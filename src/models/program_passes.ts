import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';
const ProgramPassesSchema = createSchema({
    pass_id: Type.ref(Type.objectId({ default: null })).to('event_passes', <any>Models.EventPasses),
    program_id:Type.ref(Type.objectId({ default: null })).to('event_programs', <any>Models.EventPrograms),
    created_at: Type.string({ default: +new Date() })
})

const ProgramPasses = typedModel('program_passes', ProgramPassesSchema);
export default ProgramPasses