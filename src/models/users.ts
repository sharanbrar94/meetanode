
import { createSchema, Type, typedModel } from 'ts-mongoose';
const social_type = [null, "GOOGLE", "TWITTER", "APPLE"]
const account_status = ["ACTIVATED", "DEACTIVATED"]

const UserSchema = createSchema({
    social_type: Type.string({ default: null, enum: social_type}),
    social_token: Type.string({ default: null }),
    image: Type.string({ default: null }),
    cover_image: Type.string({ default: null }),
    username: Type.string({ default: null }),
    name: Type.string({ default: null }),
    email: Type.string({ default: null }),
    is_blocked: Type.boolean({ default: false }),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const Users = typedModel('users', UserSchema);
export default Users