
import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';

const EventSchema = createSchema({
    user_id: Type.ref(Type.objectId({ default: null })).to('users', <any>Models.Users),
    event_name: Type.string({ default: null }),
    event_type: Type.number({ default: null }),
    private_event: Type.number({ default: null }),
    slug: Type.string({ default: null }),
    start_date: Type.date({ default: null }),
    end_date: Type.date({ default: null }),
    tagline: Type.string({ default: null }),
    overview : Type.string({ default: null }),
    description : Type.string({ default: null }),
    state: Type.string({ default: null }),
    city: Type.string({ default: null }),
    country: Type.string({ default: null }),
    address: Type.string({ default: null }),
    cover_photo : Type.string({ default: null }),
    twitter: Type.string({ default: null }),
    website: Type.string({ default: null }),
    is_deleted: Type.boolean({ default: false }),
    created_at: Type.string({ default: +new Date() })
})

const Events = typedModel('events', EventSchema);
export default Events