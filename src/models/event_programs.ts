import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as Models from './index';
const EventProgramsSchema = createSchema({
    event_id: Type.ref(Type.objectId({ default: null })).to('events', <any>Models.Events),
    program_name: Type.string({ default: null }),
    created_at: Type.string({ default: +new Date() })
})

const EventPrograms = typedModel('event_programs', EventProgramsSchema);
export default EventPrograms