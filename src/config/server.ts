import cors from 'cors';
import express from 'express';
import { config } from 'dotenv';
config();
import path from 'path';
import http from 'http';
import https from 'https';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';
import { connect_to_db, bootstrap_data } from './index';
import admin_routes from '../modules/admin/admin_routes';
import user_routes from '../modules/user/user_routes'
import event_routes from '../modules/event/event_routes'
import upload_routes from '../modules/uploads/upload_routes'
import swagger_ui from 'swagger-ui-express';
import openapi_docs from '../../output.openapi.json';
import fs from 'fs';

const app = express();
const env = process.env.ENVIRONMENT
console.log("----------env-------", env)
let port = process.env.LOCAL_PORT
if (env == 'PROD') { port = process.env.PROD_PORT }

app.use(cors({ origin: "*" }))
app.use(fileUpload());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json())

app.get('/', (req: any, res: any) => { res.send('Hello World!') })
app.use("/Admin", admin_routes)
app.use("/User", user_routes)
app.use("/Event", event_routes)
app.use("/Upload", upload_routes)

// make swagger documentation
let swagger_options = { customSiteTitle: "Meeta Api Documentation" };
app.use('/docs', swagger_ui.serve, swagger_ui.setup(openapi_docs, swagger_options));

let server: any;

// if (env == 'PROD') {
//   server = http.createServer(app);
//   server.listen(port, () => {
//     console.log(`Server running at port ${port}...`);
//   })
//   // backup_db_cron.start();
//   // remove_img_cron.start();
// }
// else {
//   server = http.createServer(app);
//   server.listen(port, () => {
//     console.log(`Server running at port ${port}...`);
//   })
// }

if (process.env.SSL == "true") {
  server = https.createServer(
    {
      key: fs.readFileSync(process.env.SSL_KEY),
      cert: fs.readFileSync(process.env.SSL_CERT),
    },
    app
  );
} else {
  server = http.createServer(app);
}


server.listen(port, console.log(`server is running on PORT ${port}`));


connect_to_db();
bootstrap_data();






