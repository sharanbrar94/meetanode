
import { verify_token, handle_catch, handle_custom_error } from './index';
import * as DAO from "../DAO";
import * as Models from '../models';
import { error, app_constant } from '../config/index';
const { scope } = app_constant
const admin_scope = scope.admin
const user_scope = scope.user


const authenticator = async (req: any, res: any, next: any) => {
    try {

        let { token } = req.headers, api_path = req.originalUrl
        let set_language = 'ENGLISH';

        let { language: body_language } = req.body, { language: query_language } = req.query
        if (body_language != undefined) { set_language = body_language }
        if (query_language != undefined) { set_language = query_language }

        let admin_path = api_path.includes("Admin")
        let user_path = api_path.includes("User")
        let event_path = api_path.includes("Event")
        

        if (admin_path) {

            let fetch_token_data: any = await verify_token(token, admin_scope, set_language)
            if (fetch_token_data) {

                let { admin_id, access_token, token_gen_at } = fetch_token_data

                let query: any = { _id: admin_id }
                let projection = { __v: 0 }
                let options = { lean: true }
                let fetch_data: any = await DAO.get_data(Models.Admin, query, projection, options)

                if (fetch_data.length > 0) {

                    let { roles, super_admin } = fetch_data[0]
                    let split_api_path = api_path.split('/')
                    let new_path = split_api_path[2]
                    let type = new_path.toUpperCase()

                    if (super_admin != true) {
                        let check_roles = roles.includes(type)
                        if (check_roles != true) {
                            throw await handle_custom_error('INSUFFICIENT_PERMISSIONS', set_language)
                        }
                    }

                    fetch_data[0].access_token = access_token
                    fetch_data[0].token_gen_at = token_gen_at
                    req.user_data = fetch_data[0]
                    req.session_data = fetch_token_data
                    next();

                } else {
                    throw await handle_custom_error('UNAUTHORIZED', set_language)
                }

            } else {
                throw await handle_custom_error('UNAUTHORIZED', set_language)
            }

        }
        else if (user_path || event_path) {


            let fetch_token_data: any = await verify_token(token, user_scope, set_language)
            if (fetch_token_data) {

                let { user_id, access_token, device_type, fcm_token, token_gen_at } = fetch_token_data

                let query: any = { _id: user_id }
                let projection = { __v: 0, password: 0 }
                let options = { lean: true }
                let fetch_data: any = await DAO.get_data(Models.Users, query, projection, options)

                if (fetch_data.length > 0) {

                    fetch_data[0].access_token = access_token
                    fetch_data[0].device_type = device_type
                    fetch_data[0].fcm_token = fcm_token
                    fetch_data[0].token_gen_at = token_gen_at
                    req.user_data = fetch_data[0]
                    req.session_data = fetch_token_data
                    next();

                } else {
                    throw await handle_custom_error('UNAUTHORIZED', set_language)
                }

            } else {
                throw await handle_custom_error('UNAUTHORIZED', set_language)
            }

        }
        else {
            throw await handle_custom_error('UNAUTHORIZED', set_language)
        }

    }
    catch (err) {
        handle_catch(res, err)
    }
}

export default authenticator